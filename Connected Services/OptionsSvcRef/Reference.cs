﻿//------------------------------------------------------------------------------
// <автоматически создаваемое>
//     Этот код создан программой.
//     //
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторного создания кода.
// </автоматически создаваемое>
//------------------------------------------------------------------------------

namespace OptionsSvcRef
{
    using System.Runtime.Serialization;


    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "MxDataType", Namespace = "http://schemas.datacontract.org/2004/07/ArchestrA.GRAccess")]
    public enum MxDataType : int
    {

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxDataTypeUnknown = -1,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxNoData = 0,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxBoolean = 1,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxInteger = 2,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxFloat = 3,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxDouble = 4,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxString = 5,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxTime = 6,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxElapsedTime = 7,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxReferenceType = 8,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxStatusType = 9,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxDataTypeEnum = 10,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxSecurityClassificationEnum = 11,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxDataQualityType = 12,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxQualifiedEnum = 13,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxQualifiedStruct = 14,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxInternationalizedString = 15,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxBigString = 16,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        MxDataTypeEND = 17,
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "ServiceError", Namespace = "http://schemas.datacontract.org/2004/07/GRAccessService")]
    public partial class ServiceError : object
    {

        private int ErrorCodeField;

        private string MessageField;

        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ErrorCode
        {
            get
            {
                return this.ErrorCodeField;
            }
            set
            {
                this.ErrorCodeField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message
        {
            get
            {
                return this.MessageField;
            }
            set
            {
                this.MessageField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "Attribute", Namespace = "http://schemas.datacontract.org/2004/07/GRAccessService.Models")]
    public partial class Attribute : object
    {

        private string DescriptionField;

        private string NameField;

        private OptionsSvcRef.MxDataType TypeField;

        private string ValueField;

        private System.Collections.Generic.List<string> ValuesField;

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public OptionsSvcRef.MxDataType Type
        {
            get
            {
                return this.TypeField;
            }
            set
            {
                this.TypeField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Value
        {
            get
            {
                return this.ValueField;
            }
            set
            {
                this.ValueField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Collections.Generic.List<string> Values
        {
            get
            {
                return this.ValuesField;
            }
            set
            {
                this.ValuesField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName = "OptionsSvcRef.IOptionsSvc")]
    public interface IOptionsSvc
    {

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IOptionsSvc/CreateParameter", ReplyAction = "http://tempuri.org/IOptionsSvc/CreateParameterResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(OptionsSvcRef.ServiceError), Action = "http://tempuri.org/IOptionsSvc/CreateParameterServiceErrorFault", Name = "ServiceError", Namespace = "http://schemas.datacontract.org/2004/07/GRAccessService")]
        System.Threading.Tasks.Task<bool> CreateParameterAsync(string objName, string paramName, string paramDesc, OptionsSvcRef.MxDataType type, string value, string engUnits, string trendHigh, string trendLow, System.Collections.Generic.List<string> values);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IOptionsSvc/AddStreamedValue", ReplyAction = "http://tempuri.org/IOptionsSvc/AddStreamedValueResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(OptionsSvcRef.ServiceError), Action = "http://tempuri.org/IOptionsSvc/AddStreamedValueServiceErrorFault", Name = "ServiceError", Namespace = "http://schemas.datacontract.org/2004/07/GRAccessService")]
        System.Threading.Tasks.Task<bool> AddStreamedValueAsync(string objName, string paramName, string value);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IOptionsSvc/UpdateOptions", ReplyAction = "http://tempuri.org/IOptionsSvc/UpdateOptionsResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(OptionsSvcRef.ServiceError), Action = "http://tempuri.org/IOptionsSvc/UpdateOptionsServiceErrorFault", Name = "ServiceError", Namespace = "http://schemas.datacontract.org/2004/07/GRAccessService")]
        System.Threading.Tasks.Task<bool> UpdateOptionsAsync(string objName, string paramName, System.Collections.Generic.List<string> values);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IOptionsSvc/GetEngUnits", ReplyAction = "http://tempuri.org/IOptionsSvc/GetEngUnitsResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(OptionsSvcRef.ServiceError), Action = "http://tempuri.org/IOptionsSvc/GetEngUnitsServiceErrorFault", Name = "ServiceError", Namespace = "http://schemas.datacontract.org/2004/07/GRAccessService")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<string>> GetEngUnitsAsync();

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IOptionsSvc/GetOptionsAttr", ReplyAction = "http://tempuri.org/IOptionsSvc/GetOptionsAttrResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(OptionsSvcRef.ServiceError), Action = "http://tempuri.org/IOptionsSvc/GetOptionsAttrServiceErrorFault", Name = "ServiceError", Namespace = "http://schemas.datacontract.org/2004/07/GRAccessService")]
        System.Threading.Tasks.Task<OptionsSvcRef.Attribute> GetOptionsAttrAsync(string objName, string paramName);
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public interface IOptionsSvcChannel : OptionsSvcRef.IOptionsSvc, System.ServiceModel.IClientChannel
    {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public partial class OptionsSvcClient : System.ServiceModel.ClientBase<OptionsSvcRef.IOptionsSvc>, OptionsSvcRef.IOptionsSvc
    {

        /// <summary>
        /// Реализуйте этот разделяемый метод для настройки конечной точки службы.
        /// </summary>
        /// <param name="serviceEndpoint">Настраиваемая конечная точка</param>
        /// <param name="clientCredentials">Учетные данные клиента.</param>
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);

        public OptionsSvcClient() :
                base(OptionsSvcClient.GetDefaultBinding(), OptionsSvcClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.BasicHttpBinding_IOptionsSvc.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public OptionsSvcClient(EndpointConfiguration endpointConfiguration) :
                base(OptionsSvcClient.GetBindingForEndpoint(endpointConfiguration), OptionsSvcClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public OptionsSvcClient(EndpointConfiguration endpointConfiguration, string remoteAddress) :
                base(OptionsSvcClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public OptionsSvcClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) :
                base(OptionsSvcClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public OptionsSvcClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
                base(binding, remoteAddress)
        {
        }

        public System.Threading.Tasks.Task<bool> CreateParameterAsync(string objName, string paramName, string paramDesc, OptionsSvcRef.MxDataType type, string value, string engUnits, string trendHigh, string trendLow, System.Collections.Generic.List<string> values)
        {
            return base.Channel.CreateParameterAsync(objName, paramName, paramDesc, type, value, engUnits, trendHigh, trendLow, values);
        }

        public System.Threading.Tasks.Task<bool> AddStreamedValueAsync(string objName, string paramName, string value)
        {
            return base.Channel.AddStreamedValueAsync(objName, paramName, value);
        }

        public System.Threading.Tasks.Task<bool> UpdateOptionsAsync(string objName, string paramName, System.Collections.Generic.List<string> values)
        {
            return base.Channel.UpdateOptionsAsync(objName, paramName, values);
        }

        public System.Threading.Tasks.Task<System.Collections.Generic.List<string>> GetEngUnitsAsync()
        {
            return base.Channel.GetEngUnitsAsync();
        }

        public System.Threading.Tasks.Task<OptionsSvcRef.Attribute> GetOptionsAttrAsync(string objName, string paramName)
        {
            return base.Channel.GetOptionsAttrAsync(objName, paramName);
        }

        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }

        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }

        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IOptionsSvc))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Не удалось найти конечную точку с именем \"{0}\".", endpointConfiguration));
        }

        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IOptionsSvc))
            {
                return new System.ServiceModel.EndpointAddress("http://zinc-sp2017/OptionsSvc.svc");
            }
            throw new System.InvalidOperationException(string.Format("Не удалось найти конечную точку с именем \"{0}\".", endpointConfiguration));
        }

        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return OptionsSvcClient.GetBindingForEndpoint(EndpointConfiguration.BasicHttpBinding_IOptionsSvc);
        }

        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return OptionsSvcClient.GetEndpointAddress(EndpointConfiguration.BasicHttpBinding_IOptionsSvc);
        }

        public enum EndpointConfiguration
        {

            BasicHttpBinding_IOptionsSvc,
        }
    }
}
