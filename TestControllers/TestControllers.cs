using NUnit.Framework;
using ProtoMES.Models;
using ProtoMES.Controllers;
using ProtoMES.Controllers.Models;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
//using System.Web.Http.Results;
//using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Tests
{
    public class TestParameters
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task GetParametersByArea()
        {
            // Arrange
            var mockRepository = new Mock<ParameterContext>();
            var controller = new ParametersController(mockRepository.Object);

            // Act
            string id = "Roasting_Shop";
            IActionResult actionResult = await controller.GetParametersByArea(id);

            // Assert
            Assert.IsNotNull(actionResult);
            //Assert.Pass();
        }

        [Test]
        public async Task PostMethod()
        {
            // Arrange
            var mockRepository = new Mock<ParameterContext>();
            var controller = new ParametersController(mockRepository.Object);
            var model = new Parameter()
            {
                id = 1,
                tag = "Energy_Shop",
                name = "ManualParameters.Test",
                description = "Test Test",
                type = "string",
                groupId = 1
            };

            // Act    

            //IActionResult actionResult = await controller.PostParameter(model);
            //var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Parameter>;

            // Assert
            //Assert.IsNotNull(actionResult);

            //Assert.AreEqual("DefaultApi", createdResult.RouteName);
            //Assert.AreEqual(model.Title, createdResult.RouteValues["title"]);
        }

        [Test]
        public async Task GetTags()
        {
            // Arrange
            var mockRepository = new Mock<ParameterContext>();
            var controller = new ParametersController(mockRepository.Object);

            // Act
            string[] tags = { "Roasting_Shop", "Leaching_Shop" };
            IActionResult actionResult = await controller.GetTags(tags);

            // Assert
            Assert.IsNotNull(actionResult);
            //Assert.Pass();
        }

        [Test]
        public async Task GetHistoryValues()
        {
            // Arrange
            var mockRepository = new Mock<ParameterContext>();
            var controller = new PrmValuesController(mockRepository.Object);

            // Act
            string[] tags = { "Roast_Oven1.T_s", "Roast_Oven2.T_n", "Roast_Oven1.P2" };
            IActionResult actionResult = await controller.GetHistoryValues(tags, DateTime.Now.AddHours(-8), DateTime.Now);

            // Assert
            Assert.IsNotNull(actionResult);
            //Assert.Pass();
        }


        [Test]
        public async Task CreateParameterMethod()
        {
            // Arrange
            var mockRepository = new Mock<ParameterContext>();
            var controller = new ParametersController(mockRepository.Object);
            OptionsParameter param = new OptionsParameter();
            // Act
            param.objName = "Test_Object";
            param.paramName = "Test";
            param.paramDesc = "Test";
            param.type = "String";
            param.engUnits = "�3/�";
            param.trendHigh = null;
            param.trendLow = null;
            List<string> values = new List<string> { "������", "������", "������" };
            param.values = values;
            IActionResult actionResult = await controller.CreateOptionsParameter(param);

            // Assert
            //Assert.IsNotNull(actionResult);
        }

        [Test]
        public async Task GetOptionsParameterMethod()
        {
            // Arrange
            var mockRepository = new Mock<ParameterContext>();
            var controller = new ParametersController(mockRepository.Object);
            OptionsParameter param = new OptionsParameter();
            // Act
            string objName = "Test_Object";
            string paramName = "Test";

            IActionResult actionResult = await controller.GetOptionsParameter(objName, paramName);

            // Assert
            Assert.IsNotNull(actionResult);
        }
        [Test]
        public async Task GetEngUnitsMethod()
        {
            // Arrange
            var mockRepository = new Mock<ParameterContext>();
            var controller = new ParametersController(mockRepository.Object);
            OptionsParameter param = new OptionsParameter();
            // Act

            IActionResult actionResult = await controller.GetEngUnits();

            // Assert
            Assert.IsNotNull(actionResult);
        }

        [Test]
        public async Task GetAreas()
        {
            // Arrange
            var mockRepository = new Mock<DataContext>();
            var controller = new EquipmentsController(mockRepository.Object);

            // Act
            IActionResult actionResult = await controller.GetAreas();

            // Assert
            Assert.IsNotNull(actionResult);
            //Assert.Pass();
        }

        [Test]
        public async Task GetPredictionValues()
        {
            // Arrange
            var mockRepository = new Mock<ParameterContext>();
            var controller = new ParametersController(mockRepository.Object);

            List<string> tags = new List<string>();
            tags.Add("Roast_Oven1.P1");
            tags.Add("Roast_Oven1.P2");
            tags.Add("Roast_Oven1.Q_air");
            tags.Add("Roast_Oven1.Q_O2");
            tags.Add("Roast_Oven1.T_exg");
            tags.Add("Roast_Oven1.T_ks");

            IActionResult actionResult = await controller.GetPredictValues(tags.ToArray(),
                DateTime.Now.AddDays(-1), DateTime.Now, 30);

            // Assert
            Assert.IsNotNull(actionResult);
        }
    }
}