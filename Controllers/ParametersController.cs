﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProtoMES.Controllers.Models;
using ProtoMES.Models;
using System.Xml;
using GalaxySvcRef;
using OptionsSvcRef;
using PredictionSvcRef;

namespace ProtoMES.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParametersController : ControllerBase
    {
        private GalaxySvcClient grSvc = new GalaxySvcClient();
        private OptionsSvcClient opsSvc = new OptionsSvcClient();
        private PredictionSvcClient predSvc = new PredictionSvcClient();

        private readonly ParameterContext _context;

        public ParametersController(ParameterContext context)
        {
            _context = context;
        }

        // GET: api/Parameters
        [HttpGet]
        public IEnumerable<Parameter> GetParameter()
        {

            //var ret = (from e in _context.Parameter.Local where e.children != null select new Parameter(e.id, e.name, e.description, e.type, e.groupId, e.children)).ToList();// { values = e.values}
            //return ret;// _context.Parameter.Local.ToList();

            /*Parameter prm0 = new Parameter(1, "Temperature", "Температура в печи. Зона 1", "double", 0);//, null);
            Parameter prm1 = new Parameter(2, "Temperature", "Температура в печи. Зона 1", "double", 1);//, null);
            List<Parameter> ret = new List<Parameter>();
            prm0.children = new List<Parameter>();
            prm0.children.Add(prm1);
            ret.Add(prm0);
            //ret.Add(prm1);
            return ret;*/

            List<Parameter> ret = new List<Parameter>();

            XmlDocument xml = new XmlDocument();
            xml.Load(@"C:\ProtoMES\Parameters\Parameters.xml");
            foreach (XmlNode nd in xml.DocumentElement.ChildNodes)
            {
                string name = nd.Attributes["name"].Value.ToString();
                string tag = nd.Attributes["tag"].Value.ToString();
                string desc = nd.Attributes["desc"].Value.ToString();
                Parameter prm = new Parameter(name, desc, tag);

                if (nd.HasChildNodes)
                    prm.children.AddRange(GetChilds(nd));

                ret.Add(prm);
            }

            return ret;
        }

        private List<Parameter> GetChilds(XmlNode node)
        {
            List<Parameter> ret = new List<Parameter>();

            foreach (XmlNode nd in node.ChildNodes)
            {
                string name = nd.Attributes["name"].Value.ToString();
                string tag = nd.Attributes["tag"].Value.ToString();
                string desc = nd.Attributes["desc"].Value.ToString();
                Parameter prm = new Parameter(name, desc, tag);

                if (nd.HasChildNodes)
                    prm.children.AddRange(GetChilds(nd));

                ret.Add(prm);
            }

            return ret;
        }

        // GET: api/Parameters/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetParameter([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parameter = await _context.Parameter.FindAsync(id);

            if (parameter == null)
            {
                return NotFound();
            }

            return Ok(parameter);
        }

        // PUT: api/Parameters/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutParameter([FromRoute] int id, [FromBody] Parameter parameter)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != parameter.id)
            {
                return BadRequest();
            }

            _context.Entry(parameter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParameterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Parameters
        /*[HttpPost]
        public async Task<IActionResult> PostParameter([FromBody] Parameter parameter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //_context.Parameter.Add(parameter);
            //await _context.SaveChangesAsync();

            //await grSvc.SetParameterAsync(parameter.tag, parameter.name, 1, "test");

            // await grSvc.SetParameterAsync(parameter.tag, parameter.name, 1, "test");


            return CreatedAtAction("GetParameter", new { id = parameter.id }, parameter);
        }*/

        // DELETE: api/Parameters/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteParameter([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parameter = await _context.Parameter.FindAsync(id);
            if (parameter == null)
            {
                return NotFound();
            }

            _context.Parameter.Remove(parameter);
            await _context.SaveChangesAsync();

            return Ok(parameter);
        }

        private bool ParameterExists(int id)
        {
            return _context.Parameter.Any(e => e.id == id);
        }

        [HttpGet("GetParametersByArea/{id}")]
        //[Route("Equipments/GetAreas")]
        public async Task<IActionResult> GetParametersByArea([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parameters = await grSvc.GetParametersAsync(id);

            if (parameters == null)
            {
                return NotFound();
            }

            return Ok(parameters);
        }

        [HttpGet("GetTags/{tags}")]
        //[Route("Equipments/GetAreas")]
        public async Task<IActionResult> GetTags([FromQuery] string[] tags)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parameters = await grSvc.GetParametersAsync(tags[0]);

            if (parameters == null)
            {
                return NotFound();
            }

            return Ok(parameters);
        }

        // POST: api/Parameters
        [HttpPost("CreateOptionsParameter")]
        public async Task<IActionResult> CreateOptionsParameter([FromBody]OptionsParameter param)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            OptionsSvcRef.MxDataType typ = OptionsSvcRef.MxDataType.MxString;
            if (param.type == "Integer")
                typ = OptionsSvcRef.MxDataType.MxInteger;
            else if (param.type == "Float")
                typ = OptionsSvcRef.MxDataType.MxFloat;
            else if (param.type == "Double")
                typ = OptionsSvcRef.MxDataType.MxDouble;
            try
            {
                await opsSvc.CreateParameterAsync(param.objName, param.paramName, param.paramDesc, typ, param.values[0], param.engUnits, param.trendHigh, param.trendLow, param.values);
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                    return NotFound($"Exception : {e.Message}, InnerException: {e.InnerException.Message}");
                else return NotFound($"Exception : {e.Message}");
            }
            return CreatedAtAction("CreateOptionsParameter", new { id = param.objName }, $"{param.objName}.{param.paramName}");
        }

        /*[HttpPost("CreateOptionsParameter")]
        public async Task<IActionResult> CreateOptionsParameter([FromBody]string objName, [FromBody]string paramName, [FromBody]string paramDesc,
            [FromBody]string type, [FromBody]string engUnits, [FromBody]string trendHigh, [FromBody]string trendLow, [FromBody]List<string> values)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            OptionsSvcRef.MxDataType typ = OptionsSvcRef.MxDataType.MxString;
            if (type == "Integer")
                typ = OptionsSvcRef.MxDataType.MxInteger;
            else if (type == "Float")
                typ = OptionsSvcRef.MxDataType.MxFloat;
            else if (type == "Double")
                typ = OptionsSvcRef.MxDataType.MxDouble;
            await opsSvc.CreateParameterAsync(objName, paramName, paramDesc, typ, values[0], engUnits, trendHigh, trendLow, values);

            return CreatedAtAction("CreateOptionsParameter", new { id = objName }, $"{objName}.{paramName}");
        }*/

        [HttpGet("GetOptionsParameter/{objName}/{paramName}")]
        public async Task<IActionResult> GetOptionsParameter([FromRoute] string objName, [FromRoute] string paramName)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parameter = await opsSvc.GetOptionsAttrAsync(objName, paramName);

            if (parameter == null)
            {
                return NotFound();
            }

            return Ok(parameter);
        }

        [HttpGet("GetEngUnits")]
        public async Task<IActionResult> GetEngUnits()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parameter = await opsSvc.GetEngUnitsAsync();

            if (parameter == null)
            {
                return NotFound();
            }

            return Ok(parameter);
        }

        [HttpGet("GetPredictValues/{tags}/{datefrom}/{dateto}/{steps}")]
        public async Task<IActionResult> GetPredictValues([ModelBinder(BinderType = typeof(CustomArrayModelBinder))] string[] tags,
            [FromRoute] DateTime dateFrom, [FromRoute] DateTime dateTo, [FromRoute] int steps)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var values = await predSvc.PredictValuesAsync(tags.ToList(), dateFrom, dateTo, 30);

            if (values == null)
            {
                return NotFound();
            }

            return Ok(values);
        }
    }
}
