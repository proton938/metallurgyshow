﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProtoMES.Controllers.Models;
using ProtoMES.Models;
using GalaxySvcRef;
using System.Security.Principal;

namespace ProtoMES.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EquipmentsController : ControllerBase
    {
        private readonly DataContext _context;

        private GalaxySvcClient grSvc = new GalaxySvcClient();

        public EquipmentsController(DataContext context)
        {
            _context = context;
        }

        // GET: api/Equipments
        [HttpGet]
        public IEnumerable<ProtoMES.Controllers.Models.Equipment> GetEquipment()
        {
            /*List<Equipment> list = new List<Equipment>();
            list.Add(new Equipment
            {
                id = 1,
                name = "Методическая печь"
            });
            return list;*/
            var ret = (from e in _context.Equipment select new ProtoMES.Controllers.Models.Equipment { id = e.id, name = e.name }).ToList();
            /*foreach (var e in ret)
            {
                list.Add(new Equipment {
                id = e.id,
                name = e.name});
            }*/
            return _context.Equipment.Local.ToList();//.Local.ToList();
        }

        // GET: api/Equipments/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEquipment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var equipment = await _context.Equipment.FindAsync(id);

            if (equipment == null)
            {
                return NotFound();
            }

            return Ok(equipment);
        }

        // PUT: api/Equipments/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEquipment([FromRoute] int id, [FromBody] ProtoMES.Controllers.Models.Equipment equipment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != equipment.id)
            {
                return BadRequest();
            }

            _context.Entry(equipment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EquipmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Equipments
        [HttpPost]
        public async Task<IActionResult> PostEquipment([FromBody] ProtoMES.Controllers.Models.Equipment equipment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Equipment.Add(equipment);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEquipment", new { id = equipment.id }, equipment);
        }

        // DELETE: api/Equipments/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEquipment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var equipment = await _context.Equipment.FindAsync(id);
            if (equipment == null)
            {
                return NotFound();
            }

            _context.Equipment.Remove(equipment);
            await _context.SaveChangesAsync();

            return Ok(equipment);
        }

        private bool EquipmentExists(int id)
        {
            return _context.Equipment.Any(e => e.id == id);
        }

        [HttpGet("GetAreas")]
        //[Route("Equipments/GetAreas")]
        public async Task<IActionResult> GetAreas()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //string curUser = HttpContext.User.Identity.Name;
            //WindowsIdentity currentUser = WindowsIdentity.GetCurrent();
            //string s = currentUser.Name;

            var equipment = await grSvc.GetAreasAsync();

            if (equipment == null)
            {
                return NotFound();
            }

            return Ok(equipment);
        }
    }
}
