﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Net;
using System.Xml;
using System.Linq;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;
using Microsoft.AspNetCore.Server.IISIntegration;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProtoMES.Models;
using ProtoMES.Controllers.Models;
using GalaxySvcRef;
using OptionsSvcRef;
using PredictionSvcRef;
using ReportingServices;
using System.Security.Cryptography.X509Certificates;

namespace ProtoMES.Controllers
{


    [System.Serializable]
    public class SoapException : SystemException { }
    public interface IServiceBehavior { }
    public abstract class ServiceHostBase : CommunicationObject { }
    public class ServiceHost{ }
    public sealed class ServiceAuthenticationBehavior { }
    public class ServiceDescription { }

    public interface IServiceCollection : ICollection<Microsoft.Extensions.DependencyInjection.ServiceDescriptor>, System.Collections.Generic.IEnumerable<Microsoft.Extensions.DependencyInjection.ServiceDescriptor>, System.Collections.Generic.IList<Microsoft.Extensions.DependencyInjection.ServiceDescriptor> { }

    [ServiceContract]  // https://docs.microsoft.com/ru-ru/previous-versions/office/developer/sharepoint-2010/ff521581%28v%3doffice.14%29
    public interface IRevert
    {
        [OperationContract]
        void Revert(string listName, int listItemId);
    }




    [Route("api/[controller]")]
    [ApiController]
    public class ReportsController : ControllerBase 
    {

        private ReportingService2010SoapClient rps = new ReportingService2010SoapClient(0);
        public ReportingServices.ItemNamespaceHeader myValue { get; set; }


        private static string[] Summaries = new[]
        {
            "ЖУРНАЛ ВЫПУСКА и ОТГРУЗКИ",
            "ТЕХНОЛОГИЧЕСКИЙ ЖУРНАЛ",
            "Рапорт Обжигового цеха",
            "Рапорт Выщелачивательного цеха",
            "Рапорт Сернокислотного цеха",
            "Рапорт Вельц-цеха",
            "Рапорт Электролиза",
            "Рапорт КЭЦ ПО",
            "Рапорт Гидрометаллургического цеха",
            "Энергоресурсы"
        };


        // GET: api/Reports
        [HttpGet]







        public async Task<CatalogItem[]> GetReports(String ItemPath)
        {
            // https://www.cyberforum.ru/ado-net/thread818006.html

            // var reports = await rps.GetReportServerConfigInfoAsync( TrustedUserHeader, EnableManualSnapshotCreation );
            IServiceCollection services = null;
            string n = IISDefaults.AuthenticationScheme;

            ReportingService2010SoapClient rs = new ReportingService2010SoapClient(0);
            NetworkCredential credentials = new NetworkCredential(Environment.UserDomainName, "", "http://desktop-4vqr1bb/Reports/browse/");
            rs.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            rs.ClientCredentials.Windows.ClientCredential = credentials;

            TrustedUserHeader t = new TrustedUserHeader();
            /*
            CatalogItem[] items = null;
            rs.OpenAsync();
            
            TrustedUserHeader x = new TrustedUserHeader();
            x.UserName = "proton";
            x.UserToken = Encoding.UTF8.GetBytes("{rs.ClientCredentials.Windows.AllowedImpersonationLevel}");

            CatalogItem catalog = new CatalogItem();

            Task<ListChildrenResponse> oServerInfoHeader = rs.ListChildrenAsync(t, @"/", false);

            return oServerInfoHeader;
            */
            // return t.ClientCredentials.Windows.ClientCredential;



            
            // https://stackoverflow.com/questions/58492171/reportservice2010-getitemdefinitionasync-always-returns-null-values


            TrustedUserHeader trustedUserHeader = new TrustedUserHeader { UserName = rs.ClientCredentials.Windows.ClientCredential.UserName, UserToken = Encoding.ASCII.GetBytes("{rs.ClientCredentials.Windows.AllowedImpersonationLevel}") };
            ListChildrenResponse listChildrenResponse = null;
            /*
            try
            {
                 listChildrenResponse = await rs.ListChildrenAsync(trustedUserHeader, @"/", false);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message + exception.StackTrace);
                return new CatalogItem[0];
            }
            */

             listChildrenResponse = await rs.ListChildrenAsync(trustedUserHeader, @"/", false);

            return listChildrenResponse.CatalogItems;

            // return n;



            /*
            string url = "http://desktop-4vqr1bb/ReportServer";
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            myHttpWebRequest.Credentials = CredentialCache.DefaultCredentials;
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

            return Encoding.ASCII.GetBytes("{rs.ClientCredentials.Windows.AllowedImpersonationLevel}");
            */

        }










        // GET: api/Reports/5
        [HttpGet("{id}", Name = "GetReport")]








        //  public async Task<IActionResult> Reports(ReportingServices.TrustedUserHeader TrustedUserHeader, string DataSource, string ItemPath, bool Recursive)

        // public IEnumerable<Report> Reports()
        // public async void Reports(ReportingServices.TrustedUserHeader TrustedUserHeader, string DataSource, string ItemPath, bool Recursive)
        // {

        /*
        int i = 0;
        return Enumerable.Range(0, 9).Select(index => new Report
        {
            name = Summaries[index],
            id = (++i).ToString(),                 //System.Guid.NewGuid().ToString()
            date = DateTime.Now.Date.ToString(),
            shift = 3
        });
        */


        // System.Xml.XmlNode Detail;
        // System.Xml.XmlQualifiedName DetailElementName;
        // System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
        // System.Xml.XmlNode node = doc.CreateNode(XmlNodeType.Element, SoapException.DetailElementName.Name, SoapException.DetailElementName.Namespace);

        /*
        ReportingService2010SoapClient rs = new ReportingService2010SoapClient(0);
        NetworkCredential credentials = new NetworkCredential(@"login", "password", "domain");
        rs.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
        rs.ClientCredentials.Windows.ClientCredential = credentials;
        ReportingServices.TrustedUserHeader t = new ReportingServices.TrustedUserHeader();
        CatalogItem[] items = null;
        await rs.OpenAsync();

        Console.WriteLine(t);
        */



        /*
        try
        {
            // собственно, в items содержится вся инфа об отчётах. 
            // А вот как произвести с ними те процедуры, которые мне необходимы - пока не ясно.
            // ReportingServices.ServerInfoHeader oServerInfoHeader = rs.ListChildren(t, @"/", true, out items);
        }
        catch (SoapException e)
        {
            Console.WriteLine(e);
        }
        */


        /*
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var reports = await rps.GetDataSourceContentsAsync(TrustedUserHeader, "54365ytreytr");

        if (reports == null)
        {
            return NotFound();
        }

        return Ok(reports);
        */
        // }



        // GET: api/Reports/5
        [HttpGet("{id}", Name = "GetReport")]





        public string Get(int id)
        {
            string reports = "ytreyte543653";

            return reports;
        }








        // POST: api/Reports
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Reports/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
