﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProtoMES.Controllers.Models;
using ProtoMES.Models;
using HistorianSvcRef;

namespace ProtoMES.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrmValuesController : ControllerBase
    {
        private HistorianSvcClient histSvc = new HistorianSvcClient();

        private readonly ParameterContext _context;

        public PrmValuesController(ParameterContext context)
        {
            _context = context;
        }

        // GET: api/PrmValues
        [HttpGet]
        public IEnumerable<PrmValue> GetPrmValue()
        {
            return _context.PrmValue.Local.ToList();
        }

        // GET: api/PrmValues/5
        /*[HttpGet("{datefrom}/{dateto}")]
        public async Task<IActionResult> GetPrmValue([FromRoute] DateTime dateFrom, [FromRoute] DateTime dateTo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var prmValue = await _context.PrmValue.FindAsync(id);
            var prmValue = await _context.PrmValue.Local.Where(x => x.dt >= dateFrom && x.dt < dateTo).ToList();

            if (prmValue == null)
            {
                return NotFound();
            }

            return Ok(prmValue);
        }*/

        // GET: api/PrmValues/5
        [HttpGet("{datefrom}/{dateto}")]
        public IEnumerable<PrmValue> GetPrmValue([FromRoute] DateTime dateFrom, [FromRoute] DateTime dateTo)
        {
            return _context.PrmValue.Local.Where(x => x.dt >= dateFrom && x.dt < dateTo).ToList();
        }

        // PUT: api/PrmValues/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPrmValue([FromRoute] int id, [FromBody] PrmValue prmValue)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != prmValue.id)
            {
                return BadRequest();
            }

            _context.Entry(prmValue).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PrmValueExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PrmValues
        [HttpPost]
        public async Task<IActionResult> PostPrmValue([FromBody] PrmValue prmValue)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.PrmValue.Add(prmValue);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPrmValue", new { id = prmValue.id }, prmValue);
        }

        // DELETE: api/PrmValues/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePrmValue([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var prmValue = await _context.PrmValue.FindAsync(id);
            if (prmValue == null)
            {
                return NotFound();
            }

            _context.PrmValue.Remove(prmValue);
            await _context.SaveChangesAsync();

            return Ok(prmValue);
        }

        private bool PrmValueExists(int id)
        {
            return _context.PrmValue.Any(e => e.id == id);
        }

        [HttpGet("GetHistoryValues/{tags}/{datefrom}/{dateto}")]
        public async Task<IActionResult> GetHistoryValues([ModelBinder(BinderType = typeof(CustomArrayModelBinder))] string[] tags, DateTime dateFrom, [FromRoute] DateTime dateTo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Dictionary<string, List<Models.Tag>> ret = new Dictionary<string, List<Models.Tag>>();
            List<HistorianSvcRef.Tag> values = await histSvc.GetHistoryTagsAsync(tags.ToList(), dateFrom, dateTo);
            foreach(HistorianSvcRef.Tag tag in values)
            {
                if (!ret.ContainsKey(tag.Name))
                {
                    Models.Tag t = new Models.Tag() {
                        dt = tag.TimeStamp,
                        qual = (ushort)tag.Quality,
                        val = Convert.ToDouble(tag.Value, System.Globalization.CultureInfo.InvariantCulture) };
                    ret.Add(tag.Name, new List<Models.Tag>() { t });
                }
                else
                {
                    Models.Tag t = new Models.Tag()
                    {
                        dt = tag.TimeStamp,
                        qual = (ushort)tag.Quality,
                        val = Convert.ToDouble(tag.Value, System.Globalization.CultureInfo.InvariantCulture)
                    };
                    ret[tag.Name].Add(t);
                }
            }

            if (ret == null)
            {
                return NotFound();
            }

            return Ok(ret);
        }
    }
}