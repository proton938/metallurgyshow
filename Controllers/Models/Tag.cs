﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProtoMES.Controllers.Models
{
    public class Tag
    {
        public DateTime dt { get; set; }
        public double val { get; set; }
        public ushort qual { get; set; }
    }
}
