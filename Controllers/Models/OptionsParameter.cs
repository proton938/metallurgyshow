﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProtoMES.Controllers.Models
{
    public class OptionsParameter
    {
        public OptionsParameter()
        {
        }

        public string objName { get; set; }
        public string paramName { get; set; }
        public string paramDesc { get; set; }
        public string type { get; set; }
        public string engUnits { get; set; }
        public string trendHigh { get; set; }
        public string trendLow { get; set; }
        public List<string> values { get; set; }

    }
}

