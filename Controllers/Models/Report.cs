﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProtoMES.Controllers.Models
{
    public class Report
    {
        public string id { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string date { get; set; }
        public int shift { get; set; }
    }
}
