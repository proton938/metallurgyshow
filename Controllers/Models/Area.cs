﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProtoMES.Controllers.Models
{
    public class Area
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int parentId { get; set; }
    }
}
