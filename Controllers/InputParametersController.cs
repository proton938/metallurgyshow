﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProtoMES.Controllers.Models;

namespace ProtoMES.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InputParametersController : ControllerBase
    {
        private static string[] Parameters = new[]
        {
            //"Режим работы",
            "Режим работы печи КС 3. Воздух",
            "Режим работы печи КС 3. Расход О2",
            "Режим работы печи КС 3. Упругость дутья 1",
            "Режим работы печи КС 3. Упругость дутья 2",
            "КС3. Температура. Кип. слой",
            "КС3. Температура. Под сводом",
            "КС3. Температура. В стояке",
            "КС3. Температура. Южный циклон",
            "КС3. Температура. Сев. циклон",
            "КС3. Температура. Перед эксг."
        };

        // GET: api/InputParameters
        [HttpGet]
        public IEnumerable<InputParameter> GetInputParameters()
        {
            Random random = new Random();
            int i = 2;
            List<InputParameter> ret = new List<InputParameter>();
            foreach(string name in Parameters)
            {
                InputParameter prm = new InputParameter();
                prm.name = name;
                prm.id = ++i;
                prm.min = Math.Round(random.NextDouble() * 1000, 0);
                prm.max = Math.Round(prm.min * 1.2, 0);
                ret.Add(prm);
            }
            return ret;
            /*return Enumerable.Range(1, 9).Select(index => new InputParameter
            {
                name = Parameters[index],
                id = index,
                min = random.NextDouble()*1000,
                max = min*1.2,

            });*/
        }

        // GET: api/InputParameters/5
        [HttpGet("{id}", Name = "Get")]
        public InputParameter Get(int id)
        {
            InputParameter prm = new InputParameter();
            return prm;
        }

        // POST: api/InputParameters
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/InputParameters/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
