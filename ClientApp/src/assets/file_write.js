	const fs = require("fs");

	var prmHistory = {};   // пустой массив истории параметров

	var currentValue = 10;  // начальное значение

	var currentDateTime = new Date();
	var eraUnix = new Date(Date.UTC(1970, 0, 1, 0, 0));
	var lastDateTime = new Date(currentDateTime - eraUnix - 24*3600000);

	var treeParams = fs.readFileSync("replase_server/Chel_Zinc_Plant.json", "utf8");  // дерево параметров
	treeParams = JSON.parse(treeParams);

	
	for (j=0; j<treeParams.length; j++) {
		
		if (treeParams[j].attributes.length != 0) {
			for ( i=0; i<treeParams[j].attributes.length; i++ ) {
				
				currentValue = 10*Math.random();
				
				var lastDateTime = new Date(currentDateTime - eraUnix - 24*3600000);
				
				prmHistory[treeParams[j].tagName+'.'+treeParams[j].attributes[i].name] = [];
				for (dt_val=0; dt_val<38; dt_val++) {
					var plus_minus = Math.random();
			
					if (plus_minus < 0.5) {
					currentValue = Math.abs(currentValue - Number((Math.random()).toFixed(3)));
					} else {
						currentValue = Math.abs(currentValue + Number((Math.random()).toFixed(3)));
					}
					
					lastDateTime = new Date(lastDateTime - eraUnix + 100000)
					
					
					prmHistory[treeParams[j].tagName+'.'+treeParams[j].attributes[i].name].push({dt: lastDateTime, val: Number(currentValue.toFixed(3))});
				}
			}
		} else {
			prmHistory[treeParams[j].tagName] = [];
		}
		
	}
	
	fs.writeFile("replase_server/historyValues/Main.json", JSON.stringify(prmHistory, null, '\t'), function(error){
 
		if(error) throw error; // если возникла ошибка
		console.log("Асинхронная запись файла завершена. Содержимое файла:");
		let data = fs.readFileSync("replase_server/historyValues/Main.json", "utf8");
		console.log(data);  // выводим считанные данные
	
	});
	
	
	
	/*
	
	for ( i=0; i<treeParams[0].attributes.length; i++ ) {
		
		prmHistory[0][treeParams[0].tagName].push({});
		prmHistory[0][treeParams[0].tagName][i][treeParams[0].attributes[i].name] = [];
		
		currentValue = Math.random()*10;
		
		for (j=0; j < 10; j++) {
			
			var plus_minus = Math.random();
			
			if (plus_minus < 0.5) {
			currentValue = Math.abs(currentValue - Number((Math.random()).toFixed(3)));
			} else {
				currentValue = Math.abs(currentValue + Number((Math.random()).toFixed(3)));
			}
			
			lastDateTime = new Date(lastDateTime - eraUnix + 100000)
			
			prmHistory[0][treeParams[0].tagName][i][treeParams[0].attributes[i].name].push({dt: lastDateTime, val: Number(currentValue.toFixed(3))});        
		}
	}
	
	fs.writeFile("replase_server/historyValues/Main.json", JSON.stringify(prmHistory, null, '\t'), function(error){
 
		if(error) throw error; // если возникла ошибка
		console.log("Асинхронная запись файла завершена. Содержимое файла:");
		let data = fs.readFileSync("replase_server/historyValues/Main.json", "utf8");
		console.log(data);  // выводим считанные данные
	
	});





/*

for (i=0; i < 24*36; i++) {
	
	var plus_minus = Math.random();
	
	if (plus_minus < 0.5) {
		currentValue = Math.abs(currentValue - Number((Math.random()).toFixed(3)));
	} else {
		currentValue = Math.abs(currentValue + Number((Math.random()).toFixed(3)));
	}
	
	lastDateTime = new Date(lastDateTime - eraUnix + 100000)
		
	prmHistory.push({dt: lastDateTime, val: Number(currentValue.toFixed(3))});
}
 
fs.writeFile("replase_server/Chel_Zinc_Plant/Roasting_Shop.F_sum_sm.json", JSON.stringify(prmHistory, null, '\t'), function(error){
 
    if(error) throw error; // если возникла ошибка
    console.log("Асинхронная запись файла завершена. Содержимое файла:");
    let data = fs.readFileSync("replase_server/Chel_Zinc_Plant/Roasting_Shop.F_sum_sm.json", "utf8");
    console.log(data);  // выводим считанные данные
	
});

*/