import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PageParams } from "../../../core/models/PageParams"

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  splitLink = document.location.href.split('/');

  public reports: Report[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private params: PageParams) {
    http.get<Report[]>(baseUrl + 'api/Reports').subscribe(result => {
      this.reports = result;
    }, error => console.error(error));
  }

  ngOnInit() {
  }

}

interface Report {
  id: string;
  url: number;
  name: number;
  description: string;
}
