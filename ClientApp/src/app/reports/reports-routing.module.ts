import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsComponent, ReportComponent } from './pages';

const routes: Routes = [
  { path: 'reports', component: ReportsComponent },
  { path: 'reports/:id', component: ReportsComponent },
  { path: 'report', component: ReportComponent },
  { path: 'report/:id', component: ReportComponent }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
