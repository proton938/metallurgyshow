import { Component, OnInit } from '@angular/core';

import { TreeParams } from "../../../core/models/TreeParams";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})

export class FooterComponent implements OnInit {

  public treeParams: TreeParams;

  constructor(public tree: TreeParams) {
    this.treeParams = tree;
  }


  ngOnInit() {
  }


}
