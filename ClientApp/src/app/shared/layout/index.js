"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./footer/footer.component"));
__export(require("./header/header.component"));
__export(require("./pages/redirect/redirect.component"));
__export(require("./pages/tools/tools.component"));
__export(require("./pages/login/login.component"));
//# sourceMappingURL=index.js.map