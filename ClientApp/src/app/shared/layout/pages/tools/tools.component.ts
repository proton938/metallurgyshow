import { Component, AfterViewInit, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { ReportsComponent } from '../../../../reports/pages/reports/reports.component';
import { ReportsModule } from '../../../../reports/reports.module';

@Component({
  selector: 'app-tools',
  //templateUrl: './tools.component.html',
  template: '<ng-container *ngComponentOutlet="Reports"></ng-container>', //template: '<div #elem>Element text</div>',
  styleUrls: ['./tools.component.css']
})
export class ToolsComponent implements AfterViewInit {
  Reports = ReportsComponent

  @ViewChild('elem', { static: false }) _elem: ElementRef;

  constructor(private _renderer: Renderer2) { }

  ngAfterViewInit() {
    /*const buttonElement = this._renderer.createElement('button');
    const text = this._renderer.createText('Text');

    this._renderer.appendChild(buttonElement, text);
    this._renderer.appendChild(this._elem.nativeElement, buttonElement);*/
  }

}
