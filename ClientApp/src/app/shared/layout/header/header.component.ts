import { Component, Inject, OnInit } from '@angular/core';
import { PageParams } from "../../../core/models/PageParams";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { RedirectComponent } from '../pages/redirect/redirect.component';

import { NavMenuComponent } from '../nav-menu/nav-menu.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public pageParams: PageParams;
  


  constructor(public params: PageParams) {
    this.pageParams = params;
    // this.redirectComponent = 'value';


    var currentUrl: any = document.location.href;
    currentUrl = currentUrl.split('/');

    if (currentUrl[4] != '') {
      this.pageParams.department = currentUrl[4];
    } else {
      this.pageParams.department = 'Main';
    }

    if (currentUrl[4] == undefined) {
      this.pageParams.department = 'Main';
    }


    this.pageParams.currentSection = currentUrl[3];

  }


  ngOnInit() {
    if (this.pageParams.currentSection != '') {
      (<HTMLInputElement>document.getElementById(this.pageParams.currentSection)).style.background = '#eee';
    }
  }


  private buferActive: any = '';
  public area: String;

  goto(page: String, /*area: String*/) {
    this.params.setPage(page);
    // area = this.params.getDepartment();

    // this.area = area;

    var anyPage: any = page;

    if (this.pageParams.currentSection != '') {
      (<HTMLInputElement>document.getElementById(this.pageParams.currentSection)).style.background = '#fff';
      this.pageParams.currentSection = '';
    }

    (<HTMLInputElement>document.getElementById(anyPage)).style.background = '#eee';
    if (this.buferActive != '') {
      (<HTMLInputElement>document.getElementById(this.buferActive)).style.background = '#fff';
    }
    (<HTMLInputElement>document.getElementById("SOUP_HCZ")).style.color = "#999";

    this.buferActive = anyPage;
  }


  unhideHeadMenu() {
    if (document.body.clientWidth < 768) {
      var hiddenMenu: any;
      hiddenMenu = (<HTMLInputElement>document.getElementById('headMenu')).className;
      if (hiddenMenu == 'menu-nav') {
        hiddenMenu = (<HTMLInputElement>document.getElementById('headMenu')).className = 'menu-nav-mobile';
      } else {
        hiddenMenu = (<HTMLInputElement>document.getElementById('headMenu')).className = 'menu-nav';
      }
    }
  }
}
