import { Component, Inject, OnInit, ViewChild, ElementRef, Injectable } from '@angular/core';
import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { TreeParams } from "../../../core/models/TreeParams";

import { PageParams } from "../../../core/models/PageParams";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { HeaderComponent } from '../header/header.component';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

import { RedirectComponent } from '../pages/redirect/redirect.component';

@Injectable()
export class WithCredentialsInterceptor implements HttpInterceptor {


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    request = request.clone({
      withCredentials: true
    });

    return next.handle(request);
  }
}

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  public treeParams: TreeParams;

  private auth: Auth;
  //private options = new RequestOptions({ withCredentials: true });
  public headers = new HttpHeaders({
    'Autorization': 'Windows'
  });

  public values: Value[];

  public currentPage;
  public countArrayURL;
  public sum: string = '';
  public varMenu: any = 0;

  public arrayExistingPage: any = [
    { pageName: 'mnemoscheme' },
    { pageName: 'specifications' },
    { pageName: 'archive' },
    { pageNmae: 'workorders' },
    { pageName: 'analytics' },
    { pageName: 'predictiveanalytics' },
    { pageNmae: 'reports' },
    { pageNmae: 'knowledge' }
  ];

  isExpanded = false;

  public pageParams: PageParams;


  constructor(private route: ActivatedRoute, private router: Router, private params: PageParams,  // праметры для роутинга
    public tree: TreeParams,   // загрузка модели дерева параметров
    http: HttpClient, @Inject('BASE_URL') baseUrl: string  // параметры для http-запроса
  ) {
    this.pageParams = params;
    this.treeParams = tree;

    var currentUrl: any = document.location.href;
    currentUrl = currentUrl.split('/');

    if (currentUrl[3] == '' || currentUrl[3] == 'redirect') {
      this.pageParams.page = 'mnemoscheme';
    } else {
      this.pageParams.page = currentUrl[3];
    }

    if (currentUrl[4] != 0 || currentUrl[4] != 'mnemoscheme' || currentUrl != undefined) {
      this.pageParams.currentShop = currentUrl[4];
    } else {
      this.pageParams.currentShop = 'ЧЦЗ';
    }

    //var username: string = JSON.parse(localStorage.getItem('currentUser'));

    http.get<Value[]>(baseUrl + 'api/equipments/getareas', {
      headers: new HttpHeaders({
        'Test': 'Test',
        'Content-Type': 'application/json',
      })
    }).subscribe(result => {  // конструкция для вывода массива из контроллера
      this.values = result;
    }, error => {
        console.error(error);
        http.get<Value[]>('assets/replase_server/GetAreas.json').subscribe((result) => this.values = result);
    }
    );
  }
  

  getAlt() {
    let elem = document.getElementById('showShortDesc');
    document.addEventListener('mousemove', function (event) {
     // elem.innerHTML = event.pageX + ' : ' + event.pageY;
      (<HTMLInputElement>document.getElementById("showShortDesc")).style.left = event.pageX + 'px';
      (<HTMLInputElement>document.getElementById("showShortDesc")).style.top = event.pageY - window.pageYOffset + 20 + 'px';
    });
    (<HTMLInputElement>document.getElementById("showShortDesc")).style.display = 'block';
  }

  unGetAlt() {
    (<HTMLInputElement>document.getElementById("showShortDesc")).style.left = 0 + 'px';
    (<HTMLInputElement>document.getElementById("showShortDesc")).style.top = 1200 + 'px';
    (<HTMLInputElement>document.getElementById("showShortDesc")).style.display = 'none';
  }




  collapse(dep: string, id: string) {

    this.isExpanded = false;
    this.params.setDepartment(dep);

    this.treeParams.updateSelectedPrmsModel();
    this.treeParams.getTreeParameters = [];
    this.treeParams.zoomMemory = [];

    
    (<HTMLInputElement>document.getElementById(id)).style.color = "#fff";


    if (this.pageParams.currentShop != '') {
      (<HTMLInputElement>document.getElementById(this.pageParams.currentShop)).style.color = "#999";
    }

    (<HTMLInputElement>document.getElementById("SOUP_HCZ")).style.color = "#999";


    this.pageParams.currentShop = id;

  }


  gotoSOUP_HCZ() {

    (<HTMLInputElement>document.getElementById(this.pageParams.page)).style.background = "#fff";

    if (this.pageParams.currentShop != '') {
      (<HTMLInputElement>document.getElementById(this.pageParams.currentShop)).style.color = "#999";
    }
    (<HTMLInputElement>document.getElementById("SOUP_HCZ")).style.color = "#fff";
    this.pageParams.currentShop = 'SOUP_HCZ';

    this.pageParams.page = 'mnemoscheme';
  }


  overStyle(hover) {
    (<HTMLInputElement>document.getElementById(hover)).style.color = "#fff";
  }
  outStyle(out) {
    if (this.pageParams.currentShop != out) {
      (<HTMLInputElement>document.getElementById(out)).style.color = "#999";
    }
  }



  toggle() {
    this.isExpanded = !this.isExpanded;
  }



  ngOnInit() {
    if (this.pageParams.currentShop != '') {
      (<HTMLInputElement>document.getElementById(this.pageParams.currentShop)).style.color = "#fff";
    }
  }



  private extractData<T>(res: Response) {
    if (res.status < 200 || res.status >= 300) {
      throw new Error('Bad response status: ' + res.status);
    }
    const body = res.json ? res.json() : null;
    return <T>(body || {});
  }

}



interface Value {
  id: number;
  abbreviation: string;
  areas: any;
}

export class Auth {
  isAdmin: boolean;
  isUser: boolean;
}
