import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RedirectComponent, LoginComponent, ToolsComponent } from './layout/pages';
import { ChartToolbarComponent } from './layout/chart-toolbar/chart-toolbar.component';

const routes: Routes = [
  { path: 'redirect/:page/:area', component: RedirectComponent },
  { path: 'tools', component: ToolsComponent },
  { path: 'login', component: LoginComponent },
  { path: 'chart-toolbar', component: ChartToolbarComponent },
  { path: 'chart-toolbar/:id', component: ChartToolbarComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }
