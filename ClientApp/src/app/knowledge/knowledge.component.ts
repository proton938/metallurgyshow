import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-knowledge',
  templateUrl: './knowledge.component.html',
  styleUrls: ['./knowledge.component.css']
})
export class KnowledgeComponent implements OnInit {

  private filterActivePos: any;

  public sample: any = [
    {
      shop: 'Roasting_Shop',
      tagName: 'Roast_Oven1',
      shortDesc: 'Обжиговая печь №1',
      beginning: '13.09.2019 11:28',
      end: '14.09.2019 08:39',
      className: 'positive_period',
      deploy: 0,
      attributes: [
        { name: 'Q_O2', description: 'Расход O2' },
        { name: 'T_ks', description: 'Температура кипящего слоя' },
        { name: 'T_n', description: 'Температура северный циклон' },
        { name: 'T_podsv', description: 'Температура кипящего слоя' },
        { name: 'T_s', description: 'Температура южный циклоня' }
      ]
    },
    {
      shop: 'Roasting_Shop',
      tagName: 'Roast_Oven1',
      shortDesc: 'Обжиговая печь №1',
      beginning: '16.09.2019 14:09',
      end: '17.09.2019 12:18',
      className: 'positive_period',
      deploy: 0,
      attributes: [
        { name: 'Q_O2', description: 'Расход O2' },
        { name: 'T_ks', description: 'Температура кипящего слоя' },
        { name: 'T_n', description: 'Температура северный циклон' },
        { name: 'T_podsv', description: 'Температура кипящего слоя' },
        { name: 'T_s', description: 'Температура южный циклоня' }
      ]
    },
    {
      shop: 'Roasting_Shop',
      tagName: 'Roast_Oven1',
      shortDesc: 'Обжиговая печь №1',
      beginning: '27.09.2019 08:36',
      end: '28.09.2019 13:32',
      className: 'negative_period',
      deploy: 0,
      attributes: [
        { name: 'Q_O2', description: 'Расход O2' },
        { name: 'T_ks', description: 'Температура кипящего слоя' },
        { name: 'T_n', description: 'Температура северный циклон' },
        { name: 'T_podsv', description: 'Температура кипящего слоя' },
        { name: 'T_s', description: 'Температура южный циклоня' }
      ]
    },


    {
      shop: 'Sulfuric_Acid_Shop',
      tagName: 'Roast_Oven1',
      shortDesc: 'СКЦ Система 3',
      beginning: '27.09.2019 08:36',
      end: '28.09.2019 13:32',
      className: 'negative_period',
      deploy: 0,
      attributes: [
        { name: 'Q_O2', description: 'Расход O2' },
        { name: 'T_ks', description: 'Температура кипящего слоя' }
      ]
    },
  ];

  title = 'База знаний';





  public parameters: InputParameter[];

  public pathUrl: string;
  public treeParametersUrl: string;
  public getHistoryValuesUrl: string;

  public values: Value[];
  public arrayParameters: ArrayParameters[];
  public getHistoryValues: ArrayParameters[];



  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.pathUrl = baseUrl + 'api/PrmValues';
    this.treeParametersUrl = baseUrl + 'api/parameters/getParametersByArea';
    this.getHistoryValuesUrl = baseUrl + 'api/PrmValues/GetHistoryValues';

    http.get<Value[]>(this.pathUrl).subscribe(result => {
      this.values = result;
    }, error => console.error(error));

    http.get<InputParameter[]>(baseUrl + 'api/InputParameters').subscribe(result => {
      this.parameters = result;
    }, error => console.error(error));
  }

  ngOnInit() {

  }






  public triangle: number;
  deploySample() {
    var slides = document.getElementsByClassName('triangle');       // выделяем все указатели "Свернуто/Развернуто"
    var list_prms = document.getElementsByClassName('list_prms');   // выделяем прайсы параметров

    const slide = slides[this.triangle];
    const list = list_prms[this.triangle];

    var className = this.sample[this.triangle].className;

    if (slide instanceof HTMLElement) {
      var deploy = this.sample[this.triangle].deploy;

      if (deploy == 0) {   // если в массиве значение показателя "Свернуто"

        slide.style.borderTop = '12px solid rgba(0, 0, 0, 0)';

        const currentSample = slide.parentNode.parentNode.parentNode;

        if (className == 'positive_period') {
          slide.style.borderBottom = '12px solid rgb(0, 200, 0)';            // переворачиваем треугольник на "Развернуто"
          if (currentSample instanceof HTMLElement) {
            currentSample.style.borderBottom = "solid 1px rgb(0, 200, 0)";   // добавляем нижнюю полосу
          }
        }
        if (className == 'negative_period') {
          slide.style.borderBottom = '12px solid rgb(200, 0, 0)';            // переворачиваем треугольник на "Развернуто"
          if (currentSample instanceof HTMLElement) {
            currentSample.style.borderBottom = "solid 1px rgb(200, 0, 0)";   // добавляем нижнюю полосу
          }
        }

        if (list instanceof HTMLElement) {  // разворачиваем список параметров
          list.style.display = "block";
        } 
        
        this.sample[this.triangle].deploy = 1;
      }

      else {     // если в массиве значение показателя "Развернуто"

        slide.style.borderBottom = '12px solid rgba(0, 0, 0, 0)';

        const currentSample = slide.parentNode.parentNode.parentNode;

        if (className == 'positive_period') {
          slide.style.borderTop = '12px solid rgb(0, 200, 0)';      // переворачиваем треугольник на "Свернуто"
        }
        if (className == 'negative_period') {
          slide.style.borderTop = '12px solid rgb(200, 0, 0)';      // переворачиваем треугольник на "Свернуто"
        }

        if (currentSample instanceof HTMLElement) {  // убираем нижнюю полосу
          currentSample.style.borderBottom = "none";
        }

        if (list instanceof HTMLElement) {  // сворачиваем список параметров
          list.style.display = "none";
        } 

        this.sample[this.triangle].deploy = 0;
      }
    }
  }



  public currentPrm: string = '0';    // развернуть параметры
  deployParameter() {
    var prmDisplay: any = (<HTMLInputElement>document.getElementById('prm_' + this.currentPrm));
    if (prmDisplay.style.display != 'block') {
      (<HTMLInputElement>document.getElementById('prm_' + this.currentPrm)).style.display = 'block';
      (<HTMLInputElement>document.getElementById('prm_triangle_' + this.currentPrm)).innerHTML = 'v'; 
    } else {
      (<HTMLInputElement>document.getElementById('prm_' + this.currentPrm)).style.display = 'none';
      (<HTMLInputElement>document.getElementById('prm_triangle_' + this.currentPrm)).innerHTML = '>';
    }
  }




  getFilterActive($event) {       // кнопки фильтра
    (<HTMLInputElement>document.getElementById("filterActive")).style.right = $event.target.style.right;
    (<HTMLInputElement>document.getElementById("filterActive")).style.background = $event.target.style.background;
    (<HTMLInputElement>document.getElementById("bottomHeader")).style.background = $event.target.style.background;
  }



  hiddenPositive() {   // скрыть позитивные показательные периоды
    var slides = document.getElementsByClassName('positive_period');

    for (let i = 0; i < slides.length; i++) {
      const slide = slides[i].parentNode;

      if (slide instanceof HTMLElement) {
        slide.style.display = "none";
      }
    }
  }
  unHiddenPositive() {  // вернуть позитивные показательные периоды
    var slides = document.getElementsByClassName('positive_period');

    for (let i = 0; i < slides.length; i++) {
      const slide = slides[i].parentNode;

      if (slide instanceof HTMLElement) {
        slide.style.display = "block";
      }
    }
  }

  hiddenNegative() {   // скрыть негативные показательные периоды
    var slides = document.getElementsByClassName('negative_period');

    for (let i = 0; i < slides.length; i++) {
      const slide = slides[i].parentNode;

      if (slide instanceof HTMLElement) {
        slide.style.display = "none";
      }
    }
  }
  unHiddenNegative() {   // вернуть негативные показательные периоды
    var slides = document.getElementsByClassName('negative_period');

    for (let i = 0; i < slides.length; i++) {
      const slide = slides[i].parentNode;

      if (slide instanceof HTMLElement) {
        slide.style.display = "block";
      }
    }
  }


}






interface InputParameter {
  id: string;
  url: number;
  name: number;
  description: string;
  min: number;
  max: number;
}


interface Value {
  id: number;
  dt: string;
  value1: number;
  value2: number;
  value3: number;
  value4: number;
}

interface ArrayParameters {
  shortDesc: string;
  attributes: any;
  tagName: string;
  description: string;
  length: number;
}
