import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ks2Component } from './ks2.component';

describe('Ks2Component', () => {
  let component: Ks2Component;
  let fixture: ComponentFixture<Ks2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ks2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ks2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
