import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ks1Component } from './ks1.component';

describe('Ks1Component', () => {
  let component: Ks1Component;
  let fixture: ComponentFixture<Ks1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ks1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ks1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
