import { Component, HostListener, OnInit, Inject, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TreeParams } from "../../../core/models/TreeParams";
import { Ks1Component } from './obj/ks1/ks1.component';
import { Ks2Component } from './obj/ks2/ks2.component';
import { ServicesComponent } from './services/services.component';


import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as d3Path from 'd3-path';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],

})


export class HomeComponent implements OnInit {


  @ViewChild(Ks1Component, { static: true })
  public ks1: Ks1Component;

  @ViewChild(Ks2Component, { static: true })
  public ks2: Ks2Component;


  public blockAnimation: boolean = false;  // блокировка анимации

  switchBlockAnimation(x: any) {  // метод вызываемый из дочернего компонента close() - запуск анимации в родительском
    this.blockAnimation = x;
    this.startStopAnimation();
  }



  public currentDateTime: any;   // текущее время
  public lastDateTime: any;      // час назад

  public data: any[] = [];

  public getHistoryValuesUrl: string;
  public getHistoryValues: ArrayParameters[];

  public historyValues: any[] = [];
  public arrayForChart: any[] = [
  ];       // выводим массив значений времени для вертикальной шкалы

  public arrayForVysh: any[] = [];

  public arrayParameters: any[] = [
    'Roasting_Shop.F_sum_sm'
  ];



  // переменные текстовых svg
  public ks1_Q_air: string = '##.##';
  public ks1_Q_O2: string = '##.##';

  public ks2_Q_air: string = '##.##';
  public ks2_Q_O2: string = '##.##';

  public ks3_Q_air: string = '##.##';
  public ks3_Q_O2: string = '##.##';

  public ks4_Q_air: string = '##.##';
  public ks4_Q_O2: string = '##.##';

  public ks5_Q_air: string = '##.##';
  public ks5_Q_O2: string = '##.##';

  public vp1_T_vg: string = '##.##';
  public vp2_T_vg: string = '##.##';
  public vp3_T_vg: string = '##.##';
  public vp4_T_vg: string = '##.##';
  public vp5_T_vg: string = '##.##';
  public vp6_T_vg: string = '##.##';




  @HostListener('mousewheel', ['$event'])  // зуммирование экрана колесиком при полноэкранном режиме
  onMouseWheel(e) {
    if ((<HTMLInputElement>document.getElementById("deployButton")).className == 'unDeployButton') {
      var delta = e.deltaY || e.detail || e.wheelDelta;
      var iFrameWidth: any = (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width;
      iFrameWidth = Number(iFrameWidth.slice(0, iFrameWidth.length - 1))
      if (delta < 0) {
        if (iFrameWidth < 300) {
          iFrameWidth = iFrameWidth + 10;
          (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = iFrameWidth + '%';
        }
      } else {
        if (iFrameWidth > 100) {
          iFrameWidth = iFrameWidth - 10;
          (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = iFrameWidth + '%';
        }
      }
    }

    this.chartScale();
  }

  @HostListener('window:resize', ['$event'])   // при изменении размера окна браузера - масштабируем  диаграммы
  onResize() {
    this.chartScale();
  }



  public InTouchWebWidth: any;    // зуммирование экрана регулятором при полноэкранном режиме
  getRegulatorWidth() {
    if ((<HTMLInputElement>document.getElementById("deployButton")).className == "unDeployButton") {
      this.InTouchWebWidth = (<HTMLInputElement>document.getElementById("regulatorWidth")).value;
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = this.InTouchWebWidth + '%';
    }

    this.chartScale();
  }


  private baseUrl: string;
  private serv: any;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, public tree: TreeParams, public Serv: ServicesComponent) {
    this.baseUrl = baseUrl;
    this.serv = Serv;
    this.getHistoryValuesUrl = this.baseUrl + 'api/PrmValues/GetHistoryValues';

  }



  public equipmentElements: Animate[] = [

    { id: "ks1", counter: 0, max: 5, interval: 300, boolean: true, run: false},
    { id: "ks2", counter: 4, max: 5, interval: 250, boolean: false, run: false },
    { id: "ks3", counter: 3, max: 5, interval: 230, boolean: true, run: false },
    { id: "ks4", counter: 1, max: 5, interval: 220, boolean: true, run: false },
    { id: "ks5", counter: 2, max: 5, interval: 200, boolean: true, run: false },

    { id: "vp1", counter: 0, max: 5, interval: 350, boolean: true, run: false },
    { id: "vp2", counter: 1, max: 5, interval: 320, boolean: false, run: false },
    { id: "vp3", counter: 2, max: 5, interval: 340, boolean: true, run: false },
    { id: "vp4", counter: 3, max: 5, interval: 330, boolean: false, run: false },
    { id: "vp5", counter: 4, max: 5, interval: 300, boolean: true, run: false },
    { id: "vp6", counter: 4, max: 5, interval: 310, boolean: true, run: false },

    { id: "larox1", counter: 0, max: 1, interval: 1000, boolean: false, run: false },
    { id: "larox2", counter: 0, max: 1, interval: 1100, boolean: true, run: false },
    { id: "larox3", counter: 0, max: 1, interval: 1050, boolean: false, run: false },
    { id: "larox4", counter: 0, max: 1, interval: 1030, boolean: true, run: false },
    { id: "larox5", counter: 0, max: 1, interval: 1090, boolean: true, run: false },
    { id: "larox6", counter: 0, max: 1, interval: 1010, boolean: true, run: false },
    { id: "larox7", counter: 0, max: 1, interval: 1030, boolean: true, run: false },
    { id: "larox8", counter: 0, max: 1, interval: 1080, boolean: false, run: false },

    { id: "pump1", counter: 0, max: 2, interval: 100, boolean: true, run: false },
    { id: "pump2", counter: 0, max: 2, interval: 108, boolean: true, run: false },
    { id: "pump3", counter: 0, max: 2, interval: 112, boolean: true, run: false },
    { id: "pump4", counter: 0, max: 2, interval: 120, boolean: true, run: false },
    { id: "pump5", counter: 0, max: 2, interval: 90, boolean: false, run: false },
    { id: "pump6", counter: 0, max: 2, interval: 90, boolean: true, run: false },
    { id: "pump7", counter: 0, max: 2, interval: 90, boolean: true, run: false },
    { id: "pump8", counter: 0, max: 2, interval: 90, boolean: true, run: false },
    { id: "pump9", counter: 0, max: 2, interval: 90, boolean: true, run: false },
    { id: "pump10", counter: 0, max: 2, interval: 90, boolean: true, run: false },
    { id: "pump11", counter: 0, max: 2, interval: 105, boolean: true, run: false },
    { id: "pump12", counter: 0, max: 2, interval: 115, boolean: true, run: false },

    { id: "pump90_1", counter: 0, max: 2, interval: 125, boolean: true, run: false },
    { id: "pump90_2", counter: 0, max: 2, interval: 120, boolean: true, run: false },
    { id: "pump90_3", counter: 0, max: 2, interval: 120, boolean: true, run: false },
    { id: "pump90_4", counter: 0, max: 2, interval: 120, boolean: true, run: false },
    { id: "pump90_5", counter: 0, max: 2, interval: 120, boolean: true, run: false },
    { id: "pump90_6", counter: 0, max: 2, interval: 120, boolean: true, run: false },
    { id: "pump90_7", counter: 0, max: 2, interval: 120, boolean: true, run: false },
    { id: "pump90_8", counter: 0, max: 2, interval: 120, boolean: true, run: false },
    { id: "pump90_9", counter: 0, max: 2, interval: 120, boolean: true, run: false },
    { id: "pump90_10", counter: 0, max: 2, interval: 120, boolean: false, run: false },
    { id: "pump90_11", counter: 0, max: 2, interval: 120, boolean: true, run: false },
    { id: "pump90_12", counter: 0, max: 2, interval: 120, boolean: true, run: false },

    { id: "thickener1", counter: 0, max: 8, interval: 800, boolean: true, run: false },
    { id: "thickener2", counter: 1, max: 8, interval: 810, boolean: false, run: false },
    { id: "thickener3", counter: 2, max: 8, interval: 780, boolean: true, run: false },
    { id: "thickener4", counter: 3, max: 8, interval: 815, boolean: true, run: false },
    { id: "thickener5", counter: 4, max: 8, interval: 790, boolean: true, run: false },
    { id: "thickener5_1", counter: 5, max: 8, interval: 805, boolean: false, run: false },
    { id: "thickener5_2", counter: 5, max: 8, interval: 805, boolean: true, run: false },

    { id: "water1", counter: 0, max: 3, interval: 290, boolean: true, run: false },
    { id: "water2", counter: 0, max: 3, interval: 350, boolean: true, run: false },
    { id: "water3", counter: 0, max: 3, interval: 320, boolean: true, run: false },
    { id: "water4", counter: 0, max: 3, interval: 310, boolean: true, run: false },
    { id: "water5", counter: 0, max: 3, interval: 360, boolean: true, run: false },
    { id: "water6", counter: 0, max: 3, interval: 330, boolean: true, run: false },
    { id: "water7", counter: 0, max: 3, interval: 326, boolean: true, run: false },
    { id: "water8", counter: 0, max: 3, interval: 315, boolean: true, run: false },

    { id: "induction_bake1", counter: 0, max: 13, interval: 580, boolean: true, run: false },
    { id: "induction_bake2", counter: 0, max: 13, interval: 600, boolean: true, run: false },
    { id: "induction_bake3", counter: 0, max: 13, interval: 500, boolean: false, run: false },
  ];




  setUp() {
    this.equipmentElements[0].boolean = true;
    this.equipmentElements[1].boolean = true;
    this.equipmentElements[7].boolean = true;
    this.startStopAnimation();

    // если это значение true - стартует анимация в дочернем компоненте
    this.ks1.boolean = this.equipmentElements[0].boolean;
    if (this.ks1.onShow == true) {
      this.ks1.startAnimation();
    }

    // если это значение true - стартует анимация в дочернем компоненте
    this.ks2.boolean = this.equipmentElements[0].boolean;
    if (this.ks2.onShow == true) {
      this.ks2.startAnimation();
    }
  }

  setDown() {
    this.equipmentElements[0].boolean = false;
    this.equipmentElements[1].boolean = false;
    this.equipmentElements[7].boolean = false;
    this.startStopAnimation();

    if (this.equipmentElements[0].boolean == false) {
      this.ks1.stopAnimation();
    }

    if (this.equipmentElements[0].boolean == false) {
      this.ks2.stopAnimation();
    }
  }





  private chartLeft: number;
  private chartTop: number;



  private arrayForSvg: arrayForSvg[] = [   // массив параметров для вывода графиков
    {
      svgClass: '.Obj',
      sidesVar: '',
      svgVar: '',
      arrayForAxis: [],
      charts: [
        { request: 'Roasting_Shop.F_sum_sm', stroke: '#feb41b', fill: 'rgba(255, 150, 0, 0.1)', arrayForChart: [] }
      ]
    },
    {
      svgClass: '.Vysh',
      sidesVar: '',
      svgVar: '',
      arrayForAxis: [],
      charts: [
        { request: 'Leaching_Shop.OE', stroke: '#feb41b', fill: 'rgba(255, 150, 0, 0.1)', arrayForChart: [] }
      ]
    },
    {
      svgClass: '.Velc',
      sidesVar: '',
      svgVar: '',
      arrayForAxis: [],
      charts: [
        { request: 'Waelz_Shop.RVO', stroke: 'rgb(255, 0, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'Waelz_Shop.Kr26', stroke: 'rgb(0, 255, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'Waelz_Shop.Kr27', stroke: 'rgb(0, 255, 255)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'Waelz_Shop.Kr36', stroke: 'rgb(255, 255, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] }
      ]
    },
    {
      svgClass: '.Gidromet',
      sidesVar: '',
      svgVar: '',
      arrayForAxis: [],
      charts: [
        { request: 'Hydro_Metallurgical_Shop.Q_vss2', stroke: 'rgb(255, 0, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'Hydro_Metallurgical_Shop.RIR', stroke: 'rgb(0, 255, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] }
      ]
    },
    {
      svgClass: '.KEC',
      sidesVar: '',
      svgVar: '',
      arrayForAxis: [],
      charts: [
        { request: 'Zinc_Electrolysis_Shop.R_after-510A', stroke: 'rgb(255, 0, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'Zinc_Electrolysis_Shop.R_after-510B', stroke: 'rgb(0, 255, 0)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] },
        { request: 'Zinc_Electrolysis_Shop.R_after-703А-B', stroke: 'rgb(0, 255, 255)', fill: 'rgba(0, 0, 0, 0)', arrayForChart: [] }
      ]
    },
  ];

  private buildSvg(shopNumber) {

    this.arrayForSvg[shopNumber].sidesVar = d3.select(this.arrayForSvg[shopNumber].svgClass)
      .append('g')
      .attr('transform', 'translate(' + 0 + ',' + 0 + ')');

    this.arrayForSvg[shopNumber].sidesVar.append("line")              // линия top
      .attr('stroke', 'rgb(230, 230, 230)')
      .attr('stroke-width', this.chartStrokeWidth * 1)
      .attr("x1", 0)
      .attr("y1", 0)
      .attr("x2", 100 + '%')
      .attr("y2", 0);

    this.arrayForSvg[shopNumber].sidesVar.append("line")              // линия bottom
      .attr('stroke', 'rgb(230, 230, 230)')
      .attr('stroke-width', this.chartStrokeWidth * 1)
      .attr("x1", 0)
      .attr("y1", 100 + '%')
      .attr("x2", 100 + '%')
      .attr("y2", 100 + '%');

    this.arrayForSvg[shopNumber].sidesVar.append("line")              // линия left
      .attr('stroke', 'rgb(230,230, 230)')
      .attr('stroke-width', this.chartStrokeWidth * 1)
      .attr("x1", 0)
      .attr("y1", 0)
      .attr("x2", 0)
      .attr("y2", 100 + '%');

    this.arrayForSvg[shopNumber].sidesVar.append("line")              // линия right
      .attr('stroke', 'rgb(230, 230, 230)')
      .attr('stroke-width', this.chartStrokeWidth * 1)
      .attr("x1", 100 + '%')
      .attr("y1", 0)
      .attr("x2", 100 + '%')
      .attr("y2", 100 + '%');

    this.arrayForSvg[shopNumber].svgVar = d3.select(this.arrayForSvg[shopNumber].svgClass)
      .append('g')
      .attr('transform', 'translate(' + this.chartLeft + ',' + this.chartTop + ')');
  }




  private x: any;
  private y: any;

  private width: number;
  private height: number;
  private fontSize: number;
  private chartStrokeWidth: number;
  private tickStrokeWidth: number;



  private activeAddXandYAxis(shopNumber) {
    // range of data configuring
    this.x = d3Scale.scaleTime().range([0, this.width]);
    this.y = d3Scale.scaleLinear().range([this.height, 0]);
    this.x.domain(d3Array.extent(this.arrayForSvg[shopNumber].arrayForAxis, (d) => d.dt));
    this.y.domain(d3Array.extent(this.arrayForSvg[shopNumber].arrayForAxis, (d) => d.val));

    // Configure the X Axis
    this.arrayForSvg[shopNumber].svgVar.append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .attr('stroke-width', this.tickStrokeWidth)
      .attr('style', 'stroke-opacity: 1')
      .attr('style', 'font-size: ' + this.fontSize + 'px')
      .call(d3Axis.axisBottom(this.x)
        .tickSize(-this.height)
        .ticks(5));

    // Configure the Y Axis
    this.arrayForSvg[shopNumber].svgVar.append('g')
      .attr('style', 'stroke-opacity: 1')
      .attr('style', 'font-size: ' + this.fontSize + 'px')
      .attr('stroke-width', this.tickStrokeWidth)
      .call(d3Axis.axisLeft(this.y)
        .tickSize(-this.width)
        .ticks(4))
  }


  private line: d3Shape.Line<[number, number]>; // this is line defination

  activeLineAndPathObj() {        // вывод графиков

    for (let i = 0; i < this.arrayForSvg.length; i++) {

      this.activeAddXandYAxis(i);

      for (let chartCount = 0; chartCount < this.arrayForSvg[i].charts.length; chartCount++) {

        var multiplier: number;
        if (this.arrayForSvg[i].charts[chartCount].fill != 'rgba(0, 0, 0, 0)') {
          multiplier = 1;
        } else {
          multiplier = 2;   // если график имеет прозрачную заливку увеличиваем толщину контура
        }

        this.line = d3Shape.line()
          .x((d: any) => this.x(d.dt))
          .y((d: any) => this.y(d.val));
        // Configuring line path
        this.arrayForSvg[i].svgVar.append('path')
          .datum(this.arrayForSvg[i].charts[chartCount].arrayForChart)
          .attr('stroke', this.arrayForSvg[i].charts[chartCount].stroke)
          .attr('fill', this.arrayForSvg[i].charts[chartCount].fill)
          .attr('stroke-width', this.chartStrokeWidth * multiplier)
          .attr('d', this.line);
      }
    }

  }



  ngOnInit() {

    this.startStopAnimation();
    for (let i = 0; i < this.arrayForSvg.length; i++) {
      this.buildSvg(i);
    }
    this.chartScale();
    this.chartLoad();

  }

  chartScale() {                                                                    // метод масштабирования графиков
    var svgTemplate = (<HTMLInputElement>document.getElementById('obj'));
    this.width = svgTemplate.clientWidth - (svgTemplate.clientWidth * 0.175);
    this.height = svgTemplate.clientWidth * 0.35;
    this.chartLeft = svgTemplate.clientWidth * 0.116;
    this.chartTop = svgTemplate.clientWidth * 0.058;
    this.fontSize = svgTemplate.clientWidth * 0.032;
    this.chartStrokeWidth = svgTemplate.clientWidth / 340;
    this.tickStrokeWidth = svgTemplate.clientWidth / 800;

    if (this.arrayForSvg[0].charts[0].arrayForChart.length > 2) {

      for (let i = 0; i < this.arrayForSvg.length; i++) {
        this.arrayForSvg[i].svgVar.selectAll("*").remove();
        this.arrayForSvg[i].sidesVar.selectAll("*").remove();

        this.buildSvg(i);
      }

      this.activeLineAndPathObj();

      this.coloringAxis();
    }
  }




  chartLoad() {       // загрузка данных для графиков
    var that = this;

    // this.serv.runChartLoad++;

    var currentUrl: any = document.location.href;
    currentUrl = currentUrl.split('/');

    if (this.serv.runChartLoad > 1) {
      this.serv.runChartLoad--;
    } else {
      if (currentUrl.length < 5) {

        this.getHistoryValuesUrl = this.baseUrl + 'api/PrmValues/GetHistoryValues';

        this.currentDateTime = new Date();        // Выводим текущую дату
        var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));
        this.lastDateTime = new Date(this.currentDateTime - eraUnix - 3600000);

        this.lastDateTime = this.lastDateTime.getFullYear() + '-' + ('0' + (this.lastDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + this.lastDateTime.getDate()).slice(-2) + ' ' + ('0' + this.lastDateTime.getHours()).slice(-2) + ':' + ('0' + this.lastDateTime.getMinutes()).slice(-2);
        this.currentDateTime = this.currentDateTime.getFullYear() + '-' + ('0' + (this.currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + this.currentDateTime.getDate()).slice(-2) + ' ' + ('0' + this.currentDateTime.getHours()).slice(-2) + ':' + ('0' + this.currentDateTime.getMinutes()).slice(-2);


        this.getHistoryValuesUrl = this.getHistoryValuesUrl + '/' + this.arrayParameters[0] + '/' + this.lastDateTime + '/' + this.currentDateTime;


        this.http.get<ArrayParameters[]>(this.getHistoryValuesUrl).subscribe(result => {

          this.getHistoryValues = result;

          this.arrayForSvg[0].svgVar.selectAll("*").remove();
          this.arrayForSvg[0].sidesVar.selectAll("*").remove();
          this.arrayForSvg[1].svgVar.selectAll("*").remove();
          this.arrayForSvg[1].sidesVar.selectAll("*").remove();
          this.arrayForChart = [];

          var min: number = 1000000;

          for (let i = 0; i < this.getHistoryValues['Roasting_Shop.F_sum_sm'].length; i++) {
            if (min > this.getHistoryValues['Roasting_Shop.F_sum_sm'][i].val) {
              min = this.getHistoryValues['Roasting_Shop.F_sum_sm'][i].val;
            }
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues['Roasting_Shop.F_sum_sm'][i].dt), val: this.getHistoryValues['Roasting_Shop.F_sum_sm'][i].val });
          }

          this.arrayForChart.unshift({ dt: this.arrayForChart[0].dt, val: min });
          this.arrayForChart.push({ dt: this.arrayForChart[this.arrayForChart.length - 1].dt, val: min });

          this.buildSvg(0);
          this.activeLineAndPathObj();

          this.coloringAxis();

        }, error => {

            console.error(error);

            if (this.arrayForSvg[0].charts[0].arrayForChart.length == 0) {
              this.http.get<ArrayParameters[]>('assets/replase_server/historyValues/Main.json').subscribe((result) => {

                this.getHistoryValues = result;

                this.bodyHttp();

              });
            } else {

              // процедура циклической прокрутки графиков

              for (let j = 0; j < this.arrayForSvg.length; j++) {

                this.arrayForSvg[j].svgVar.selectAll("*").remove();

                for (let chartCount = 0; chartCount < this.arrayForSvg[j].charts.length; chartCount++) {

                  if (this.arrayForSvg[j].charts[chartCount].fill != 'rgba(0, 0, 0, 0)') { // если график имеет непрозрачную заливку двигаем цикл со второй по предпоследнюю точку

                    var bufer = this.arrayForSvg[j].charts[chartCount].arrayForChart[1].val;
                    for (let i = 0; i < this.arrayForSvg[j].charts[chartCount].arrayForChart.length; i++) {
                      if (i > 0 && i < this.arrayForSvg[j].charts[chartCount].arrayForChart.length - 2) {
                        this.arrayForSvg[j].charts[chartCount].arrayForChart[i].val = this.arrayForSvg[j].charts[0].arrayForChart[i + 1].val;
                      }
                    }
                    this.arrayForSvg[j].charts[chartCount].arrayForChart[this.arrayForSvg[j].charts[chartCount].arrayForChart.length - 2].val = bufer;

                  } else {

                    var bufer = this.arrayForSvg[j].charts[chartCount].arrayForChart[0].val;
                    for (let i = 0; i < this.arrayForSvg[j].charts[chartCount].arrayForChart.length; i++) {
                      if (i < this.arrayForSvg[j].charts[chartCount].arrayForChart.length - 1) {
                        this.arrayForSvg[j].charts[chartCount].arrayForChart[i].val = this.arrayForSvg[j].charts[chartCount].arrayForChart[i + 1].val;
                      }
                    }
                    this.arrayForSvg[j].charts[chartCount].arrayForChart[this.arrayForSvg[j].charts[chartCount].arrayForChart.length - 1].val = bufer;

                  }

                }


              }

              this.activeLineAndPathObj();
              this.coloringAxis();

            }
        });

       
        setTimeout(function () {
          that.chartLoad();
        }, 3000); 
      }
    }
  }



  bodyHttp() {  // метод формирования массива данных для граффического представления

    for (let j = 0; j < this.arrayForSvg.length; j++) {

      this.arrayForSvg[j].svgVar.selectAll("*").remove();
      this.arrayForSvg[j].sidesVar.selectAll("*").remove();

      var min: number = 1000000;
      var max: number = 0;

      for (let chartCount = 0; chartCount < this.arrayForSvg[j].charts.length; chartCount++) {  // цикл вывода массивов для графиков (их может оказаться несколько в одной системе координат)
        this.arrayForSvg[j].charts[chartCount].arrayForChart = [];

        for (let i = 0; i < this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request].length; i++) {
          if (min > this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request][i].val) {
            min = this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request][i].val;
          }
          if (max < this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request][i].val) {
            max = this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request][i].val;
          }
         
          this.arrayForSvg[j].charts[chartCount].arrayForChart.push({ dt: new Date(this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request][i].dt), val: this.getHistoryValues[this.arrayForSvg[j].charts[chartCount].request][i].val });
        }
        if (this.arrayForSvg[j].charts[chartCount].fill != 'rgba(0, 0, 0, 0)') {  // если график имеет непрозрачную заливку добавляем нулевые точки в начале и в конце
          this.arrayForSvg[j].charts[chartCount].arrayForChart.unshift({ dt: this.arrayForSvg[j].charts[chartCount].arrayForChart[0].dt, val: min });
          this.arrayForSvg[j].charts[chartCount].arrayForChart.push({ dt: this.arrayForSvg[j].charts[chartCount].arrayForChart[this.arrayForSvg[j].charts[chartCount].arrayForChart.length - 1].dt, val: min });
        }

      }
      // задаем массив для координатных осей с начальными и конечными значениями
      this.arrayForSvg[j].arrayForAxis.push({ dt: this.arrayForSvg[j].charts[0].arrayForChart[0].dt, val: min }, { dt: this.arrayForSvg[j].charts[0].arrayForChart[this.arrayForSvg[j].charts[0].arrayForChart.length-1].dt, val: max});

      this.buildSvg(j);     
    }


    this.activeLineAndPathObj();

    this.coloringAxis();

  }



  coloringAxis() {
    // процедура перекрашивания координатных осей
    var ticks = document.getElementsByClassName('tick');

    for (let i = 0; i < ticks.length; i++) {
      ticks[i].children[0].attributes['stroke'].value = '#ff7f2aff';    // окрашивание сетки
      ticks[i].children[1].attributes['fill'].value = '#fff';           // окрашивание текстов
    }

    var domain = document.getElementsByClassName('domain');             // окрашивание краев
    for (let i = 0; i < domain.length; i++) {
      domain[i].attributes['stroke'].value = '#ff7f2aff';
    }
  }







  startStopAnimation() {
    if (this.blockAnimation == false)   // если нет блокировки анимации
    {

      for (let i = 0; i < this.equipmentElements.length; i++) {
        var allChildrens: any = (<HTMLInputElement>document.getElementById(this.equipmentElements[i].id));  // выбираем элемент для анимации

        if (this.equipmentElements[i].boolean == true) {   // если объект оборудования включен...
          if (this.equipmentElements[i].run != true) {  // ...но еще не запущена раскадровка

            allChildrens.children[this.equipmentElements[i].counter].style.display = 'block';  // раскрываем стартовый кадр
            allChildrens.children[allChildrens.children.length - 1].style.display = 'none';  // скрываем стоп-кадр

            // запускаем раскадровку
            this.storyboard(this.equipmentElements[i].id, this.equipmentElements[i].counter, this.equipmentElements[i].max, this.equipmentElements[i].interval, i);
            this.equipmentElements[i].run = true;  // отмечаем, что раскадровка уже запущена
          }

        } else {     // если объект оборудования выключен

          for (let j = 0; j < allChildrens.children.length; j++) {  // скрываем все действующие кадры
            allChildrens.children[j].style.display = 'none';
          }
          allChildrens.children[allChildrens.children.length - 1].style.display = 'block';  // раскрываем стоп-кадр

          this.equipmentElements[i].run = false;  // отмечаем, что раскадровка остановлена
        }
      }

    } else {  // если блокировка анимации установлена

      for (let i = 0; i < this.equipmentElements.length; i++) {
        var allChildrens: any = (<HTMLInputElement>document.getElementById(this.equipmentElements[i].id));  // выбираем элемент для анимации

        for (let j = 0; j < allChildrens.children.length; j++) {  // скрываем все действующие кадры
          allChildrens.children[j].style.display = 'none';
        }
        allChildrens.children[allChildrens.children.length - 1].style.display = 'block';  // раскрываем стоп-кадр

        this.equipmentElements[i].run = false;  // отмечаем, что раскадровка остановлена
      }

    }
  }


  storyboard(id, counter, max, interval, index) {  // метод раскадровки
    var that = this;

    if (this.blockAnimation == false) {
      if (this.equipmentElements[index].boolean == true) {

        var allChildrens: any = (<HTMLInputElement>document.getElementById(id));
        if (counter < max) {
          allChildrens.children[counter].style.display = 'none';
          allChildrens.children[counter + 1].style.display = 'block';
          counter++;
        } else {
          allChildrens.children[counter].style.display = 'none';
          allChildrens.children[0].style.display = 'block';
          counter = 0;
        }

        setTimeout(function () {
          that.storyboard(id, counter, max, interval, index);
        }, interval);
      }
    }

  }




  showKs1() {
    (<HTMLInputElement>document.getElementById("InTouchWeb")).style.display = 'none';
    (<HTMLInputElement>document.getElementById("showKs1")).style.display = 'block';

    this.ks1.onShow = true; // отмечаем, что окно подраздела открыто для контроля анимации в нем

    // если это значение true - стартует анимация в дочернем компоненте
    this.ks1.boolean = this.equipmentElements[0].boolean;  
    this.ks1.startAnimation();  

    this.blockAnimation = true;
    this.startStopAnimation();
  }

  showKs2() {
    (<HTMLInputElement>document.getElementById("InTouchWeb")).style.display = 'none';
    (<HTMLInputElement>document.getElementById("showKs2")).style.display = 'block';

    this.ks2.onShow = true; // отмечаем, что окно подраздела открыто для контроля анимации в нем

    // если это значение true - стартует анимация в дочернем компоненте
    this.ks2.boolean = this.equipmentElements[1].boolean;
    this.ks2.startAnimation();

    this.blockAnimation = true;
    this.startStopAnimation();
  }




  deployScheme() {
    if ((<HTMLInputElement>document.getElementById("deployButton")).className == "deployButton") {
      (<HTMLInputElement>document.getElementById("regulatorWidth")).style.display = 'block';
      (<HTMLInputElement>document.getElementById("schemeWindow")).style.position = "fixed";
      (<HTMLInputElement>document.getElementById("schemeWindow")).style.zIndex = "997";
      (<HTMLInputElement>document.getElementById("deployButton")).className = "unDeployButton";
    } else {
      (<HTMLInputElement>document.getElementById("regulatorWidth")).style.display = 'none';
      (<HTMLInputElement>document.getElementById("schemeWindow")).style.position = "relative";
      (<HTMLInputElement>document.getElementById("schemeWindow")).style.zIndex = "1";
      (<HTMLInputElement>document.getElementById("deployButton")).className = "deployButton";
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = '100%';
    }

    this.chartScale();
  }

}


interface arrayForSvg {
  svgClass: string;
  sidesVar: any;
  svgVar: any;
  charts: charts[];
  arrayForAxis: any[];
}

interface charts {
  arrayForChart: any[];
  request: string;
  stroke: string;
  fill: string;
}


interface Animate {
  id: string;
  counter: number;
  max: number;
  interval: number;
  boolean: boolean;
  run: boolean;
}


interface ArrayParameters {
  dt: any;
  shortDesc: string;
  attributes: any;
  tagName: string;
  description: string;
  length: number;
}
