import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
//import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent, ContactUsComponent, HomeComponent, ToolsComponent, Ks1Component } from './pages';

const routes: Routes = [
  { path: 'ks1', component: Ks1Component},
  { path: 'ks2', component: Ks1Component},
  { path: '', component: HomeComponent },
  { path: 'contactus', component: ContactUsComponent },
  { path: 'aboutus', component: AboutUsComponent },
  { path: 'tools', component: ToolsComponent },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HomeRoutingModule { }
