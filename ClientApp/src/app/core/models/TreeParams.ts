import { Injectable } from '@angular/core';

@Injectable()

export class TreeParams {

  public getTreeParametersUrl: string = '';   // строка get-запроса на дерево параметров

  public getArraySelectedPrms: any[] = [];    // массив выбранных параметров

  public getSelectedParameters: any = [];     // список id выделенных check-ов для возврата их выделения при возврате с другого раздела

  public getTreeParameters: any[] = [];

  public markRemoveDateTime: number = 0;        // отметка изменения даты и времени (включая сенсорное масштабирования графика)
  public markRemoveSelectedPrms: number = 0;    // отметка изменения выбора параметров


  toggleMarkRemoveDateTime() {       // переключаем - отметка изменения даты и времени true
    this.markRemoveDateTime = 1;
  }

  unToggleMarkRemoveDateTime() {
    this.markRemoveDateTime = 0;
  }

  toggleMarkRemoveSelectedPrms() {
    this.markRemoveSelectedPrms = 1;
  }

  unToggleMarkRemoveSelectedPrms() {
    this.markRemoveSelectedPrms = 0;
  }


  updateUrlModel() {
    this.getTreeParametersUrl = '';      // обновляем url строку get-запроса в модели
  }

  updateSelectedPrmsModel() {
    this.updateUrlModel();
    this.getArraySelectedPrms = [];      // опустошаем в модели массив выбранных параметров
    this.getSelectedParameters = [];     // опустошаем в модели массив id выделенных check-ов дерева параметров
  }



  public startDateTime: string;     // дата + время начала
  public stopDateTime: string;      // дата + время завершения



  public zoomMemory: any = [];


  public arrayColoring: any = [
    '#ff0000',
    '#008000',
    '#ff6600',
    '#008080',
    '#ffcc00',
    '#0000ff',
    '#800080',
    '#89a02c',
    '#9955ff',
    '#d38d5f',
    '#ff00ff',
    '#00d4aa',
    '#003380',
    '#d42aff',
    '#ff0000',
    '#008000',
    '#ff6600',
    '#008080',
    '#ffcc00',
    '#0000ff',
    '#800080',
    '#89a02c',
    '#9955ff',
    '#d38d5f',
    '#ff00ff',
    '#00d4aa',
    '#003380',
    '#d42aff'
  ];



  constructor() { }

}
