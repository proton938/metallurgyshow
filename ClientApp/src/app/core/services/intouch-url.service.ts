import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class IntouchUrlService {

  constructor() { }

  private compChangeSource = new Subject();
  private urlSource = new Subject<String>();

  componentChanged$ = this.compChangeSource.asObservable();
  urlCreated$ = this.urlSource.asObservable();
  refresh() {
    this.compChangeSource.next();
  }
  broadcastUrl(url: String) {
    this.urlSource.next(url);
  }
}
