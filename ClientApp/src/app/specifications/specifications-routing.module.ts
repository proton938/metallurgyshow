import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpecificationsComponent, SpecificationComponent } from './pages';

const routes: Routes = [
  { path: 'specifications', component: SpecificationsComponent },
  { path: 'specification', component: SpecificationComponent },
  { path: 'specification/:id', component: SpecificationComponent },
  { path: 'specifications/:id', component: SpecificationsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpecificationsRoutingModule { }
