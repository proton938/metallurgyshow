import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpecificationsRoutingModule } from './specifications-routing.module';
import { SpecificationsComponent } from './pages/specifications/specifications.component';
import { SpecificationComponent } from './pages/specification/specification.component';

@NgModule({
  imports: [
    CommonModule,
    SpecificationsRoutingModule
  ],
  declarations: [SpecificationsComponent, SpecificationComponent]
})
export class SpecificationsModule { }
