import { Component, Inject, OnInit, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-specifications',
  templateUrl: './specifications.component.html',
  styleUrls: ['./specifications.component.css']
})

@Injectable()

export class SpecificationsComponent implements OnInit {

  title = 'Ввод параметров';

  public parameters: InputParameter[];

  public pathUrl: string;
  public treeParametersUrl: string;
  public getHistoryValuesUrl: string;
  public createParameter: string;

  public values: Value[];
  public arrayParameters: ArrayParameters[];
  public getHistoryValues: ArrayParameters[];

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

    this.pathUrl = baseUrl + 'api/PrmValues';
    this.treeParametersUrl = baseUrl + 'api/parameters/getParametersByArea';
   //  this.getHistoryValuesUrl = baseUrl + 'api/PrmValues/GetHistoryValues';
    this.createParameter = baseUrl + 'api/parameters/CreateOptionsParameter';


    http.get<Value[]>(this.pathUrl).subscribe(result => {
      this.values = result;
    }, error => console.error(error));

    http.get<InputParameter[]>(baseUrl + 'api/InputParameters').subscribe(result => {
      this.parameters = result;
    }, error => console.error(error));

  }

  ngOnInit() {
    this.treeParameters();

    window.addEventListener('scroll', this.scroll, true);
  }

  scroll = (): void => {
    var obj: any = (<HTMLInputElement>document.getElementById("headerTable"));
    var rect: any = obj.getBoundingClientRect();                                                        // абсолютная высота заголовка таблицы
    var client_width = obj.clientWidth;
    if (rect.top < 0) {
      if (document.body.clientWidth >= 1080) {
        (<HTMLInputElement>document.getElementById("headerTableFixed")).style.display = 'table';
        (<HTMLInputElement>document.getElementById("headerTableFixed")).style.left = rect.left + 'px';
        (<HTMLInputElement>document.getElementById("headerTableFixed")).style.width = client_width + 'px';
      }
    } else {
      (<HTMLInputElement>document.getElementById("headerTableFixed")).style.display = 'none';
    }
  };

  


  public idAdd: any;
  public idPrm: any;
  public nameObj: string;         // имя объекта для передачи в CreateOptionsParameter

  openFormAddParameters() {       // открыть форму создания параметра

    var addHeight: string = (<HTMLInputElement>document.getElementById("addParams_" + this.idAdd)).style.height;

    if (addHeight != '150px') {
      for (let j = 0; j <= this.arrayParameters.length - 1; j++) {
        for (let i = 0; i <= this.arrayParameters[j].attributes.length - 1; i++) {
          (<HTMLInputElement>document.getElementById("addParams_" + (j + 1) + '_' + (i + 1))).style.height = 'auto';
        }
      }

      if (document.body.clientWidth >= 1080) {
        var headMenuHeight: any = (<HTMLInputElement>document.getElementById("headMenu")).clientHeight;     // высота верхнего меню для вычетания из абсолютной высоты
        var obj: any = (<HTMLInputElement>document.getElementById("addParams_" + this.idAdd));
        var rect: any = obj.getBoundingClientRect();                                                        // абсолютная высота строки
        document.getElementById("formAddPrm").style.top = pageYOffset + rect.top - headMenuHeight + "px";
        document.getElementById("formAddPrm").style.display = "block";
        (<HTMLInputElement>document.getElementById("addParams_" + this.idAdd)).style.height = 150 + 'px';
        for (let j = 0; j <= this.arrayParameters.length - 1; j++) {
          for (let i = 0; i <= this.arrayParameters[j].attributes.length - 1; i++) {
            (<HTMLInputElement>document.getElementById("addParams_" + (j + 1) + '_' + (i + 1))).innerHTML = '+';
          }
        }
        (<HTMLInputElement>document.getElementById("addParams_" + this.idAdd)).innerHTML = 'V';
      } else {
        document.getElementById("formAddPrm").style.top = 0 + "px";
        document.getElementById("formAddPrm").style.display = "block";
      }

    } else {
      (<HTMLInputElement>document.getElementById('namePrm')).value = '';
      (<HTMLInputElement>document.getElementById('descPrm')).value = '';
      (<HTMLInputElement>document.getElementById('typePrm')).value = 'Integer';
      (<HTMLInputElement>document.getElementById('engUnits')).value = '';
      this.resetArray();

      document.getElementById("formAddPrm").style.top = -200 + "px";
      document.getElementById("formAddPrm").style.display = "none";
      (<HTMLInputElement>document.getElementById("addParams_" + this.idAdd)).style.height = 'auto';
      (<HTMLInputElement>document.getElementById("addParams_" + this.idAdd)).innerHTML = '+';
    }

    this.idPrm = this.idAdd;
    this.idAdd = this.idAdd.split('_');
    this.idAdd = Number(this.idAdd[1]) + 1;
  }



  closeFormAddParameters() {                                        // закрыть форму создания параметра
    (<HTMLInputElement>document.getElementById('namePrm')).value ='';
    (<HTMLInputElement>document.getElementById('descPrm')).value ='';
    (<HTMLInputElement>document.getElementById('typePrm')).value = 'Integer';
    (<HTMLInputElement>document.getElementById('engUnits')).value = '';
    this.resetArray();

    document.getElementById("formAddPrm").style.top = -200 + "px";
    document.getElementById("formAddPrm").style.display = "none";
    for (let i = 1; i <= this.parameters.length + 2; i++) {
      (<HTMLInputElement>document.getElementById("addParams_" + i)).style.height = 'auto';
      (<HTMLInputElement>document.getElementById("addParams_" + i)).innerHTML = '+';
    }
  }


  /*
  private falseArray: any = [];        // фальш-массив
  falseTree() {
    for (let j = 0; j < 11; j++) {
      this.falseArray.push({ tagName: 'rtetre', shortDesk: '543543', attributes: [] });
      for (let i = 0; j < 8; i++) {
        this.falseArray[j].attributes.push({ name: '532trt', description: 'Фальш-параметр' })
      }
    }
  }
  */


  public identif: string = '';
  public styleDisplay: any;

  treeParameters() {
    var currentUrl: any = document.location.href;
    currentUrl = currentUrl.split('/');
    if (currentUrl.length < 5) {
      currentUrl = 'Main';
    } else {
      currentUrl = currentUrl[4];
    }

    var testLoadMenu = (<HTMLInputElement>document.getElementById(currentUrl));    // считываем URL запроса на днрево цеха с боковой панели

    if (testLoadMenu) {
      currentUrl = (<HTMLInputElement>document.getElementById(currentUrl)).value;
      this.treeParametersUrl = this.treeParametersUrl + '/' + currentUrl;

      this.http.get<ArrayParameters[]>(this.treeParametersUrl).subscribe(result => {
        this.arrayParameters = result;
        for (let i = 0; i < this.arrayParameters.length; i++) {      // если имя параметра пустое - заменяем его на системный тег 
          if (this.arrayParameters[i].shortDesc == '') {
            this.arrayParameters[i].shortDesc = this.arrayParameters[i].tagName;
          }
        }
      }, error => {
          console.error(error);

          this.http.get<ArrayParameters[]>('assets/replase_server/' + currentUrl + '.json').subscribe((result) => {
            this.arrayParameters = result;
            for (let i = 0; i < this.arrayParameters.length; i++) {      // если имя параметра пустое - заменяем его на системный тег 
              if (this.arrayParameters[i].shortDesc == '') {
                this.arrayParameters[i].shortDesc = this.arrayParameters[i].tagName;
              }
            }

            // this.treeParams.getTreeParameters = this.arrayParameters;   // передаем массив дерева в модель

          });
      });

    } else {
      currentUrl = 'Chel_Zinc_Plant';
      this.treeParametersUrl = this.treeParametersUrl + '/' + currentUrl;

      this.http.get<ArrayParameters[]>(this.treeParametersUrl).subscribe(result => {
        this.arrayParameters = result;
        for (let i = 0; i < this.arrayParameters.length; i++) {      // если имя параметра пустое - заменяем его на системный тег 
          if (this.arrayParameters[i].shortDesc == '') {
            this.arrayParameters[i].shortDesc = this.arrayParameters[i].tagName;
          }
        }
      }, error => {
          console.error(error);

          this.http.get<ArrayParameters[]>('assets/replase_server/' + currentUrl + '.json').subscribe((result) => {
            this.arrayParameters = result;
            for (let i = 0; i < this.arrayParameters.length; i++) {      // если имя параметра пустое - заменяем его на системный тег 
              if (this.arrayParameters[i].shortDesc == '') {
                this.arrayParameters[i].shortDesc = this.arrayParameters[i].tagName;
              }
            }

            // this.treeParams.getTreeParameters = this.arrayParameters;   // передаем массив дерева в модель

          });
      });
    }
  }


  

  createNewParameter(/*param: OptionsParameter*/) {

    var validField: number = 0;   // валидация полей аттрибутов

    var allFields: any = (<HTMLInputElement>document.getElementById('addFieldArray')).childNodes;

    var values: any = [];
    for (let i = 0; i < allFields.length; i++) {
      var fieldValue: any = (<HTMLInputElement>document.getElementById('addFieldArray')).childNodes[i];
      if (fieldValue.checkValidity()) {
        values.push(fieldValue.value);
      }
      else {
        validField = 1;
      }
    }


    if (validField == 0) {

      var nameObj: string = (<HTMLInputElement>document.getElementById('nameObj')).value;
      var namePrm: string = (<HTMLInputElement>document.getElementById('namePrm')).value;
      var descPrm: string = (<HTMLInputElement>document.getElementById('descPrm')).value;
      var typePrm: string = (<HTMLInputElement>document.getElementById('typePrm')).value;
      var engUnits: string = (<HTMLInputElement>document.getElementById('engUnits')).value;

      if (namePrm == '' || descPrm == '') {
        { alert('Не введено имя или описание'); }
      } else {

        if (values.length == 0) {      // если не введено ни одного значения
          if (typePrm != 'String') {
            values.push('0');
          } else {
            values.push('');
          }
        }

        const body = {
          objName: nameObj,
          paramName: namePrm,
          paramDesc: descPrm,
          type: typePrm,
          engUnits: engUnits,
          trendHigh: `1`,
          trendLow: `0`,
          values: values
        };

        console.log(body);

        return this.http.post<OptionsParameter[]>(`http://localhost:5001/api/parameters/CreateOptionsParameter`, body).subscribe(data => {
          alert('Успешно = ' + data);
          console.log(data);
        }, error => {
          if (error.error.text != undefined) {
            alert('Параметр: "' + namePrm + '" в объекте: "' + nameObj + '" успешно создан ');
            console.dir(error);

            (<HTMLInputElement>document.getElementById('namePrm')).value = '';
            (<HTMLInputElement>document.getElementById('descPrm')).value = '';
            (<HTMLInputElement>document.getElementById('typePrm')).value = 'Integer';
            (<HTMLInputElement>document.getElementById('engUnits')).value = '';
            this.resetArray();

            document.getElementById("formAddPrm").style.top = -200 + "px";
            document.getElementById("formAddPrm").style.display = "none";
            (<HTMLInputElement>document.getElementById("addParams_" + this.idPrm)).style.height = 'auto';
            (<HTMLInputElement>document.getElementById("addParams_" + this.idPrm)).innerHTML = '+';

          } else {
            alert('Не удалось создать параметр "' + namePrm + '" ! Возможно открыто окно объекта "' + nameObj + '" в программе ArchestrA IDE');
            console.dir(error);
          }
        });

      } 

    } else {
      alert('Введенные данные не соответствуют выбранному типу!');
    }

  }




  unHideTree() {
    for (let j = 0; j <= this.arrayParameters.length - 1; j++) {
      for (let i = 0; i <= this.arrayParameters[j].attributes.length - 1; i++) {
        (<HTMLInputElement>document.getElementById("addParams_" + (j + 1) + '_' + (i + 1))).style.height = 'auto';
        (<HTMLInputElement>document.getElementById("addParams_" + (j + 1) + '_' + (i + 1))).innerHTML = '+';
      }
    }
    document.getElementById("formAddPrm").style.top = -200 + "px";
    document.getElementById("formAddPrm").style.display = "none";

    this.styleDisplay = (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display;
    if (this.styleDisplay == 'none') {
      (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display = 'table';
      (<HTMLInputElement>document.getElementById('arrowOne_' + this.identif)).innerHTML = 'v';
    } else {
      (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display = 'none';
      (<HTMLInputElement>document.getElementById('arrowOne_' + this.identif)).innerHTML = '>';
    }
  }





  // добавление полей массива


  addFieldArray() {

    var allFieldsValues: any = [];

    var allFields: any = (<HTMLInputElement>document.getElementById('addFieldArray')).childNodes;
    for (let i = 0; i < allFields.length; i++) {
      var fieldPattern: any = (<HTMLInputElement>document.getElementById('addFieldArray')).childNodes[i];
      allFieldsValues.push(fieldPattern.value);
    }

    var numberField: any = (<HTMLInputElement>document.getElementById('addFieldArray')).childNodes.length;
    numberField = Number(numberField) + 1;

    (<HTMLInputElement>document.getElementById('addFieldArray')).innerHTML += '<input type="text" style="margin-top: 10px; margin-left: -10px; border: solid 1px #feb41b; width: 280px; display: block;" placeholder="' + numberField + '" pattern="^[ 0-9]+$" required >';
    (<HTMLInputElement>document.getElementById('addFieldArray')).style.display = 'block';

    (<HTMLInputElement>document.getElementById('resetArray')).style.display = 'block';
    if (document.body.clientWidth > 1080) {
      (<HTMLInputElement>document.getElementById('arrayButton')).style.boxShadow = '0 0 10px rgba(0,0,0,0.1)';
    }

    for (let i = 0; i < allFields.length-1; i++) {
      var fieldPattern: any = (<HTMLInputElement>document.getElementById('addFieldArray')).childNodes[i];
      if (allFieldsValues[i] != '') {
        fieldPattern.value = allFieldsValues[i];
      }
    }
  }



  // переключение типа
  patternExpression(expression) {
    var allFields: any = (<HTMLInputElement>document.getElementById('addFieldArray')).childNodes;
    for (let i = 0; i < allFields.length; i++) {
      var fieldPattern: any = (<HTMLInputElement>document.getElementById('addFieldArray')).childNodes[i];
      fieldPattern.pattern = expression;
    }
  }


  // сброс полей массива
  resetArray() {
    (<HTMLInputElement>document.getElementById('addFieldArray')).innerHTML = '';
    (<HTMLInputElement>document.getElementById('addFieldArray')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('resetArray')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('arrayButton')).style.boxShadow = 'none';
  }


}






interface InputParameter {
  id: string;
  url: number;
  name: number;
  description: string;
  min: number;
  max: number;
}


interface Value {
  id: number;
  dt: string;
  value1: number;
  value2: number;
  value3: number;
  value4: number;
}

interface ArrayParameters {
  shortDesc: string;
  attributes: any;
  tagName: string;
  description: string;
  length: number;
}

interface OptionsParameter {
  objName: string;
  paramName: string;
  paramDesc: string;
  type: string;
  engUnits: string;
  trendHigh: string;
  trendLow: string;
  values: string []
}
