import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatTabsModule } from '@angular/material';

import { WorkOrdersRoutingModule } from './work-orders-routing.module';
import { WorkordersComponent } from './pages/workorders/workorders.component';
import { WorkorderComponent } from './pages/workorder/workorder.component';

@NgModule({
  imports: [
    MatTabsModule,
    CommonModule,
    WorkOrdersRoutingModule
  ],
  declarations: [WorkordersComponent, WorkorderComponent]
})
export class WorkOrdersModule { }
