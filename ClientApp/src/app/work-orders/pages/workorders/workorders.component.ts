import { Component, OnInit, Inject, ViewChild, ElementRef, ViewEncapsulation} from '@angular/core'

import "dhtmlx-gantt";

import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as d3Path from 'd3-path';


@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'gantt',
  styleUrls: ['./gantt.component.css'],
  //providers: [TaskService, LinkService],
  template: `<div #gantt_here class='gantt-chart'></div>`,
})

export class WorkordersComponent implements OnInit {
  // @ViewChild("gantt_here") ganttContainer: ElementRef;



  title = "Планировщик задач";
  public chartInterval: any[] = [];





  public currentDateTime: Date;
  public bufer: string;

  public dataLength: number;     // ширина диапазона

  private margin = { top: 20, right: 0, bottom: 40, left: 80 };
  private width: number;
  private height: number;
  private x: any;
  private y: any;
  private svg: any;
  private svg1: any;
  private svg2: any;


  /*
  private data = [{"dt": new Date("2019-10-13 20:14:58"),"value": 100.0},
                       {"dt": new Date("2019-10-13 21:14:58"), "value": 1000.0},
                       {"dt": new Date("2019-10-13 22:14:58"), "value": 0}  ]

*/


/*
private buildSvgGant() {
    this.svg = d3.select('.gant_template')
      .append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

private addXandYAxis() {
    // range of data configuring
    this.x = d3Scale.scaleTime().range([0, this.width]);
    this.y = d3Scale.scaleLinear().range([this.height, 0]);
    this.x.domain(d3Array.extent(this.data, (d) => d.dt));
    this.y.domain(d3Array.extent(this.data, (d) => d.value));

    // Configure the X Axis
    this.svg.append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .call(d3Axis.axisBottom(this.x)
        .ticks(5));

    // Configure the Y Axis
    this.svg.append('g')
      .attr('style', 'stroke-opacity: 0.3')
      .call(d3Axis.axisLeft(this.y)
        .tickSize(-this.width)
        .ticks(10));
  }

  private addObject1() {            
    this.svg.append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', -60)
      .attr('x', -10)
      .attr('text-anchor', 'end')
      .text('Исполнители');

    this.svg.append('text')
      .attr('y', 440)
      .attr('x', 1100)
      .attr('text-anchor', 'end')
      .text('Поставленные задачи');
  }

  private topLine() {
    this.svg.append('rect')           // столбец максимума первого процесса
      .attr("width", this.width)
      .attr("height", this.height/10)
      .attr("x", 0)
      .attr("y", 0)
      .attr('stroke', '#eee')
      .attr('fill', 'rgba(0,0,0,0.02)')
      .attr('stroke-width', '1');
  }

*/
  ngOnInit() {
      // gantt.init(this.ganttContainer.nativeElement);
  }


}

