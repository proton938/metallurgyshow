import { Component, OnInit, Inject } from '@angular/core';
// import { SharedModule } from '../shared/shared.module';
// import { ChartToolbarComponent } from '../shared/layout/chart-toolbar/chart-toolbar.component';

import { TreeParams } from "../core/models/TreeParams";

import { HttpClient } from '@angular/common/http';

import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';

@Component({
  selector: 'app-test',
  //templateUrl: './test.component.html',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  public treeParams: TreeParams;

  public title: string = 'Архив';

  public start: any = 150;
  public stop: any = 200;

  public startDateTime: any;
  public stopDateTime: any;
  public currentDateTime: any;   // текущее время
  public currentMonth: any;
  public currentDate: any;
  public currentHour: any;
  public lastHour: any;
  public timeBufer: any;

  // y = SharedModule
  // x = ChartToolbarComponent

  data: any[] = [];

  data1: any[] = [];     // массивы для сбора данных 
  data2: any[] = [];
  data3: any[] = [];
  data4: any[] = [];


  public dataLength: number;     // ширина диапазона
  public yLimit: number = 1300;

  public pathUrl: string;
  public treeParametersUrl: string;
  public getHistoryValuesUrl: string;

  public values: Value[];
  public arrayParameters: ArrayParameters[];
  public getHistoryValues: ArrayParameters[];

  private baseUrl: string;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, public tree: TreeParams) {

    this.baseUrl = baseUrl;

    this.treeParams = tree;

    this.pathUrl = baseUrl + 'api/PrmValues';
    this.treeParametersUrl = baseUrl + 'api/parameters/getParametersByArea';
    this.getHistoryValuesUrl = baseUrl + 'api/PrmValues/GetHistoryValues';

    this.currentDateTime = new Date();        // Выводим текущую дату

    this.currentMonth = '' + Number(this.currentDateTime.getMonth() + 1);   // двузначное значение месяца
    if (this.currentMonth.length < 2) {
      this.currentMonth = '0' + this.currentMonth;
    }

    this.currentDate = '' + Number(this.currentDateTime.getDate());       // двузначное значение даты
    if (this.currentDate.length < 2) {
      this.currentDate = '0' + this.currentDate;
    }

    this.currentHour = this.currentDateTime.getHours() + ':00';
    if (this.currentHour.length < 5) {
      this.currentHour = '0' + this.currentHour;
    }

    this.lastHour = this.currentDateTime.getHours() - 1 + ':00';
    if (this.lastHour.length < 5) {
      this.lastHour = '0' + this.lastHour;
    }

    this.startDateTime = this.currentDateTime.getFullYear() + '-' + this.currentMonth + '-' + this.currentDate + ' ' + this.lastHour;
    this.startDateTime = this.startDateTime.slice(0, 14) + '48';
    this.stopDateTime = this.currentDateTime.getFullYear() + '-' + this.currentMonth + '-' + this.currentDate + ' ' + this.currentHour;

    this.http.get<Value[]>(this.pathUrl + '/' + this.startDateTime + '/' + this.stopDateTime).subscribe(result => {
      this.values = result;
      for (let i = 0; i < this.values.length - 1; i++) {
        this.timeBufer = new Date(this.values[i].dt);
        this.timeBufer = this.timeBufer.getHours() + ':' + this.timeBufer.getMinutes() + ':' + this.timeBufer.getSeconds();
        this.data.push({ dt: this.timeBufer, value1: this.values[i].value1, value2: this.values[i].value2, value3: this.values[i].value3, value4: this.values[i].value4 });
      }
    }, error => console.error(error));


    // если массив параметров в модели не пустой выводим его сразу в дерево 
    if (this.treeParams.getTreeParameters.length != 0) {
      this.arrayParameters = this.treeParams.getTreeParameters;
      this.permission = 1;  // блокируем выгрузку массива из галактики
    }

    this.treeParams.unToggleMarkRemoveDateTime();         // обнуляем отметку изменения даты/времени в модели
    this.treeParams.unToggleMarkRemoveSelectedPrms();     // обнуляем отметку изменения выбора параметров в модели

  }

  getValues() {
    return this.values;
  }



  ngOnInit() {

    if (this.treeParams.getTreeParametersUrl == '') {

      // при загрузке страницы выводим последний час

      var currentDateTime: any = new Date();        // Выводим текущую дату

      (<HTMLInputElement>document.getElementById("dateStop")).value = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);
      (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + currentDateTime.getHours()).slice(-2) + ':00';
      (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + currentDateTime.getMinutes()).slice(-2);

      var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

      var lastDateTime: any = new Date(currentDateTime - eraUnix - 3600000);

      (<HTMLInputElement>document.getElementById("dateStart")).value = lastDateTime.getFullYear() + '-' + ('0' + (lastDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + lastDateTime.getDate()).slice(-2);
      (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + lastDateTime.getHours()).slice(-2) + ':00';
      (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + lastDateTime.getMinutes()).slice(-2);

    } else {

      var currentDateTime: any = new Date(this.treeParams.startDateTime);        // Выводим выгружаемую дату

      (<HTMLInputElement>document.getElementById("dateStart")).value = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);
      (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + currentDateTime.getHours()).slice(-2) + ':00';
      (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + currentDateTime.getMinutes()).slice(-2);

      var lastDateTime: any = new Date(this.treeParams.stopDateTime);

      (<HTMLInputElement>document.getElementById("dateStop")).value = lastDateTime.getFullYear() + '-' + ('0' + (lastDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + lastDateTime.getDate()).slice(-2);
      (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + lastDateTime.getHours()).slice(-2) + ':00';
      (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + lastDateTime.getMinutes()).slice(-2);
    }


    // this.outTest();

    this.getClient();

    window.addEventListener('scroll', this.scroll, true);
  }



  scroll = (): void => {
    var obj: any = (<HTMLInputElement>document.getElementById("archiveWindow"));
    var rect: any = obj.getBoundingClientRect();                                                        // абсолютная высота заголовка таблицы
    var client_width = obj.clientWidth;
    if (rect.top < 0) {
      (<HTMLInputElement>document.getElementById("headerFixedRow")).style.opacity = '1';
      (<HTMLInputElement>document.getElementById("headerFixedRow")).style.top = '0px';
      if (document.body.clientWidth >= 768) { 
        (<HTMLInputElement>document.getElementById("headerFixedRow")).style.left = rect.left + 'px';
        (<HTMLInputElement>document.getElementById("headerFixedRow")).style.width = client_width + 'px';
      } else {
        (<HTMLInputElement>document.getElementById("headerFixedRow")).style.left = '0px';
        (<HTMLInputElement>document.getElementById("headerFixedRow")).style.top = '53px';
        (<HTMLInputElement>document.getElementById("headerFixedRow")).style.width = rect.left + client_width + 'px';
      }
    } else {
      (<HTMLInputElement>document.getElementById("headerFixedRow")).style.opacity = '0';
    }
  };






  unHideToolbarMenu() {       // скрытие меню инструментов
    var hiddenToolBar = (<HTMLInputElement>document.getElementById("unHiddenToolbar")).className;
    if (hiddenToolBar == 'unhidden_toolbar') {
      (<HTMLInputElement>document.getElementById("unHiddenToolbar")).className = 'hidden_toolbar';
      (<HTMLInputElement>document.getElementById('toolbarTitle')).style.display = 'none';
    } else {
      (<HTMLInputElement>document.getElementById("unHiddenToolbar")).className = 'unhidden_toolbar';
      (<HTMLInputElement>document.getElementById('toolbarTitle')).style.display = 'inline-block';
    }
  }




  public selected_prms: string = '';
  public arrayCellTime: any = [];       // выводим массив значений времени для вертикальной шкалы

  getClient() {
    (<HTMLInputElement>document.getElementById('archiveWindow')).style.height = 'auto';

    if (this.treeParams.markRemoveDateTime == 1) {   // если менялась отметка изменения даты/времени вызываем метод обнуления url
      this.treeParams.updateUrlModel();
    }

    if (this.treeParams.markRemoveSelectedPrms == 1) {  // если менялась отметка изменения выбора параметров вызываем метод обнуления массива выбранных параметров
      this.treeParams.updateSelectedPrmsModel();
    }



    /*  если в модели дерева параметров еще не было сохранения данных запроса -
        формируем запрос с нуля - считываем выбранные параметры с дерева панели chart-toolbar,
        что бы вложить их в строку запроса 
    */

    if (this.treeParams.getTreeParametersUrl == '') {

      this.selected_prms = '';

      this.arrayCellTime = [];
      var select_check: any;

      if (this.treeParams.getArraySelectedPrms.length == 0) {     // если ранее не было выбрано ни одного параметра (массив в модели пустой)

        // процедура выгрузки массива выбранных параметров
        for (let i = 0; i < this.arrayParameters.length; i++) {
          for (let j = 0; j < this.arrayParameters[i].attributes.length; j++) {
            select_check = (<HTMLInputElement>document.getElementById('callParameter_' + this.arrayParameters[i].tagName + '_' + (j + 1)));
            if (select_check.checked) {
              this.selected_prms = this.selected_prms + this.arrayParameters[i].tagName + '.' + this.arrayParameters[i].attributes[j].name + ',';
              this.treeParams.getArraySelectedPrms.push({ tagName_name: this.arrayParameters[i].tagName + '.' + this.arrayParameters[i].attributes[j].name, historyValues: [], name: this.arrayParameters[i].attributes[j].name, short_desc: this.arrayParameters[i].shortDesc, description: this.arrayParameters[i].attributes[j].description, engUnits: this.arrayParameters[i].attributes[j].engUnits });

              this.treeParams.getSelectedParameters.push('callParameter_' + this.arrayParameters[i].tagName + '_' + (j + 1));   // массив id отмеченных checkbox-ов в модели
            }
          }
        }

        this.selected_prms = this.selected_prms.slice(0, this.selected_prms.length - 1);   // убираем лишнюю зяпятую в конце массива выбранных параметров

      } else {

        for (let i = 0; i < this.treeParams.getArraySelectedPrms.length; i++) {
          this.selected_prms = this.selected_prms + this.treeParams.getArraySelectedPrms[i].tagName_name + ',';
          this.treeParams.getArraySelectedPrms[i].historyValues = [];
        }
        this.selected_prms = this.selected_prms.slice(0, this.selected_prms.length - 1);   // убираем лишнюю зяпятую в конце массива выбранных параметров

      }


      var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
      startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут

      var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
      stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут

      this.startDateTime = (<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + startTime;
      this.stopDateTime = (<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime;


      // если время завершения - будущее - преврашаем его в текущее
      var currentDateTime: any = new Date();        // Выводим текущую дату
      var buferStopDateTume: any = new Date(this.stopDateTime);
      if (buferStopDateTume - currentDateTime > 0) {
        (<HTMLInputElement>document.getElementById("dateStop")).value = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);
        (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + currentDateTime.getHours()).slice(-2) + ':00';
        (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + currentDateTime.getMinutes()).slice(-2);

        var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
        stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут
        this.stopDateTime = (<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime;
      }

      this.treeParams.startDateTime = this.startDateTime;    // отправляем точки времени в модель
      this.treeParams.stopDateTime = this.stopDateTime;

      this.getHistoryValuesUrl = this.baseUrl + 'api/PrmValues/GetHistoryValues' + '/' + this.selected_prms + '/' + this.startDateTime + '/' + this.stopDateTime;

      // передаем данные в модель
      this.treeParams.getTreeParametersUrl = this.getHistoryValuesUrl;

    } else {

      /*
       если ранее параметры запроса сохранялись в модель -
       повторяем проделанный ранее запрос по сохраненному url для возврата в исходное
       отображение данных на экране, которое было до покидания раздела Анализ
       */

      this.getHistoryValuesUrl = this.treeParams.getTreeParametersUrl;

      for (let i = 0; i < this.treeParams.getArraySelectedPrms.length; i++) {  // чистим ранее выведенный массив истории параметров
        this.treeParams.getArraySelectedPrms[i].historyValues = [];
      }

    }



    this.http.get<ArrayParameters[]>(this.getHistoryValuesUrl).subscribe(result => {
      this.getHistoryValues = result;
      var shortVal: any;         // переменная для укороченных цифр
      for (let i = 0; i < this.treeParams.getArraySelectedPrms.length; i++) {
        for (let j = 0; j < this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name].length; j++) {  // получен ассоциативный массив - по ключу обращаемся к дочернему нумерованному, для получения длины
          this.timeBufer = new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt);
          this.timeBufer = ('0' + this.timeBufer.getHours()).slice(-2) + ':' + ('0' + this.timeBufer.getMinutes()).slice(-2) + ':' + ('0' + this.timeBufer.getSeconds()).slice(-2);

          shortVal = Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val).toFixed(3);    // укорачиваем цифры три знака после запятой
          if (shortVal == 'NaN') {
            shortVal = 0;
          }


          this.treeParams.getArraySelectedPrms[i].historyValues.push({ dt: this.timeBufer, val: shortVal });

          if (i == 0) {
            this.arrayCellTime.push({ dt: this.timeBufer });
          }

        }
      }
    }, error => {

        console.error(error);

        var currentUrl: any = document.location.href;
        currentUrl = currentUrl.split('/');
        if (currentUrl.length < 5) {
          currentUrl = 'Main';
        } else {
          currentUrl = currentUrl[4];
        }

        this.http.get<ArrayParameters[]>('assets/replase_server/historyValues/' + currentUrl +'.json').subscribe((result) => {

          this.getHistoryValues = result;

          // alert(JSON.stringify(this.getHistoryValues));

          var shortVal: any;         // переменная для укороченных цифр
          for (let i = 0; i < this.treeParams.getArraySelectedPrms.length; i++) {
            for (let j = 0; j < this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name].length; j++) {  // получен ассоциативный массив - по ключу обращаемся к дочернему нумерованному, для получения длины
              this.timeBufer = new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt);
              this.timeBufer = ('0' + this.timeBufer.getHours()).slice(-2) + ':' + ('0' + this.timeBufer.getMinutes()).slice(-2) + ':' + ('0' + this.timeBufer.getSeconds()).slice(-2);

              shortVal = Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val).toFixed(3);    // укорачиваем цифры три знака после запятой
              if (shortVal == 'NaN') {
                shortVal = 0;
              }


              this.treeParams.getArraySelectedPrms[i].historyValues.push({ dt: this.timeBufer, val: shortVal });

              if (i == 0) {
                this.arrayCellTime.push({ dt: this.timeBufer });
              }

            }
          }
        });

    });


    this.treeParams.unToggleMarkRemoveDateTime();         // убираем отметку изменения даты/времени 
    this.treeParams.unToggleMarkRemoveSelectedPrms();     // убираем отметку изменения выбора параметров


    console.log(this.treeParams.getArraySelectedPrms);

  }




  public tPermission: number = 0;  // переключатель движения прокрутки

  rightScroll() {
    if (this.tPermission == 0) {
      var that = this;
      setTimeout(function () {
        var left: number = (<HTMLInputElement>document.getElementById("archiveWindow")).scrollLeft;
        (<HTMLInputElement>document.getElementById("archiveWindow")).scrollLeft = left - 10;
        that.rightScroll();
      }, 1);
    }
  }


  leftScroll() {
    if (this.tPermission == 0) {
      var that = this;
      setTimeout(function () {
        var left: number = (<HTMLInputElement>document.getElementById("archiveWindow")).scrollLeft;
        (<HTMLInputElement>document.getElementById("archiveWindow")).scrollLeft = left + 10;
        that.leftScroll();
      }, 1);
    }
  }


  synchroScroll() {
    (<HTMLInputElement>document.getElementById("scrollHeaderRow")).scrollLeft = (<HTMLInputElement>document.getElementById("archiveWindow")).scrollLeft;
  }






  outTest() {       // функция фальш-массива 

    (<HTMLInputElement>document.getElementById('archiveWindow')).style.height = 'auto';

    this.selected_prms = '';
    this.treeParams.getArraySelectedPrms = [];
    this.arrayCellTime = [];

    for (let i = 0; i < 30; i++) {
      this.treeParams.getArraySelectedPrms.push({ name: 'пр' + (i+1), historyValues: [] });
    }

    for (let i = 0; i < 30; i++) {
      for (let j = 0; j < 60; j++) {  // получен ассоциативный массив - по ключу обращаемся к дочернему нумерованному, для получения длины
        this.treeParams.getArraySelectedPrms[i].historyValues.push({ dt: '--:--:--', val: '0.000' });
        if (i == 0) {
          this.arrayCellTime.push({ dt: '--:--:--' });
        }
      }
    }

  }




  public identif: string = '';
  public styleDisplay: any;
  private permission: number = 0;

  treeParameters() {
    var currentUrl: any = document.location.href;
    currentUrl = currentUrl.split('/');
    if (currentUrl.length < 5) {
      currentUrl = 'Main';
    } else {
      currentUrl = currentUrl[4];
    }

    var testLoadMenu = (<HTMLInputElement>document.getElementById(currentUrl));

    if (testLoadMenu) {
      currentUrl = (<HTMLInputElement>document.getElementById(currentUrl)).value;
      this.treeParametersUrl = this.treeParametersUrl + '/' + currentUrl;

      this.styleDisplay = (<HTMLInputElement>document.getElementById('one_1')).style.display;
      if (this.styleDisplay == 'none') {
        (<HTMLInputElement>document.getElementById('one_1')).style.display = 'table';
        (<HTMLInputElement>document.getElementById('rolloverDown')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('rolloverUp')).style.display = 'block';

        // возвращаем checked ранее выделенных параметров
        if (this.treeParams.getSelectedParameters.length != 0) {
          for (let i = 0; i < this.treeParams.getSelectedParameters.length; i++) {
            (<HTMLInputElement>document.getElementById(this.treeParams.getSelectedParameters[i])).checked = true;
          }
        }

      } else {
        (<HTMLInputElement>document.getElementById('one_1')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('rolloverDown')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('rolloverUp')).style.display = 'none';
      }

      if (this.permission == 0) {     // если разрешение на повторную выгрузку параметров из галактики открыто - осуществляем
        this.http.get<ArrayParameters[]>(this.treeParametersUrl).subscribe(result => {
          this.arrayParameters = result;
          for (let i = 0; i < this.arrayParameters.length; i++) {      // если имя параметра пустое - заменяем его на системный тег 
            if (this.arrayParameters[i].shortDesc == '') {
              this.arrayParameters[i].shortDesc = this.arrayParameters[i].tagName;
            }
          }

          this.treeParams.getTreeParameters = this.arrayParameters;   // передаем массив дерева в модель

        }, error => {
            console.error(error);

            this.http.get<ArrayParameters[]>('assets/replase_server/' + currentUrl + '.json').subscribe((result) => {
              this.arrayParameters = result;
              for (let i = 0; i < this.arrayParameters.length; i++) {      // если имя параметра пустое - заменяем его на системный тег 
                if (this.arrayParameters[i].shortDesc == '') {
                  this.arrayParameters[i].shortDesc = this.arrayParameters[i].tagName;
                }
              }

              this.treeParams.getTreeParameters = this.arrayParameters;   // передаем массив дерева в модель

            });

        });

        this.permission = 1;                   // закрываем разрешение на повторную выгрузку массива закрываем
      }


    } else {
      alert('Меню навигации еще не догрузилось!');
    }
  }



  unHideTree() {
    this.styleDisplay = (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display;
    if (this.styleDisplay == 'none') {
      (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display = 'table';
      (<HTMLInputElement>document.getElementById('arrowOne_' + this.identif)).innerHTML = 'v';
    } else {
      (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display = 'none';
      (<HTMLInputElement>document.getElementById('arrowOne_' + this.identif)).innerHTML = '>';
    }
  }



  allAttributes(counterObject) {  // выделение всех параметров объекта
    if ((<HTMLInputElement>document.getElementById('callObject_' + (counterObject + 1))).checked) {
      for (let i = 0; i < this.arrayParameters[counterObject].attributes.length; i++) {
        (<HTMLInputElement>document.getElementById('callParameter_' + this.arrayParameters[counterObject].tagName + '_' + (i + 1))).checked = true;
      }
    } else {
      for (let i = 0; i < this.arrayParameters[counterObject].attributes.length; i++) {
        (<HTMLInputElement>document.getElementById('callParameter_' + this.arrayParameters[counterObject].tagName + '_' + (i + 1))).checked = false;
      }
    }
  }



}



interface Value {
  id: number;
  dt: string;
  value1: number;
  value2: number;
  value3: number;
  value4: number;
}

interface ArrayParameters {
  shortDesc: string;
  attributes: any;
  tagName: string;
  description: string;
  length: number;
}

