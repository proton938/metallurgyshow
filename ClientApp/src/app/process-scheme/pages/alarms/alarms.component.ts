import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-alarms',
  templateUrl: './alarms.component.html',
  styleUrls: ['./alarms.component.css']
})
export class AlarmsComponent implements OnInit {

  public equipments: Equipment[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Equipment[]>(baseUrl + 'api/Equipments').subscribe(result => {
      this.equipments = result;
    }, error => console.error(error));
  }

  ngOnInit() {
  }

}

interface Equipment {
  id: number;
  parent: number;
  name: string;
  description: string;
}
