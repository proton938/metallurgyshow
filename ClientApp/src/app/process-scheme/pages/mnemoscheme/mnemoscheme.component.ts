import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
//import { IntouchUrlService } from '../../../core/services/intouch-url.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-mnemoscheme',
  templateUrl: './mnemoscheme.component.html',
  styleUrls: ['./mnemoscheme.component.css']
})
export class MnemoschemeComponent implements OnInit {
  area: string;
  url: String;
  sanitizer: DomSanitizer;

  constructor(private route: ActivatedRoute, private router: Router, private san: DomSanitizer/*, private urlService: IntouchUrlService*/) {
    this.area = "test";
    this.sanitizer = san;
  }

  ngOnInit() {
    this.area = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        params.get('id'))).toString();

    this.scaleIFrame();

    var currentUrl: any = document.location.href;
    currentUrl = currentUrl.split('/');


    if (currentUrl[4] != 'Main') {
      (<HTMLInputElement>document.getElementById("inputText")).style.display = "none";
    } else {
      (<HTMLInputElement>document.getElementById("inputText")).style.display = "block";
    }
  }

  photoURL() {
    this.area = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        params.get('id'))).toString();
    let id = this.route.snapshot.paramMap.get('id');
    this.url = "http://localhost/intouchweb/api/symbol/" + id.toString();
    return this.url;

  }

  // http://zinc-sp2017/intouchweb/api/symbol/

  scaleIFrame() {
    var currentUrl: any = document.location.href;
    currentUrl = currentUrl.split('/');

    currentUrl = currentUrl[4];

    if (currentUrl == "Energ") {
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = "500px";
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.height = "400px";
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.marginLeft = "30px";
    }

    if (currentUrl == "Electr") {

      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.width = "900px";
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.height = "400px";
      (<HTMLInputElement>document.getElementById("InTouchWeb")).style.marginLeft = "30px";
    }

  }

    //onRefresh() {
    //  this.url = "test";
    //}
}

