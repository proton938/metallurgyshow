"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./alarms/alarms.component"));
__export(require("./scheme/scheme.component"));
__export(require("./mnemoscheme/mnemoscheme.component"));
//# sourceMappingURL=index.js.map