import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CommonModule } from '@angular/common';

import { ProcessSchemeRoutingModule } from './process-scheme-routing.module';
import { SchemeComponent } from './pages/scheme/scheme.component';
import { AlarmsComponent } from './pages/alarms/alarms.component';
import { MnemoschemeComponent } from './pages/mnemoscheme/mnemoscheme.component';

import { MatTabsModule } from '@angular/material';
import { SafePipe } from '../core/pipes/safe';


@NgModule({
  imports: [
    CommonModule,
    ProcessSchemeRoutingModule,
    MatTabsModule,
    //SafePipe
  ],
  declarations: [SchemeComponent, AlarmsComponent, MnemoschemeComponent, SafePipe],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [MnemoschemeComponent]
})
export class ProcessSchemeModule { }
