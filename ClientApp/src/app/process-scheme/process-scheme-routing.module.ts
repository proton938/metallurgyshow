import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlarmsComponent, SchemeComponent, MnemoschemeComponent } from './pages';

const routes: Routes = [
  { path: 'alarms', component: AlarmsComponent },
  { path: 'scheme', component: SchemeComponent },
  { path: 'mnemoscheme/:id', component: MnemoschemeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcessSchemeRoutingModule { }
