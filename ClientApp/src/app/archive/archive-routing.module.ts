import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistorianComponent, ParametersComponent, ArchiveComponent } from './pages';
import { TestComponent } from '../test/test.component';

const routes: Routes = [
  { path: 'historian', component: HistorianComponent },
  { path: 'historian/:tag', component: HistorianComponent },
  { path: 'parameters', component: ParametersComponent },
  { path: 'archive', component: TestComponent },
  { path: 'archive/:id', component: TestComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArchiveRoutingModule { }
