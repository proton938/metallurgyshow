"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./historian/historian.component"));
__export(require("./parameters/parameters.component"));
__export(require("./archive/archive.component"));
//# sourceMappingURL=index.js.map