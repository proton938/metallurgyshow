import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.css']
})
export class ArchiveComponent implements OnInit {


  public start: any = 150;
  public stop: any = 200;

  public startDateTime: any;
  public stopDateTime: any;
  public currentDateTime: any;   // текущее время
  public currentMonth: any;
  public currentDate: any;
  public currentHour: any;
  public lastHour: any;
  public timeBufer: any;

  public pathUrl: string;
  public treeParametersUrl: string;
  public getHistoryValuesUrl: string;

  public arrayParameters: ArrayParameters[];
  public getHistoryValues: ArrayParameters[];

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.treeParametersUrl = baseUrl + 'api/parameters/getParametersByArea';
  }

  ngOnInit() {
    this.outTest();

    window.addEventListener('scroll', this.scroll, true);
  }


  scroll = (): void => {
    var obj: any = (<HTMLInputElement>document.getElementById("archiveWindow"));
    var rect: any = obj.getBoundingClientRect();                                                        // абсолютная высота заголовка таблицы
    var client_width = obj.clientWidth;
    if (rect.top < 0) {
      (<HTMLInputElement>document.getElementById("headerFixedRow")).style.opacity = '1';
      (<HTMLInputElement>document.getElementById("headerFixedRow")).style.top = '0px';
      if (document.body.clientWidth >= 768) {
        (<HTMLInputElement>document.getElementById("headerFixedRow")).style.left = rect.left + 'px';
        (<HTMLInputElement>document.getElementById("headerFixedRow")).style.width = client_width + 'px';
      } else {
        (<HTMLInputElement>document.getElementById("headerFixedRow")).style.left = '0px';
        (<HTMLInputElement>document.getElementById("headerFixedRow")).style.top = '53px';
        (<HTMLInputElement>document.getElementById("headerFixedRow")).style.width = rect.left + client_width + 'px';
      }
    } else {
      (<HTMLInputElement>document.getElementById("headerFixedRow")).style.opacity = '0';
    }
  };






  public selected_prms: string = '';
  public arraySelectedPrms: any = [];
  public arrayCellTime: any = [];       // выводим массив значений времени для вертикальной шкалы

  getClient() {
    var windowHeight = screen.height;
    var obj = (<HTMLInputElement>document.getElementById('archiveWindow'));
    var rect: any = obj.getBoundingClientRect();
    (<HTMLInputElement>document.getElementById('archiveWindow')).style.height = windowHeight - rect.top - 200 + 'px';
    (<HTMLInputElement>document.getElementById("arrayValues")).style.left = 150 + 'px';
    (<HTMLInputElement>document.getElementById("arrayTagRow")).style.left = 150 + 'px';
    (<HTMLInputElement>document.getElementById("arrayValues")).style.top = 30 + 'px';
    (<HTMLInputElement>document.getElementById("arrayTimeCell")).style.top = 30 + 'px';

    // процедура выгрузки массива выбранных параметров
    this.selected_prms = '';
    this.arraySelectedPrms = [];
    this.arrayCellTime = [];
    var select_check: any;
    for (let i = 0; i < this.arrayParameters.length; i++) {
      for (let j = 0; j < this.arrayParameters[i].attributes.length; j++) {
        select_check = (<HTMLInputElement>document.getElementById('callParameter_' + this.arrayParameters[i].tagName + '_' + (j + 1)));
        if (select_check.checked) {
          this.selected_prms = this.selected_prms + this.arrayParameters[i].tagName + '.' + this.arrayParameters[i].attributes[j].name + ',';
          this.arraySelectedPrms.push({ tagName_name: this.arrayParameters[i].tagName + '.' + this.arrayParameters[i].attributes[j].name, historyValues: [], name: this.arrayParameters[i].attributes[j].name, shor_desc: this.arrayParameters[i].shortDesc });
        }
      }
    }
    this.selected_prms = this.selected_prms.slice(0, this.selected_prms.length - 1);

    this.startDateTime = (<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + (<HTMLInputElement>document.getElementById("timeStart")).value;
    this.stopDateTime = (<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + (<HTMLInputElement>document.getElementById("timeStop")).value;

    this.http.get<ArrayParameters[]>(this.getHistoryValuesUrl + '/' + this.selected_prms + '/' + this.startDateTime + '/' + this.stopDateTime).subscribe(result => {
      this.getHistoryValues = result;
      var shortVal: any;         // переменная для укороченных цифр
      for (let i = 0; i < this.arraySelectedPrms.length; i++) {
        for (let j = 0; j < this.getHistoryValues[this.arraySelectedPrms[i].tagName_name].length; j++) {  // получен ассоциативный массив - по ключу обращаемся к дочернему нумерованному, для получения длины
          this.timeBufer = new Date(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].dt);
          this.timeBufer = ('0' + this.timeBufer.getHours()).slice(-2) + ':' + ('0' + this.timeBufer.getMinutes()).slice(-2) + ':' + ('0' + this.timeBufer.getSeconds()).slice(-2);

          shortVal = Number(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val).toFixed(3);    // укорачиваем цифры три знака после запятой
          if (shortVal == 'NaN') {
            shortVal = 0;
          }

          this.arraySelectedPrms[i].historyValues.push({ dt: this.timeBufer, val: shortVal });
          if (i == 0) {
            this.arrayCellTime.push({ dt: this.timeBufer });
          }
        }
      }
    }, error => console.error(error));

  }




  public tPermission: number = 0;  // переключатель движения прокрутки

  rightScroll() {
    if (this.tPermission == 0) {
      var that = this;
      setTimeout(function () {
        var left: number = (<HTMLInputElement>document.getElementById("archiveWindow")).scrollLeft;
        (<HTMLInputElement>document.getElementById("archiveWindow")).scrollLeft = left - 20;
        that.rightScroll();
      }, 1);
    }
  }


  leftScroll() {
    if (this.tPermission == 0) {
      var that = this;
      setTimeout(function () {
        var left: number = (<HTMLInputElement>document.getElementById("archiveWindow")).scrollLeft;
        (<HTMLInputElement>document.getElementById("archiveWindow")).scrollLeft = left + 20;
        that.leftScroll();
      }, 1);
    }
  }


  synchroScroll() {
    (<HTMLInputElement>document.getElementById("scrollHeaderRow")).scrollLeft = (<HTMLInputElement>document.getElementById("archiveWindow")).scrollLeft;
  }





  outTest() {       // функция фальш-массива 

    (<HTMLInputElement>document.getElementById('archiveWindow')).style.height = 'auto';

    this.selected_prms = '';
    this.arraySelectedPrms = [];
    this.arrayCellTime = [];

    for (let i = 0; i < 10; i++) {
      this.arraySelectedPrms.push({ name: 'пр' + (i + 1), historyValues: [] });
    }

    for (let i = 0; i < 10; i++) {
      for (let j = 0; j < 10; j++) {  // получен ассоциативный массив - по ключу обращаемся к дочернему нумерованному, для получения длины
        this.arraySelectedPrms[i].historyValues.push({ dt: '--:--:--', val: '0.000' });
        if (i == 0) {
          this.arrayCellTime.push({ dt: '--:--:--' });
        }
      }
    }

  }




}








interface Value {
  id: number;
  dt: string;
  value1: number;
  value2: number;
  value3: number;
  value4: number;
}

interface ArrayParameters {
  shortDesc: string;
  attributes: any;
  tagName: string;
  description: string;
  length: number;
}
