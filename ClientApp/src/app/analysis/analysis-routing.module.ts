import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PredictiveAnalyticsComponent } from './pages';
import { AnalyticsComponent } from './pages';

const routes: Routes = [
  { path: 'predictiveanalytics', component: PredictiveAnalyticsComponent },
  { path: 'predictiveanalytics/:id', component: PredictiveAnalyticsComponent },
  { path: 'analytics', component: AnalyticsComponent },
  { path: 'analytics/:id', component: AnalyticsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnalysisRoutingModule { }
