import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as d3Scale from 'd3-scale';
import * as d3 from 'd3-selection';
import * as d3Shape from 'd3-shape';
import { TreeParams } from "../../../core/models/TreeParams";



@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})

export class AnalyticsComponent implements OnInit {

  form: FormGroup;

  stroke: string = '#feb41b';
  arrayAttributesCharts: any[] = [];   // массив аттрибутов графиков

  public treeParams: TreeParams;

  // переключатель страниц

  d1() {
    document.getElementById("conveyor").style.left = 0 + "%";
    (<HTMLInputElement>document.getElementById("page_number")).value = '1';
  }        
  d2() {
    document.getElementById("conveyor").style.left = -103 + "%";
    (<HTMLInputElement>document.getElementById("page_number")).value = '2';
  }
  d3() {
    document.getElementById("conveyor").style.left = -212 + "%";
    (<HTMLInputElement>document.getElementById("page_number")).value = '3';
  }

  public start: number;
  public stop: number;

  public startDateTime: any;
  public stopDateTime: any;
  public currentDateTime: any;   // текущее время
  public currentMonth: any;
  public currentDate: any;
  public currentHour: any;
  public lastHour: any;
  public timeBufer: any;

  public startDate: any;
  public stopDate: any;
  public startHour: any;
  public stopHour: any;

  public timeScaleRight: number = 0;
  public timeScaleLeft: number = 0;


  public values: Value[];
  public arrayParameters: ArrayParameters[];
  public getHistoryValues: ArrayParameters[];

  title = 'Анализ данных';

  public dataLength: number;     // ширина диапазона

  data: any[] = [];
  data1: any[] = [];     // массивы для сбора данных 
  data2: any[] = [];
  data3: any[] = [];
  data4: any[] = [];

  public pathUrl: string;
  public treeParametersUrl: string;
  public getHistoryValuesUrl: string;

  public yLimit: number = 1300;

  public yMaxLimit: number = 0;
  public yMinLimit: number = 1000000;
  private leftOffsetForBarChart: number = 0;        // переменная смещения прямоугольников интеграционной диаграмы 

  public maxValue: number = 0;
  public mediumValue: number = 0;
  public minValue: number = 1000000;
  public bufer: any;
  public getPageNumber: string;
  public counter: number;
  private arc: any;

  public margin = { top: 20, right: 0, bottom: 40, left: 80 };
  public width: number;
  public height: number;
  public x: any;
  public y: any;
  public svg1: any;
  public svg2: any;
  public svg3: any;
  public line: d3Shape.Line<[number, number]>; // this is line defination

  public idChart: any;

  public baseUrl: string;



  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, public tree: TreeParams, private fb: FormBuilder) {

    this.baseUrl = baseUrl;

    this.treeParams = tree;

    this.pathUrl = baseUrl + 'api/PrmValues';
    this.treeParametersUrl = baseUrl + 'api/parameters/getParametersByArea';
    this.getHistoryValuesUrl = baseUrl + 'api/PrmValues/GetHistoryValues';

    http.get<Value[]>(this.pathUrl).subscribe(result => {
      this.values = result;
    }, error => console.error(error));

    this.http.get<ArrayParameters[]>(this.getHistoryValuesUrl + '/' + this.selected_prms + '/' + this.startDateTime + '/' + this.stopDateTime).subscribe(result => {
      this.getHistoryValues = result;
    }, error => console.error(error));



    var documentWidth: number = document.body.clientWidth;
    // configure margins and width/height of the graph
    if (documentWidth > 768) {
      this.width = (documentWidth - documentWidth / 15 - 200) - this.margin.left - this.margin.right;
    } else {
      this.width = (documentWidth - documentWidth / 5) - this.margin.left - this.margin.right;
    }
    this.height = 460 - this.margin.top - this.margin.bottom;



    // если массив параметров в модели не пустой выводим его сразу в дерево 
    if (this.treeParams.getTreeParameters.length != 0) {
      this.arrayParameters = this.treeParams.getTreeParameters;
      this.permission = 1;  // блокируем выгрузку массива из галактики
    }

    this.treeParams.unToggleMarkRemoveDateTime();         // обнуляем отметку изменения даты/времени в модели
    this.treeParams.unToggleMarkRemoveSelectedPrms();     // обнуляем отметку изменения выбора параметров в модели
  }



  ngOnInit() {

   //  this.initForm();

    if (this.treeParams.zoomMemory.length > 0) {   // если массив истории зуммирования не пустой раскрываем кнопку возврата предыдущих итераций
      (<HTMLInputElement>document.getElementById("returnButton")).style.display = 'block';
    }

    if (this.treeParams.getTreeParametersUrl == '') {

      // при загрузке страницы выводим последний час

      var currentDateTime: any = new Date();        // Выводим текущую дату

      (<HTMLInputElement>document.getElementById("dateStop")).value = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);
      (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + currentDateTime.getHours()).slice(-2) + ':00';
      (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + currentDateTime.getMinutes()).slice(-2);

      var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

      var lastDateTime: any = new Date(currentDateTime - eraUnix - 3600000);

      (<HTMLInputElement>document.getElementById("dateStart")).value = lastDateTime.getFullYear() + '-' + ('0' + (lastDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + lastDateTime.getDate()).slice(-2);
      (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + lastDateTime.getHours()).slice(-2) + ':00';
      (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + lastDateTime.getMinutes()).slice(-2);

    } else {

      var currentDateTime: any = new Date(this.treeParams.startDateTime);        // Выводим выгружаемую дату

      (<HTMLInputElement>document.getElementById("dateStart")).value = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);
      (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + currentDateTime.getHours()).slice(-2) + ':00';
      (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + currentDateTime.getMinutes()).slice(-2);

      var lastDateTime: any = new Date(this.treeParams.stopDateTime);

      (<HTMLInputElement>document.getElementById("dateStop")).value = lastDateTime.getFullYear() + '-' + ('0' + (lastDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + lastDateTime.getDate()).slice(-2);
      (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + lastDateTime.getHours()).slice(-2) + ':00';
      (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + lastDateTime.getMinutes()).slice(-2);
    }






    this.startDateTime = (<HTMLInputElement>document.getElementById("dateStart")).value + ' ' + (<HTMLInputElement>document.getElementById("timeStart")).value;
    this.stopDateTime = (<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + (<HTMLInputElement>document.getElementById("timeStop")).value;

    this.buildSvg1();
    this.buildSvg2();
    this.buildSvg3();

    if (this.treeParams.getArraySelectedPrms.length == 0) {     // если ранее не было выбрано ни одного параметра (массив в модели пустой)
      this.fullRequest();
    }

     this.getClient();
  }






  unHideToolbarMenu() {       // скрытие меню инструментов
    var hiddenToolBar = (<HTMLInputElement>document.getElementById("unHiddenToolbar")).className;
    if (hiddenToolBar == 'unhidden_toolbar') {
      (<HTMLInputElement>document.getElementById("unHiddenToolbar")).className = 'hidden_toolbar';
      (<HTMLInputElement>document.getElementById('toolbarTitle')).style.display = 'none';
    } else {
      (<HTMLInputElement>document.getElementById("unHiddenToolbar")).className = 'unhidden_toolbar';
      (<HTMLInputElement>document.getElementById('toolbarTitle')).style.display = 'inline-block';
    }
  }



  testGraphic() {        // вывод координатной сетки при старте страницы
     
    this.startDateTime = (<HTMLInputElement>document.getElementById("dateStart")).value + ' ' + (<HTMLInputElement>document.getElementById("timeStart")).value;
    this.stopDateTime = (<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + (<HTMLInputElement>document.getElementById("timeStop")).value;

    this.data1 = [];
    this.data2 = [];
    this.data3 = [];
    this.data4 = [];

    this.fullRequest();
  }



  public timePoint1: any = '';
  public timePoint2: any = '';
  public xSelectRect: any = '';


                                  // функции масштабирования графика выделением участка
  sensorScale() {

    if (this.timePoint1 == '') {

      this.timePoint1 = new Date((<HTMLInputElement>document.getElementById("selectTime")).value);

      var lineX: any = (<HTMLInputElement>document.getElementById("lineX")).value;
      (<HTMLInputElement>document.getElementById("selectRect")).setAttribute("x", lineX);
      this.xSelectRect = lineX;
    } else {

      this.treeParams.toggleMarkRemoveDateTime();      // отмечаем, что произошли изменения даты/времени старта и завершения

      this.timePoint2 = new Date((<HTMLInputElement>document.getElementById("selectTime")).value);

      var timeDifference = this.timePoint2 - this.timePoint1;

      if (Math.abs(timeDifference) > 300000) {

        // запоминаем начало и завершение текущего временного интервала для возврата

        var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
        startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут
        var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
        stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут
        this.treeParams.zoomMemory.unshift({ dateTimeStart: (<HTMLInputElement>document.getElementById("dateStart")).value + ' ' + startTime, dateTimeStop: (<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime });
        (<HTMLInputElement>document.getElementById("returnButton")).style.display = 'block';



        if (Number(timeDifference) > 0) {
          (<HTMLInputElement>document.getElementById("dateStart")).value = this.timePoint1.getFullYear() + '-' + ('0' + (this.timePoint1.getMonth() + 1)).slice(-2) + '-' + ('0' + this.timePoint1.getDate()).slice(-2);
          (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + this.timePoint1.getHours()).slice(-2) + ':00';
          (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + this.timePoint1.getMinutes()).slice(-2);
          (<HTMLInputElement>document.getElementById("dateStop")).value = this.timePoint2.getFullYear() + '-' + ('0' + (this.timePoint2.getMonth() + 1)).slice(-2) + '-' + ('0' + this.timePoint2.getDate()).slice(-2);
          (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + this.timePoint2.getHours()).slice(-2) + ':00';
          (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + this.timePoint2.getMinutes()).slice(-2);
        } else {
          (<HTMLInputElement>document.getElementById("dateStart")).value = this.timePoint2.getFullYear() + '-' + ('0' + (this.timePoint2.getMonth() + 1)).slice(-2) + '-' + ('0' + this.timePoint2.getDate()).slice(-2);
          (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + this.timePoint2.getHours()).slice(-2) + ':00';
          (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + this.timePoint2.getMinutes()).slice(-2);
          (<HTMLInputElement>document.getElementById("dateStop")).value = this.timePoint1.getFullYear() + '-' + ('0' + (this.timePoint1.getMonth() + 1)).slice(-2) + '-' + ('0' + this.timePoint1.getDate()).slice(-2);
          (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + this.timePoint1.getHours()).slice(-2) + ':00';
          (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + this.timePoint1.getMinutes()).slice(-2);
        }

        (<HTMLInputElement>document.getElementById("showChartDescription")).style.left = 0 + 'px';
        (<HTMLInputElement>document.getElementById("showChartDescription")).style.top = 1200 + 'px';
        (<HTMLInputElement>document.getElementById("showChartDescription")).style.display = 'none';

        this.getClient();
      }

      this.timePoint1 = '';
      this.timePoint2 = '';

      (<HTMLInputElement>document.getElementById("selectRect")).setAttribute("width", '0');
      (<HTMLInputElement>document.getElementById("selectRect")).setAttribute("x", '0');
    }
  }





  returnZoom() {

    this.treeParams.toggleMarkRemoveDateTime();    // отмечаем, что произошли изменения даты/времени старта и завершения

    let start = new Date(this.treeParams.zoomMemory[0].dateTimeStart);
    let stop = new Date(this.treeParams.zoomMemory[0].dateTimeStop);

    this.treeParams.zoomMemory.shift();
    if (this.treeParams.zoomMemory.length < 1) {
      (<HTMLInputElement>document.getElementById("returnButton")).style.display = 'none';
    }

    (<HTMLInputElement>document.getElementById("dateStart")).value = start.getFullYear() + '-' + ('0' + (start.getMonth() + 1)).slice(-2) + '-' + ('0' + start.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + start.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + start.getMinutes()).slice(-2);

    (<HTMLInputElement>document.getElementById("dateStop")).value = stop.getFullYear() + '-' + ('0' + (stop.getMonth() + 1)).slice(-2) + '-' + ('0' + stop.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + stop.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + stop.getMinutes()).slice(-2);

    this.getClient();
  }





  public identif: string = '';
  public styleDisplay: any;
  private permission: number = 0;

  treeParameters() {
    var currentUrl: any = document.location.href;
    currentUrl = currentUrl.split('/');
    if (currentUrl.length < 5) {
      currentUrl = 'Main';
    } else {
      currentUrl = currentUrl[4];
    }

    var testLoadMenu = (<HTMLInputElement>document.getElementById(currentUrl));

    if (testLoadMenu) {
      currentUrl = (<HTMLInputElement>document.getElementById(currentUrl)).value;
      this.treeParametersUrl = this.treeParametersUrl + '/' + currentUrl;

      this.styleDisplay = (<HTMLInputElement>document.getElementById('one_1')).style.display;
      if (this.styleDisplay == 'none') {
        (<HTMLInputElement>document.getElementById('one_1')).style.display = 'table';
        (<HTMLInputElement>document.getElementById('rolloverDown')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('rolloverUp')).style.display = 'block';

        // возвращаем checked ранее выделенных параметров
        if (this.treeParams.getSelectedParameters.length != 0) {
          for (let i = 0; i < this.treeParams.getSelectedParameters.length; i++) {
            (<HTMLInputElement>document.getElementById(this.treeParams.getSelectedParameters[i])).checked = true;
          }
        }

      } else {
        (<HTMLInputElement>document.getElementById('one_1')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('rolloverDown')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('rolloverUp')).style.display = 'none';
      }

      if (this.permission == 0) {     // если разрешение на повторную выгрузку параметров из галактики открыто - осуществляем
        this.http.get<ArrayParameters[]>(this.treeParametersUrl).subscribe(result => {
          this.arrayParameters = result;
          for (let i = 0; i < this.arrayParameters.length; i++) {      // если имя параметра пустое - заменяем его на системный тег 
            if (this.arrayParameters[i].shortDesc == '') {
              this.arrayParameters[i].shortDesc = this.arrayParameters[i].tagName;
            }
          }

          this.treeParams.getTreeParameters = this.arrayParameters;   // передаем массив дерева в модель

        }, error => {
            console.error(error);

            this.http.get<ArrayParameters[]>('assets/replase_server/' + currentUrl+'.json').subscribe((result) => {
              this.arrayParameters = result;
              for (let i = 0; i < this.arrayParameters.length; i++) {      // если имя параметра пустое - заменяем его на системный тег 
                if (this.arrayParameters[i].shortDesc == '') {
                  this.arrayParameters[i].shortDesc = this.arrayParameters[i].tagName;
                }
              }

              this.treeParams.getTreeParameters = this.arrayParameters;   // передаем массив дерева в модель

            });
        });

        this.permission = 1;                   // закрываем разрешение на повторную выгрузку массива закрываем
      }



    } else {
      alert('Меню навигации еще не догрузилось!');
    }
  }



  unHideTree() {
    this.styleDisplay = (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display;
    if (this.styleDisplay == 'none') {
      (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display = 'table';
      (<HTMLInputElement>document.getElementById('arrowOne_' + this.identif)).innerHTML = 'v';
    } else {
      (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display = 'none';
      (<HTMLInputElement>document.getElementById('arrowOne_' + this.identif)).innerHTML = '>';
    }
  }




  allAttributes(counterObject) {  // выделение всех параметров объекта (родительский checkbox выделяет все дочерние)
    if ((<HTMLInputElement>document.getElementById('callObject_' + (counterObject + 1))).checked) {
      for (let i = 0; i < this.arrayParameters[counterObject].attributes.length; i++) {
        (<HTMLInputElement>document.getElementById('callParameter_' + this.arrayParameters[counterObject].tagName + '_' + (i + 1))).checked = true;
      }
    } else {
      for (let i = 0; i < this.arrayParameters[counterObject].attributes.length; i++) {
        (<HTMLInputElement>document.getElementById('callParameter_' + this.arrayParameters[counterObject].tagName + '_' + (i + 1))).checked = false;
      }
    }
  }






  public selected_prms: string = '';

  public historyValues: any[] = [];
  public arrayForChart: any[] = [];       // выводим массив значений времени для вертикальной шкалы

  getClient() {
    this.timePoint1 = '';
    this.timePoint2 = '';

    var documentWidth: number = document.body.clientWidth;
    // configure margins and width/height of the graph
    this.width = (documentWidth - documentWidth / 15 - 200) - this.margin.left - this.margin.right;
    this.height = 460 - this.margin.top - this.margin.bottom;

    this.svg1.selectAll("*").remove();
    this.svg2.selectAll("*").remove();


    if (this.treeParams.markRemoveDateTime == 1) {   // если менялась отметка изменения даты/времени вызываем метод обнуления url
      this.treeParams.updateUrlModel();
    }

    if (this.treeParams.markRemoveSelectedPrms == 1) {  // если менялась отметка изменения выбора параметров вызываем метод обнуления массива выбранных параметров
      this.treeParams.updateSelectedPrmsModel();
    }


    /*  если в модели дерева параметров еще не было сохранения данных запроса -
        формируем запрос с нуля - считываем выбранные параметры с дерева панели chart-toolbar,
        что бы вложить их в строку запроса 
    */

    if (this.treeParams.getTreeParametersUrl == '') {   

      this.selected_prms = '';

      this.arrayForChart = [];
      var select_check: any;


      if (this.treeParams.getArraySelectedPrms.length == 0) {     // если ранее не было выбрано ни одного параметра (массив в модели пустой)

        // процедура выгрузки массива выбранных параметров
        for (let i = 0; i < this.arrayParameters.length; i++) {                    // выводим массив выбранных параметров 
          for (let j = 0; j < this.arrayParameters[i].attributes.length; j++) {
            select_check = (<HTMLInputElement>document.getElementById('callParameter_' + this.arrayParameters[i].tagName + '_' + (j + 1)));
            if (select_check.checked) {
              this.selected_prms = this.selected_prms + this.arrayParameters[i].tagName + '.' + this.arrayParameters[i].attributes[j].name + ',';  // формируем строку выбранных параметров для URL
              this.treeParams.getArraySelectedPrms.push({ tagName_name: this.arrayParameters[i].tagName + '.' + this.arrayParameters[i].attributes[j].name, historyValues: [], name: this.arrayParameters[i].attributes[j].name, short_desc: this.arrayParameters[i].shortDesc, description: this.arrayParameters[i].attributes[j].description, engUnits: this.arrayParameters[i].attributes[j].engUnits });

              this.treeParams.getSelectedParameters.push('callParameter_' + this.arrayParameters[i].tagName + '_' + (j + 1));   // массив id отмеченных checkbox-ов в модели
            }
          }
        }

        this.selected_prms = this.selected_prms.slice(0, this.selected_prms.length - 1);   // убираем лишнюю зяпятую в конце массива выбранных параметров

      } else {

        for (let i = 0; i < this.treeParams.getArraySelectedPrms.length; i++) {
          this.selected_prms = this.selected_prms + this.treeParams.getArraySelectedPrms[i].tagName_name + ',';
        }
        this.selected_prms = this.selected_prms.slice(0, this.selected_prms.length - 1);   // убираем лишнюю зяпятую в конце строки выбранных параметров для URL

      }


      var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
      startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут

      var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
      stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут

      this.startDateTime = (<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + startTime;
      this.stopDateTime = (<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime;


      // если время завершения - будущее - преврашаем его в текущее
      var currentDateTime: any = new Date();        // Выводим текущую дату
      var buferStopDateTume: any = new Date(this.stopDateTime);
      if (buferStopDateTume - currentDateTime > 0) {
        (<HTMLInputElement>document.getElementById("dateStop")).value = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);
        (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + currentDateTime.getHours()).slice(-2) + ':00';
        (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + currentDateTime.getMinutes()).slice(-2);

        var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
        stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут
        this.stopDateTime = (<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime;
      }


      this.treeParams.startDateTime = this.startDateTime;    // отправляем точки времени в модель
      this.treeParams.stopDateTime = this.stopDateTime;


      this.getHistoryValuesUrl = this.baseUrl + 'api/PrmValues/GetHistoryValues' + '/' + this.selected_prms + '/' + this.startDateTime + '/' + this.stopDateTime;

      // передаем данные в модель
      this.treeParams.getTreeParametersUrl = this.getHistoryValuesUrl;

    } else {

      /*
       если ранее параметры запроса сохранялись в модель -
       повторяем проделанный ранее запрос по сохраненному url для возврата в исходное
       отображение данных на экране, которое было до покидания раздела Анализ
       */

      this.getHistoryValuesUrl = this.treeParams.getTreeParametersUrl;

    }

   

    this.http.get<ArrayParameters[]>(this.getHistoryValuesUrl).subscribe(result => {

      this.getHistoryValues = result;

      // bodyHttp start предполагается вставить метод

      this.yMaxLimit = 0;    // зануляем максимум
      this.yMinLimit = 1000000;    // увеличиваем минимум
      this.leftOffsetForBarChart = 0;   // зануляем смещение интеграционных диаграм

      /* формирование массива выбранных параметров с выводом истории по каждому из них
         вывод стартовых значений для формирования систем координат 
       */
      for (let i = 0; i < this.treeParams.getArraySelectedPrms.length; i++) {

        if (this.treeParams.getTreeParametersUrl == '') {  // c массивом treeParams.getArraySelectedPrms[i].historyValues работаем только без предварительного вывода данных
          this.treeParams.getArraySelectedPrms[i].historyValues.push({ val: 0 }); // определяем минимальное значение шкалы
        }

        for (let j = 0; j < this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name].length; j++) {  // получен ассоциативный массив - по ключу обращаемся к дочернему нумерованному, для получения длины
          if (j == 0) {   // зануляем первое значение
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }

          if (this.treeParams.getTreeParametersUrl == '') { // c массивом treeParams.getArraySelectedPrms[i].historyValues работаем только без предварительного вывода данных
            if (this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val != 'NaN') {
              this.treeParams.getArraySelectedPrms[i].historyValues.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val) });
            } else {
              this.treeParams.getArraySelectedPrms[i].historyValues.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: 0 });
            }
          }

          if (this.yMaxLimit < Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val)) { // Выводим максимум
            this.yMaxLimit = Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val);
          }
          if (this.yMinLimit > Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val)) { // Выводим минимум
            this.yMinLimit = Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val);
          }


          if (j == this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name].length-1) {   // зануляем последнее значение
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: this.yMaxLimit });
          }
        }

      }

      this.activeAddXandYAxis1();
      this.selectRect();
      this.addObject1();
      this.activeAddXandYAxis2();
      this.addObject2();

      for (let i = 0; i < this.treeParams.getArraySelectedPrms.length; i++) {

        this.maxValue = 0;        // приводим параметры интеграционных диаграм в исходное значение
        this.minValue = 1000000;
        this.arrayAttributesCharts = [];

        this.arrayForChart = [];
        for (let j = 0; j < this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name].length; j++) {
          if (j == 0) {     // зануляем первое значение
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }

          if (this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val != 'NaN') {
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val) });

            if (this.maxValue < this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val) { // максимум интеграционных диаграм
              this.maxValue = this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val;
            }
            if (this.minValue > this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val) { // минимум интеграционных диаграм
              this.minValue = this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val;
            }
          } else {
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }

          if (j == this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name].length - 1) {  // зануляем последнее значение
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: 0 }); // зануляем последнее значение для завершения графика
          }
        }

        this.idChart = this.treeParams.getArraySelectedPrms[i].short_desc + '_' + this.treeParams.getArraySelectedPrms[i].description + '_' + i + '_' + this.treeParams.getArraySelectedPrms[i].engUnits;
        if ((<HTMLInputElement>document.getElementById('toolbarColoring')).style.display == 'none') {
          this.arrayAttributesCharts.push({ id: 'chart_' + this.idChart, short_desc: this.treeParams.getArraySelectedPrms[i].short_desc, description: this.treeParams.getArraySelectedPrms[i].description, stroke: '#feb41b' });
        }
        

        this.activeLineAndPath1(i);   // вывод графиков
        this.activeAddBarChart();   // вывод интеграционных диаграм
        this.leftOffsetForBarChart = this.leftOffsetForBarChart + 200;
      }

      this.lineCursor(); // линии прицела курсора

      for (let i = 0; i < this.treeParams.getArraySelectedPrms.length; i++) {  // дополнительный цикл для невидимых линий интерактивной подсказки
        this.arrayForChart = [];
        for (let j = 0; j < this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name].length; j++) {

          if (this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val != 'NaN') {
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val) });
          } else {
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }

        }
        this.idChart = this.treeParams.getArraySelectedPrms[i].short_desc + '_' + this.treeParams.getArraySelectedPrms[i].description + '_' + i + '_' + this.treeParams.getArraySelectedPrms[i].engUnits;
        this.descriptionLine();
      }

      // bodyHttp stop предполагается вставить метод

    }, error => {
        console.error(error);

        var currentUrl: any = document.location.href;
        currentUrl = currentUrl.split('/');
        if (currentUrl.length < 5) {
          currentUrl = 'Main';
        } else {
          currentUrl = currentUrl[4];
        }

        this.arrayForChart = [];

        // alert(JSON.stringify(this.treeParams.getArraySelectedPrms, null, '\t'));


        this.http.get<ArrayParameters[]>('assets/replase_server/historyValues/' + currentUrl + '.json').subscribe((result) => {

          this.getHistoryValues = result;

          // alert(JSON.stringify(this.getHistoryValues));
          this.bodyHttp();

        });

    });


    this.treeParams.unToggleMarkRemoveDateTime();         // убираем отметку изменения даты/времени 
    this.treeParams.unToggleMarkRemoveSelectedPrms();     // убираем отметку изменения выбора параметров

  }


  bodyHttp() {   // метод формирования массива данных для граффического представления

    this.yMaxLimit = 0;    // зануляем максимум
    this.yMinLimit = 1000000;    // увеличиваем минимум
    this.leftOffsetForBarChart = 0;   // зануляем смещение интеграционных диаграм
    this.arrayAttributesCharts = [];

    /* формирование массива выбранных параметров с выводом истории по каждому из них
       вывод стартовых значений для формирования систем координат 
     */
    for (let i = 0; i < this.treeParams.getArraySelectedPrms.length; i++) {

      if (this.treeParams.getTreeParametersUrl == '') {  // c массивом treeParams.getArraySelectedPrms[i].historyValues работаем только без предварительного вывода данных
        this.treeParams.getArraySelectedPrms[i].historyValues.push({ val: 0 }); // определяем минимальное значение шкалы
      }

      for (let j = 0; j < this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name].length; j++) {  // получен ассоциативный массив - по ключу обращаемся к дочернему нумерованному, для получения длины
        if (j == 0) {   // зануляем первое значение
          this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: 0 });
        }

        if (this.treeParams.getTreeParametersUrl == '') { // c массивом treeParams.getArraySelectedPrms[i].historyValues работаем только без предварительного вывода данных
          if (this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val != 'NaN') {
            this.treeParams.getArraySelectedPrms[i].historyValues.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val) });
          } else {
            this.treeParams.getArraySelectedPrms[i].historyValues.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }
        }

        if (this.yMaxLimit < Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val)) { // Выводим максимум
          this.yMaxLimit = Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val);
        }
        if (this.yMinLimit > Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val)) { // Выводим минимум
          this.yMinLimit = Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val);
        }


        if (j == this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name].length - 1) {   // зануляем последнее значение
          this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: this.yMaxLimit });
        }
      }

    }

    this.activeAddXandYAxis1();
    this.selectRect();
    this.addObject1();
    this.activeAddXandYAxis2();
    this.addObject2();

    for (let i = 0; i < this.treeParams.getArraySelectedPrms.length; i++) {

      this.maxValue = 0;        // приводим параметры интеграционных диаграм в исходное значение
      this.minValue = 1000000;

      this.arrayForChart = [];
      for (let j = 0; j < this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name].length; j++) {
        if (j == 0) {     // зануляем первое значение
          if ((<HTMLInputElement>document.getElementById("toolbarColoring")).style.display == "none") {
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }
        }

        if (this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val != 'NaN') {
          this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val) });

          if (this.maxValue < this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val) { // максимум интеграционных диаграм
            this.maxValue = this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val;
          }
          if (this.minValue > this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val) { // минимум интеграционных диаграм
            this.minValue = this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val;
          }
        } else {
          this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: 0 });
        }

        if (j == this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name].length - 1) {  // зануляем последнее значение
          if ((<HTMLInputElement>document.getElementById("toolbarColoring")).style.display == "none") {
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: 0 }); // зануляем последнее значение для завершения графика
          }
        }

      }

      this.idChart = this.treeParams.getArraySelectedPrms[i].short_desc + '_' + this.treeParams.getArraySelectedPrms[i].description + '_' + i + '_' + this.treeParams.getArraySelectedPrms[i].engUnits;
      var strokeColor: string;
      if ((<HTMLInputElement>document.getElementById('toolbarColoring')).style.display == 'none') {
        strokeColor = '#feb41b';
      } else {
        strokeColor = this.treeParams.arrayColoring[i];
      }
      this.arrayAttributesCharts.push({ id: 'chart_' + this.idChart, short_desc: this.treeParams.getArraySelectedPrms[i].short_desc, description: this.treeParams.getArraySelectedPrms[i].description, stroke: strokeColor });

      this.activeLineAndPath1(i);   // вывод графиков
      this.activeAddBarChart();   // вывод интеграционных диаграм
      this.leftOffsetForBarChart = this.leftOffsetForBarChart + 200;
    }

    this.lineCursor(); // линии прицела курсора

    for (let i = 0; i < this.treeParams.getArraySelectedPrms.length; i++) {  // дополнительный цикл для невидимых линий интерактивной подсказки
      this.arrayForChart = [];
      for (let j = 0; j < this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name].length; j++) {

        if (this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val != 'NaN') {
          this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: Number(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].val) });
        } else {
          this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.treeParams.getArraySelectedPrms[i].tagName_name][j].dt), val: 0 });
        }

      }
      this.idChart = this.treeParams.getArraySelectedPrms[i].short_desc + '_' + this.treeParams.getArraySelectedPrms[i].description + '_' + i + '_' + this.treeParams.getArraySelectedPrms[i].engUnits;
      this.descriptionLine();
    }
  }





  private activeAddXandYAxis1() {                        // координатные оси первого SVG-шаблона
    // range of data configuring
    this.x = d3Scale.scaleTime().range([0, this.width]);
    this.y = d3Scale.scaleLinear().range([this.height, 0]);
    this.x.domain(d3Array.extent(this.arrayForChart, (d) => d.dt));
    this.y.domain(d3Array.extent(this.arrayForChart, (d) => d.val));

    // Configure the X Axis
    this.svg1.append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .attr('style', 'stroke-opacity: 0.1')
      .call(d3Axis.axisBottom(this.x)
        .tickSize(-this.height)
        .ticks(10));

    // Configure the Y Axis
    this.svg1.append('g')
      .attr('style', 'stroke-opacity: 0.2')
      .call(d3Axis.axisLeft(this.y)
        .tickSize(-this.width)
        .ticks(10))
  }

  private selectRect() {       // селекторный прямоугольник
    this.svg1.append('rect')           
      .attr("width", 0)
      .attr("height", this.height)
      .attr("x", 0)
      .attr("y", 0)
      .attr('stroke', '#feb41b')
      .attr('fill', 'rgba(0,0,0,0.05)')
      .attr('stroke-width', '0')
      .attr('id', 'selectRect');
  }



  private activeAddXandYAxis2() {
    // range of data configuring
    this.x = d3Scale.scaleTime().range([0, this.width]);
    this.y = d3Scale.scaleLinear().range([this.height, 0]);
    this.x.domain(d3Array.extent(this.arrayForChart, (d) => d.dt));
    this.y.domain(d3Array.extent(this.arrayForChart, (d) => d.val));

    // Configure the X Axis
    this.svg2.append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .attr('style', 'stroke-opacity: 0.1')
      .call(d3Axis.axisBottom(this.x)
        .ticks(0));

    // Configure the Y Axis
    this.svg2.append('g')
      .attr('style', 'stroke-opacity: 0.2')
      .call(d3Axis.axisLeft(this.y)
        .tickSize(-this.width)
        .ticks(10))
  }



  private activeLineAndPath1(numberStroke) {        // вывод графиков
    var strokeColor: string;
    var fillColor: string;
    if ((<HTMLInputElement>document.getElementById('toolbarColoring')).style.display == 'none') {
      strokeColor = '#feb41b';
      fillColor = 'rgba(255, 150, 0, 0.1)';
    } else {
      strokeColor = this.arrayAttributesCharts[numberStroke].stroke;
      fillColor = 'none';
    }
    this.line = d3Shape.line()
      .x((d: any) => this.x(d.dt))
      .y((d: any) => this.y(d.val));
    // Configuring line path
    this.svg1.append('path')
      .datum(this.arrayForChart)
      .attr('class', 'line over_line')
      .attr('stroke', strokeColor)
      .attr('fill', fillColor)         
      .attr('stroke-width', '2')
      .attr('d', this.line)
      .attr('id', 'chart_' + this.idChart);
  }




  private lineCursor() {                // линии пересечения на курсоре
    this.svg1.append("line")                             
      .attr('stroke', 'rgba(0, 0, 0, 0)')
      .attr('stroke-width', '0.5')
      .attr("x1", 50)
      .attr("y1", 0)
      .attr("x2", 50)
      .attr("y2", this.height)
      .attr("id", 'xLineCursor');
    this.svg1.append("line")
      .attr('stroke', 'rgba(0, 0, 0, 0)')
      .attr('stroke-width', '0.5')
      .attr("x1", 0)
      .attr("y1", 100)
      .attr("x2", this.width)
      .attr("y2", 100)
      .attr("id", 'yLineCursor');
  }

  

  descriptionLine() {                           // невидимые линии для вывода подсказок при наведении
    this.line = d3Shape.line()
      .x((d: any) => this.x(d.dt))
      .y((d: any) => this.y(d.val));

    // Configuring line path
    this.svg1.append('path')
      .datum(this.arrayForChart)
      .attr('class', 'line over_line')
      .attr('stroke', 'rgba(255, 240, 0, 0)')
      .attr('fill', 'none')         
      .attr('stroke-width', '16')
      .attr('d', this.line)
      .attr('id', this.idChart)
      .on('mouseover', function (d) {                     // интерактив графика

        this.style.stroke = 'rgba(255, 240, 0, 0.05)';

        var obj: any = (<HTMLInputElement>document.getElementById("group1"));
        var rect: any = obj.getBoundingClientRect();
        var splitId: any = this.id.split('_');           // переменная вывода наименования объекта, который имеет данный параметр и сам параметр

        if ((<HTMLInputElement>document.getElementById("toolbarColoring")).style.display == "none") {
          (<HTMLInputElement>document.getElementById("chart_" + this.id)).style.stroke = 'rgb(255, 10, 0)';
          (<HTMLInputElement>document.getElementById("chart_" + this.id)).style.fill = 'rgba(255, 240, 0, 0.3)';
        } else {
          (<HTMLInputElement>document.getElementById("chart_" + this.id)).style.strokeWidth = '4';
        }
        (<HTMLInputElement>document.getElementById("xLineCursor")).style.stroke = 'rgba(0, 0, 0, 0.5)';
        (<HTMLInputElement>document.getElementById("yLineCursor")).style.stroke = 'rgba(0, 0, 0, 0.5)';

        let elem = document.getElementById('showChartDescription');
        document.addEventListener('mousemove', function (event) {
          
          (<HTMLInputElement>document.getElementById("showChartDescription")).style.left = event.pageX + 10 + 'px';
          (<HTMLInputElement>document.getElementById("showChartDescription")).style.top = event.pageY - window.pageYOffset + 20 + 'px';

          var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
          startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут

          var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
          stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут

          var startDateTime: any = new Date((<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + startTime);
          var stopDateTime: any = new Date((<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime);

          var height: any = (<HTMLInputElement>document.getElementById("yAxisHeight")).value;
          height = Number(height);
          var width: any = (<HTMLInputElement>document.getElementById("xAxisWidth")).value;
          width = Number(width);
          var marginTop: any = (<HTMLInputElement>document.getElementById("marginTop")).value;
          var marginLeft: any = (<HTMLInputElement>document.getElementById("marginLeft")).value;
          var yMaxLimit: any = (<HTMLInputElement>document.getElementById("yMaxLimit")).value;

          var fullInterval: any = stopDateTime - startDateTime;
          var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));
          eraUnix = startDateTime - eraUnix;

          var lineX: any = '' + (event.pageX - rect.left - marginLeft);
          var lineY: any = event.pageY - window.pageYOffset - rect.top - marginTop;

          (<HTMLInputElement>document.getElementById("xLineCursor")).setAttribute("x1", lineX);
          (<HTMLInputElement>document.getElementById("xLineCursor")).setAttribute("x2", lineX);

          (<HTMLInputElement>document.getElementById("yLineCursor")).setAttribute("y1", lineY);
          (<HTMLInputElement>document.getElementById("yLineCursor")).setAttribute("y2", lineY);

          var timePoint1 = (<HTMLInputElement>document.getElementById("timePoint1")).value;
          var timePoint2 = (<HTMLInputElement>document.getElementById("timePoint2")).value;
          var xSelectRect: any = (<HTMLInputElement>document.getElementById("xSelectRect")).value; 

          if (timePoint1 != '') {
            if (timePoint2 == '') {
              xSelectRect = Number(xSelectRect);
              var widthSelectRect: any = Math.abs(lineX - xSelectRect);
              if (Number(lineX) < xSelectRect) {
                (<HTMLInputElement>document.getElementById("selectRect")).setAttribute("width", widthSelectRect);
                (<HTMLInputElement>document.getElementById("selectRect")).setAttribute("x", lineX);
              } else {
                (<HTMLInputElement>document.getElementById("selectRect")).setAttribute("width", widthSelectRect);
                (<HTMLInputElement>document.getElementById("selectRect")).setAttribute("x", xSelectRect);
              }
            }
          }

          var selectTime: any = new Date(eraUnix + (fullInterval * (event.pageX - rect.left - marginLeft) / width));  // выводим выделенную временную точку
          var timeBufer = selectTime;
          var selectDate = ('0' + selectTime.getDate()).slice(-2) + '.' + ('0' + (selectTime.getMonth() + 1)) + '.' + selectTime.getFullYear();
          selectTime = ('0'+selectTime.getHours()).slice(-2) + ':' + ('0' + selectTime.getMinutes()).slice(-2) + ':' + ('0' + selectTime.getSeconds()).slice(-2);

          var value: any = (height - (event.pageY - window.pageYOffset - rect.top - marginTop)) * yMaxLimit / height;  // выводим значение значение параметра в данной точке
          value = value.toFixed(3);

          (<HTMLInputElement>document.getElementById("showChartDescription")).innerHTML = '<div style="display: inline-block; background: #FFE4B5; padding: 1px 10px 1px 10px; border-radius: 3px; border: solid 1px #B22222;; ">' + splitId[0] + '</div><input type="hidden" id="lineX" value="' + lineX + '" /><input id="selectTime" type="hidden" value="' + timeBufer + '" /> ' + '<br><div style="display: inline-block; background: #FFE4B5; border: solid 1px #B22222; border-radius: 3px; margin-top: 5px; padding: 1px 10px 1px 10px;">' + splitId[1] + '</div><br><div style="display: inline-block; background: #FFFACD; border: solid 1px #B22222; margin-top: 5px; padding: 1px 10px 1px 10px; border-radius: 3px; color: #B22222; ">' + selectDate + '<br>' + selectTime + '</div><br><div style="display: inline-block; background: #fff; margin-top: 5px; padding: 3px 10px 3px 10px; border: solid 2px #B22222; border-radius: 3px;"><span style="color: #B22222;">' + value + '</span>  <span style="color: #555; margin-left: 5px;">' + splitId[3] + '</span></div>';
        });
        (<HTMLInputElement>document.getElementById("showChartDescription")).style.display = 'block';

        
      })
      .on('mouseout', function () {
        this.style.stroke = 'rgba(255, 240, 0, 0)';

        if ((<HTMLInputElement>document.getElementById("toolbarColoring")).style.display == "none") {
          (<HTMLInputElement>document.getElementById("chart_" + this.id)).style.stroke = '#feb41b';
          (<HTMLInputElement>document.getElementById("chart_" + this.id)).style.fill = 'rgba(255, 150, 0, 0.1)';
        } else {
          (<HTMLInputElement>document.getElementById("chart_" + this.id)).style.strokeWidth = '2';
        }

        (<HTMLInputElement>document.getElementById("xLineCursor")).style.stroke = 'rgba(0, 0, 0, 0)';
        (<HTMLInputElement>document.getElementById("yLineCursor")).style.stroke = 'rgba(0, 0, 0, 0)';

        (<HTMLInputElement>document.getElementById("showChartDescription")).style.left = 0 + 'px';
        (<HTMLInputElement>document.getElementById("showChartDescription")).style.top = 1200 + 'px';
        (<HTMLInputElement>document.getElementById("showChartDescription")).style.display = 'none';
      });
  }




  private activeAddBarChart() {

    var splitText: any = this.idChart.split('_');           // переменная вывода наименования объекта, который имеет данный параметр и сам параметр

    this.mediumValue = (this.maxValue + this.minValue) / 2;
    this.svg2.append('text')               // наименование процесса
      .attr('text-anchor', 'medium')
      .attr('transform', 'rotate(-90)')
      .attr('y', this.leftOffsetForBarChart + 25)
      .attr('x', -this.height + 20)
      .text(splitText[0]);
    this.svg2.append('text')               // наименование процесса
      .attr('text-anchor', 'medium')
      .attr('transform', 'rotate(-90)')
      .attr('y', this.leftOffsetForBarChart + 45)
      .attr('x', -this.height + 20)
      .text(splitText[1]);
    this.svg2.append('rect')           // столбец максимума процесса
      .attr("width", 40)
      .attr("height", this.height * (this.maxValue / this.yMaxLimit))
      .attr("x", this.leftOffsetForBarChart + 60)
      .attr("y", this.height - this.height * (this.maxValue / this.yMaxLimit))
      .attr('stroke', '#feb41b')
      .attr('fill', '#FFEBCD')
      .attr('stroke-width', '2');
    this.svg2.append("line")              // линия максимума процесса
      .attr('stroke', '#feb41b')
      .attr('stroke-width', '2')
      .attr("x1", this.leftOffsetForBarChart + 60)
      .attr("y1", this.height - this.height * (this.maxValue / this.yMaxLimit))
      .attr("x2", this.leftOffsetForBarChart + 120)
      .attr("y2", this.height - this.height * (this.maxValue / this.yMaxLimit));
    this.svg2.append('text')                    // текст максимума процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.maxValue / this.yMaxLimit))
      .attr('x', this.leftOffsetForBarChart + 130)
      .text(this.maxValue.toFixed(3));
    this.svg2.append('rect')                 // столбец медиума первого процесса
      .attr("width", 40)
      .attr("height", this.height * (this.mediumValue / this.yMaxLimit))
      .attr("x", this.leftOffsetForBarChart + 70)
      .attr("y", this.height - this.height * (this.mediumValue / this.yMaxLimit))
      .attr('stroke', '#feb41b')
      .attr('fill', '#FFE4C4')
      .attr('stroke-width', '2');
    this.svg2.append("line")                 // линия медиума процесса
      .attr('stroke', '#feb41b')
      .attr('stroke-width', '2')
      .attr("x1", this.leftOffsetForBarChart + 70)
      .attr("y1", this.height - this.height * (this.mediumValue / this.yMaxLimit))
      .attr("x2", this.leftOffsetForBarChart + 130)
      .attr("y2", this.height - this.height * (this.mediumValue / this.yMaxLimit));
    this.svg2.append('text')                   // текст медиума первого процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.mediumValue / this.yMaxLimit))
      .attr('x', this.leftOffsetForBarChart + 140)
      .text(this.mediumValue.toFixed(3));
    this.svg2.append('rect')                  // столбец минимума процесса
      .attr("width", 40)
      .attr("height", this.height * (this.minValue / this.yMaxLimit))
      .attr("x", this.leftOffsetForBarChart + 80)
      .attr("y", this.height - this.height * (this.minValue / this.yMaxLimit))
      .attr('stroke', '#feb41b')
      .attr('fill', '#FFDEAD')
      .attr('stroke-width', '2');
    this.svg2.append("line")               // линия минимума процесса
      .attr('stroke', '#feb41b')
      .attr('stroke-width', '2')
      .attr("x1", this.leftOffsetForBarChart + 80)
      .attr("y1", this.height - this.height * (this.minValue / this.yMaxLimit))
      .attr("x2", this.leftOffsetForBarChart + 140)
      .attr("y2", this.height - this.height * (this.minValue / this.yMaxLimit));
    this.svg2.append('text')                     // текст минимума процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.minValue / this.yMaxLimit))
      .attr('x', this.leftOffsetForBarChart + 150)
      .text(this.minValue.toFixed(3));

  }



  openToolbarColoring() {
    (<HTMLInputElement>document.getElementById("toolbarColoring")).style.display = "block";
    (<HTMLInputElement>document.getElementById("coloringButton")).style.display = "none";
    if (this.treeParams.getArraySelectedPrms.length != 0) {
      this.svg1.selectAll("*").remove();
      this.svg2.selectAll("*").remove();
      this.bodyHttp();
    }
  }

  replaceColor(idChart, strokeChart) {  // замена цвета линии графика
    (<HTMLInputElement>document.getElementById(idChart)).style.stroke = strokeChart;
    (<HTMLInputElement>document.getElementById(idChart)).style.fill = 'none';
  }

  closeToolbarColoring() {
    (<HTMLInputElement>document.getElementById("toolbarColoring")).style.display = "none";
    (<HTMLInputElement>document.getElementById("coloringButton")).style.display = "block";
    if (this.treeParams.getArraySelectedPrms.length != 0) {
      this.svg1.selectAll("*").remove();
      this.svg2.selectAll("*").remove();
      this.bodyHttp();
    }
  }





  rightScalePlus() {  // правое масштабирование на +

    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

    var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
    stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут

    var stopDateTime: any = new Date((<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime);
    stopDateTime = new Date(stopDateTime - eraUnix + 300000);


    (<HTMLInputElement>document.getElementById("dateStop")).value = stopDateTime.getFullYear() + '-' + ('0' + (stopDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + stopDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + stopDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + stopDateTime.getMinutes()).slice(-2);

    this.treeParams.getTreeParametersUrl = '';

    this.getClient();
  }


  rightScaleMinus() {    // правое масштабирование на -

    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

    var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
    stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут

    var stopDateTime: any = new Date((<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime);
    stopDateTime = new Date(stopDateTime - eraUnix - 300000);

    (<HTMLInputElement>document.getElementById("dateStop")).value = stopDateTime.getFullYear() + '-' + ('0' + (stopDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + stopDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + stopDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + stopDateTime.getMinutes()).slice(-2);

    this.treeParams.getTreeParametersUrl = '';

    this.getClient();
  }


  leftScaleMinus() {  // левое масштабирование на -

    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

    var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
    startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут

    var startDateTime: any = new Date((<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + startTime);
    startDateTime = new Date(startDateTime - eraUnix + 300000);

    (<HTMLInputElement>document.getElementById("dateStart")).value = startDateTime.getFullYear() + '-' + ('0' + (startDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + startDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + startDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + startDateTime.getMinutes()).slice(-2);

    this.treeParams.getTreeParametersUrl = '';

    this.getClient();
  }


  leftScalePlus() {    // левое масштабирование на +

    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

    var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
    startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут

    var startDateTime: any = new Date((<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + startTime);
    startDateTime = new Date(startDateTime - eraUnix - 300000);

    (<HTMLInputElement>document.getElementById("dateStart")).value = startDateTime.getFullYear() + '-' + ('0' + (startDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + startDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + startDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + startDateTime.getMinutes()).slice(-2);

    this.treeParams.getTreeParametersUrl = '';

    this.getClient();
  }


  moveRight() {    // сдвигаем временной интервал вправо 

    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

    var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
    stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут

    var stopDateTime: any = new Date((<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime);
    stopDateTime = new Date(stopDateTime - eraUnix + 300000);

    (<HTMLInputElement>document.getElementById("dateStop")).value = stopDateTime.getFullYear() + '-' + ('0' + (stopDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + stopDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + stopDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + stopDateTime.getMinutes()).slice(-2);


    var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
    startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут

    var startDateTime: any = new Date((<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + startTime);
    startDateTime = new Date(startDateTime - eraUnix + 300000);

    (<HTMLInputElement>document.getElementById("dateStart")).value = startDateTime.getFullYear() + '-' + ('0' + (startDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + startDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + startDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + startDateTime.getMinutes()).slice(-2);

    this.treeParams.getTreeParametersUrl = '';

    this.getClient();
  }


  moveLeft() {    // сдвигаем временной интервал влево

    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

    var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
    stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут

    var stopDateTime: any = new Date((<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime);
    stopDateTime = new Date(stopDateTime - eraUnix - 300000);

    (<HTMLInputElement>document.getElementById("dateStop")).value = stopDateTime.getFullYear() + '-' + ('0' + (stopDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + stopDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + stopDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + stopDateTime.getMinutes()).slice(-2);

    var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
    startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут

    var startDateTime: any = new Date((<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + startTime);
    startDateTime = new Date(startDateTime - eraUnix - 300000);

    (<HTMLInputElement>document.getElementById("dateStart")).value = startDateTime.getFullYear() + '-' + ('0' + (startDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + startDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + startDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + startDateTime.getMinutes()).slice(-2);

    this.treeParams.getTreeParametersUrl = '';

    this.getClient();
  }
  



  // для построения предварительной пустой координатной сетки используем запрос к генератору псевдослучайных коэффициентов "api/PrmValues"
  fullRequest() {
    this.startDateTime = this.startDateTime.slice(0, this.startDateTime.length - 2) + (<HTMLInputElement>document.getElementById("minStart")).value;
    this.stopDateTime = this.stopDateTime.slice(0, this.stopDateTime.length - 2) + (<HTMLInputElement>document.getElementById("minStop")).value;

    this.http.get<Value[]>(this.pathUrl + '/' + this.startDateTime + '/' + this.stopDateTime).subscribe(result => {
      this.values = result;

      this.svg1.selectAll("*").remove();
      this.svg2.selectAll("*").remove();
      this.svg3.selectAll("*").remove();

      this.data1 = [];
      this.data2 = [];
      this.data3 = [];
      this.data4 = [];

      this.start = 0;
      this.stop = this.values.length - 1;

      this.getDateArray();
      this.addXandYAxis1();
      this.addObject1();


      /*
      this.drawLineAndPath1();
      this.drawLineAndPath2();
      this.drawLineAndPath3();
      this.drawLineAndPath4();
      */

      this.addXandYAxis2();
      this.addObject2();
      // this.addBarChart();


      this.addObject3();
      this.addArc();

    }, error => console.error(error));
  }





  getDateArray() {            // Вывод массива тестовых данных
    this.data1.push({ dt: new Date(this.values[this.start].dt), value: 0 });
    this.data2.push({ dt: new Date(this.values[this.start].dt), value: 0 });
    this.data3.push({ dt: new Date(this.values[this.start].dt), value: 0 });
    this.data4.push({ dt: new Date(this.values[this.start].dt), value: 0 });
    for (let i = this.start; i <= this.stop; i++) {
      this.data1.push({ dt: new Date(this.values[i].dt), value: this.values[i].value1 });
      this.data2.push({ dt: new Date(this.values[i].dt), value: this.values[i].value2 });
      this.data3.push({ dt: new Date(this.values[i].dt), value: this.values[i].value3 });
      this.data4.push({ dt: new Date(this.values[i].dt), value: this.values[i].value4 });
    }
    this.data1.push({ dt: new Date(this.values[this.stop].dt), value: 0 });
    this.data2.push({ dt: new Date(this.values[this.stop].dt), value: 0 });
    this.data3.push({ dt: new Date(this.values[this.stop].dt), value: 0 });
    this.data4.push({ dt: new Date(this.values[this.stop].dt), value: 0 });

    this.data1.push({ dt: new Date(this.values[this.stop].dt), value: this.yLimit });
    this.data2.push({ dt: new Date(this.values[this.stop].dt), value: this.yLimit });
    this.data3.push({ dt: new Date(this.values[this.stop].dt), value: this.yLimit });
    this.data4.push({ dt: new Date(this.values[this.stop].dt), value: this.yLimit });
    this.dataLength = this.data1.length;
  }

  getValues() {
    return this.values;
  }

  private buildSvg1() {
    this.svg1 = d3.select('.group1')
      .append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

  private addXandYAxis1() {
    // range of data configuring
    this.x = d3Scale.scaleTime().range([0, this.width]);
    this.y = d3Scale.scaleLinear().range([this.height, 0]);
    this.x.domain(d3Array.extent(this.data1, (d) => d.dt));
    this.y.domain(d3Array.extent(this.data1, (d) => d.value));

    // Configure the X Axis
    this.svg1.append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .attr('style', 'stroke-opacity: 0.1')
      .call( d3Axis.axisBottom(this.x)
        .tickSize(-this.height)
        .ticks(10));

    // Configure the Y Axis
    this.svg1.append('g')
      .attr('style', 'stroke-opacity: 0.2')
      .call(d3Axis.axisLeft(this.y)
        .tickSize(-this.width)
        .ticks(5))
  }

  private drawLineAndPath1() {        // первый тестовый график (не задействован)
    this.data1[this.stop -this.start + 3].value = 0;
    this.line = d3Shape.line()
      .x((d: any) => this.x(d.dt))
      .y((d: any) => this.y(d.value));
    // Configuring line path
    this.svg1.append('path')
      .datum(this.data1)
      .attr('class', 'line')
      .attr('stroke', '#feb41b')
      .attr('fill', 'rgba(255, 200, 0, 0.1)')
      .attr('stroke-width', '2')
      .attr('d', this.line);
  }

  private drawLineAndPath2() {      // второй тестовый график (не задействован)
    this.data2[this.stop - this.start + 3].value = 0;
    this.line = d3Shape.line()
      .x((d: any) => this.x(d.dt))
      .y((d: any) => this.y(d.value));
    // Configuring line path
    this.svg1.append('path')
      .datum(this.data2)
      .attr('class', 'line')
      .attr('stroke', '#feb41b')
      .attr('fill', 'rgba(255, 150, 0, 0.1)')
      .attr('stroke-width', '2')
      .attr('d', this.line);
  }
                                             
  private drawLineAndPath3() {      // третий тестовый график (не задействован)
    this.data3[this.stop - this.start + 3].value = 0;
    this.line = d3Shape.line()
      .x((d: any) => this.x(d.dt))
      .y((d: any) => this.y(d.value));
    // Configuring line path
    this.svg1.append('path')
      .datum(this.data3)
      .attr('class', 'line')
      .attr('stroke', 'red')
      .attr('fill', 'rgba(255, 50, 0, 0.1)')
      .attr('stroke-width', '2')
      .attr('d', this.line);
  }

  private drawLineAndPath4() {      // четвертый тестовый (не задействован)
    this.data4[this.stop - this.start + 3].value = 0;
    this.line = d3Shape.line()
      .x((d: any) => this.x(d.dt))
      .y((d: any) => this.y(d.value));
    // Configuring line path
    this.svg1.append('path')
      .datum(this.data4)
      .attr('class', 'line')
      .attr('stroke', 'red')
      .attr('fill', 'rgba(255, 0, 0, 0.1)')
      .attr('stroke-width', '2')
      .attr('d', this.line);
  }

  private addObject1() {            
    this.svg1.append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', -50)
      .attr('x', 0)
      .attr('text-anchor', 'end')
      .text('Величина');

    this.svg1.append('text')
      .attr('y', 440)
      .attr('x', 0)
      .attr('text-anchor', 'start')
      .text('Хронологическая последовательность');
  }




  private buildSvg2() {
    this.svg2 = d3.select('.group2')
      .append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')')
      .attr('overflow', 'auto');
  }

  private addXandYAxis2() {
    // range of data configuring
    this.x = d3Scale.scaleTime().range([0, this.width]);
    this.y = d3Scale.scaleLinear().range([this.height, 0]);
    this.x.domain(d3Array.extent(this.data1, (d) => d.dt));
    this.y.domain(d3Array.extent(this.data1, (d) => d.value));

    // Configure the X Axis
    this.svg2.append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .call(d3Axis.axisBottom(this.x)
        .ticks(0));

    // Configure the Y Axis
    this.svg2.append('g')
      .attr('style', 'stroke-opacity: 0.3')
      .call(d3Axis.axisLeft(this.y)
        .tickSize(-this.width)
        .ticks(5));
  }

  private addObject2() {
    this.svg2.append('text')
      .attr('y', 440)
      .attr('x', 0)
      .attr('text-anchor', 'start')
      .text('Интеграционная диаграмма');

    this.svg2.append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', -45)
      .attr('x', 0)
      .attr('text-anchor', 'end')
      .text('Средняя статистика');
  }

  private addBarChart() {
    this.minValue = this.yLimit;

    // показатели первого графика
    for (let i = 0; i <= this.stop - this.start; i++) {
      if (this.data1[i].value > this.maxValue) {
        this.maxValue = this.data1[i].value;
      }
      if (this.data1[i].value < this.minValue && this.data1[i].value != 0) {
        this.minValue = this.data1[i].value;
      }
    }
    this.mediumValue = (this.maxValue + this.minValue) / 2;
    this.svg2.append('text')               // наименование первого процесса
      .attr('text-anchor', 'medium')
      .attr('transform', 'rotate(-90)')
      .attr('y', this.width / 25 - 20 )
      .attr('x', -this.height/3*2)
      .text('Первый процесс');
    this.svg2.append('rect')           // столбец максимума первого процесса
      .attr("width", this.width / 9)
      .attr("height", this.height * (this.maxValue / this.yLimit))
      .attr("x", this.width / 25 -10)
      .attr("y", this.height - this.height * (this.maxValue / this.yLimit))
      .attr('stroke', '#feb41b')
      .attr('fill', '#FFFFE0')
      .attr('stroke-width', '2');
    this.svg2.append("line")              // линия максимума первого процесса
      .attr('stroke', '#feb41b')
      .attr('stroke-width', '2')
      .attr("x1", this.width / 25 -10)
      .attr("y1", this.height - this.height * (this.maxValue / this.yLimit))
      .attr("x2", this.width / 25 * 4.0)
      .attr("y2",this.height - this.height * (this.maxValue / this.yLimit)); 
    this.svg2.append('text')                    // текст максимума первого процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.maxValue / this.yLimit))
      .attr('x', this.width / 25 * 4.0)
      .text(this.maxValue);
    this.svg2.append('rect')                 // столбец медиума первого процесса
      .attr("width", this.width / 9)
      .attr("height", this.height * (this.mediumValue / this.yLimit))
      .attr("x", this.width / 25-5)
      .attr("y", this.height - this.height * (this.mediumValue / this.yLimit))
      .attr('stroke', '#feb41b')
      .attr('fill', '#FFFACD')
      .attr('stroke-width', '2');
    this.svg2.append("line")                 // линия медиума первого процесса
      .attr('stroke', '#feb41b')
      .attr('stroke-width', '2')
      .attr("x1", this.width / 25-5)
      .attr("y1", this.height - this.height * (this.mediumValue / this.yLimit))
      .attr("x2", this.width / 25 * 4.2)
      .attr("y2", this.height - this.height * (this.mediumValue / this.yLimit)); 
    this.svg2.append('text')                   // текст медиума первого процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.mediumValue / this.yLimit))
      .attr('x', this.width / 25 * 4.2)
      .text(this.mediumValue);
    this.svg2.append('rect')                  // столбец минимума первого процесса
      .attr("width", this.width / 9)
      .attr("height", this.height * (this.minValue / this.yLimit))
      .attr("x", this.width / 25)
      .attr("y", this.height - this.height * (this.minValue / this.yLimit))
      .attr('stroke', '#feb41b')
      .attr('fill', '#FFEFD5')
      .attr('stroke-width', '2');
    this.svg2.append("line")               // линия минимума первого процесса
      .attr('stroke', '#feb41b')
      .attr('stroke-width', '2')
      .attr("x1", this.width / 25)
      .attr("y1", this.height - this.height * (this.minValue / this.yLimit))
      .attr("x2", this.width / 25 * 4.4)
      .attr("y2", this.height - this.height * (this.minValue / this.yLimit));        
    this.svg2.append('text')                     // текст минимума первого процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.minValue / this.yLimit))
      .attr('x', this.width / 25 * 4.4)
      .text(this.minValue);


    // показатели второго графика
    this.maxValue = 0;
    for (let i = 0; i <= this.stop - this.start; i++) {
      if (this.data2[i].value > this.maxValue) {
        this.maxValue = this.data2[i].value;
      }
      if (this.data2[i].value < this.minValue && this.data2[i].value != 0) {
        this.minValue = this.data2[i].value;
      }
    }
    this.mediumValue = (this.maxValue + this.minValue) / 2;
    this.svg2.append('text')           // наименование второго процесса
      .attr('text-anchor', 'medium')
      .attr('transform', 'rotate(-90)')
      .attr('y', this.width / 25 * 7 - 20 )
      .attr('x', -this.height / 3 * 2)
      .text('Второй процесс');
    this.svg2.append('rect')                 // столбец максимума второго процесса
      .attr("width", this.width / 9)
      .attr("height", this.height * (this.maxValue / this.yLimit))
      .attr("x", this.width / 25*7-10)
      .attr("y", this.height - this.height * (this.maxValue / this.yLimit))
      .attr('stroke', '#feb41b')
      .attr('fill', '#FFEBCD')
      .attr('stroke-width', '2');
    this.svg2.append("line")                   // линия максимума второго процесса
      .attr('stroke', '#feb41b')
      .attr('stroke-width', '2')
      .attr("x1", this.width / 25*7-10)
      .attr("y1", this.height - this.height * (this.maxValue / this.yLimit))
      .attr("x2", this.width / 25*10)
      .attr("y2", this.height - this.height * (this.maxValue / this.yLimit)); 
    this.svg2.append('text')                         // текст максимума второго процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.maxValue / this.yLimit))
      .attr('x', this.width / 25*10)
      .text(this.maxValue);
    this.svg2.append('rect')                         // столбец медиума второго процесса
      .attr("width", this.width / 9)
      .attr("height", this.height * (this.mediumValue / this.yLimit))
      .attr("x", this.width / 25*7 - 5)
      .attr("y", this.height - this.height * (this.mediumValue / this.yLimit))
      .attr('stroke', '#feb41b')
      .attr('fill', '#FFE4C4')
      .attr('stroke-width', '2');
    this.svg2.append("line")                               // линия медиума второго процесса
      .attr('stroke', '#feb41b')
      .attr('stroke-width', '2')
      .attr("x1", this.width / 25*7 - 5)
      .attr("y1", this.height - this.height * (this.mediumValue / this.yLimit))
      .attr("x2", this.width / 25*10.2 )
      .attr("y2", this.height - this.height * (this.mediumValue / this.yLimit)); 
    this.svg2.append('text')                             // текст медиума второго процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.mediumValue / this.yLimit))
      .attr('x', this.width / 25*10.2)
      .text(this.mediumValue);
    this.svg2.append('rect')                         // столбец минимума второго процесса
      .attr("width", this.width / 9)
      .attr("height", this.height * (this.minValue / this.yLimit))
      .attr("x", this.width / 25*7)
      .attr("y", this.height - this.height * (this.minValue / this.yLimit))
      .attr('stroke', '#feb41b')
      .attr('fill', '#FFDEAD')
      .attr('stroke-width', '2');
    this.svg2.append("line")                   // линия минимума второго процесса
      .attr('stroke', '#feb41b')
      .attr('stroke-width', '2')
      .attr("x1", this.width / 25*7)
      .attr("y1", this.height - this.height * (this.minValue / this.yLimit))
      .attr("x2", this.width / 25*10.4)
      .attr("y2", this.height - this.height * (this.minValue / this.yLimit)); 
    this.svg2.append('text')                           // текст минимума второго процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.minValue / this.yLimit))
      .attr('x', this.width / 25*10.4)
      .text(this.minValue);

    
    // показатели третьего графика
    this.maxValue = 0;
    for (let i = 0; i <= this.stop - this.start; i++) {
      if (this.data3[i].value > this.maxValue) {
        this.maxValue = this.data3[i].value;
      }
      if (this.data3[i].value < this.minValue && this.data3[i].value != 0) {
        this.minValue = this.data3[i].value;
      }
    }
    this.mediumValue = (this.maxValue + this.minValue) / 2;
    this.svg2.append('text')                         // наименование третьего процесса
      .attr('text-anchor', 'medium')
      .attr('transform', 'rotate(-90)')
      .attr('y', this.width / 25 * 13 -20)
      .attr('x', - this.height / 3*2)
      .text('Третий процесс');
    this.svg2.append('rect')                         // столбец максимума третьего процесса
      .attr("width", this.width / 9)
      .attr("height", this.height * (this.maxValue / this.yLimit))
      .attr("x", this.width / 25 * 13 -10)
      .attr("y", this.height - this.height * (this.maxValue / this.yLimit))
      .attr('stroke', '#eb9f60')
      .attr('fill', '#ffdbab')
      .attr('stroke-width', '2');
    this.svg2.append("line")                             // линия максимума третьего процесса
      .attr('stroke', '#eb9f60')
      .attr('stroke-width', '2')
      .attr("x1", this.width / 25 * 13 - 10)
      .attr("y1", this.height - this.height * (this.maxValue / this.yLimit))
      .attr("x2", this.width / 25 * 16)
      .attr("y2", this.height - this.height * (this.maxValue / this.yLimit));
    this.svg2.append('text')                             // текст максимума третьего процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.maxValue / this.yLimit))
      .attr('x', this.width / 25 * 16)
      .text(this.maxValue);
    this.svg2.append('rect')                         // столбец медиума третьего процесса
      .attr("width", this.width / 9)
      .attr("height", this.height * (this.mediumValue / this.yLimit))
      .attr("x", this.width / 25 * 13 - 5)
      .attr("y", this.height - this.height * (this.mediumValue / this.yLimit))
      .attr('stroke', '#eb9f60')
      .attr('fill', '#ffcb8f')
      .attr('stroke-width', '2');
    this.svg2.append("line")                         // линия медиума третьего процесса
      .attr('stroke', '#eb9f60')
      .attr('stroke-width', '2')
      .attr("x1", this.width / 25 * 13 -5)
      .attr("y1", this.height - this.height * (this.mediumValue / this.yLimit))
      .attr("x2", this.width / 25 * 16.15)
      .attr("y2", this.height - this.height * (this.mediumValue / this.yLimit));
    this.svg2.append('text')                        // текст медиума третьего процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.mediumValue / this.yLimit))
      .attr('x', this.width / 25 * 16.15)
      .text(this.mediumValue);
    this.svg2.append('rect')                 // столбец минимума третьего процесса 
      .attr("width", this.width / 9)
      .attr("height", this.height * (this.minValue / this.yLimit))
      .attr("x", this.width / 25 * 13)
      .attr("y", this.height - this.height * (this.minValue / this.yLimit))
      .attr('stroke', '#eb9f60')
      .attr('fill', '#ffbf80')
      .attr('stroke-width', '2');
    this.svg2.append("line")                       // линия минимума третьего процесса
      .attr('stroke', '#eb9f60')
      .attr('stroke-width', '2')
      .attr("x1", this.width / 25 * 13)
      .attr("y1", this.height - this.height * (this.minValue / this.yLimit))
      .attr("x2", this.width / 25 * 16.4)
      .attr("y2", this.height - this.height * (this.minValue / this.yLimit));
    this.svg2.append('text')                     // текст минимума третьего процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.minValue / this.yLimit))
      .attr('x', this.width / 25 * 16.4)
      .text(this.minValue);



    // показатели четвертого графика
    this.maxValue = 0;
    for (let i = 0; i <= this.stop - this.start; i++) {
      if (this.data4[i].value > this.maxValue) {
        this.maxValue = this.data4[i].value;
      }
      if (this.data4[i].value < this.minValue && this.data4[i].value != 0) {
        this.minValue = this.data4[i].value;
      }
    }
    this.mediumValue = (this.maxValue + this.minValue) / 2;
    this.svg2.append('text')                 // наименование четвертого процесса
      .attr('text-anchor', 'medium')
      .attr('transform', 'rotate(-90)')
      .attr('y', this.width / 25 * 19 - 20)
      .attr('x', - this.height / 3*2)
      .text('Четвертый процесс');
    this.svg2.append('rect')                 // столбец максимума четвертого процесса 
      .attr("width", this.width / 9)
      .attr("height", this.height * (this.maxValue / this.yLimit))
      .attr("x", this.width / 25 * 19-10)
      .attr("y", this.height - this.height * (this.maxValue / this.yLimit))
      .attr('stroke', '#c37054')
      .attr('fill', '#ffbf80')
      .attr('stroke-width', '2');
    this.svg2.append("line")                    // линия максимума четвертого процесса
      .attr('stroke', '#c37054')
      .attr('stroke-width', '2')
      .attr("x1", this.width / 25 * 19 - 10)
      .attr("y1", this.height - this.height * (this.maxValue / this.yLimit))
      .attr("x2", this.width / 25 * 22)
      .attr("y2", this.height - this.height * (this.maxValue / this.yLimit));
    this.svg2.append('text')                         // текст максимума четвертого процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.maxValue / this.yLimit))
      .attr('x', this.width / 25 * 22)
      .text(this.maxValue);
    this.svg2.append('rect')                             // столбец медиума четвертого процесса
      .attr("width", this.width / 9)
      .attr("height", this.height * (this.mediumValue / this.yLimit))
      .attr("x", this.width / 25 * 19 - 5)
      .attr("y", this.height - this.height * (this.mediumValue / this.yLimit))
      .attr('stroke', '#c37054')
      .attr('fill', '#f39f60')
      .attr('stroke-width', '2');
    this.svg2.append("line")                             // линия медиума четвертого процесса
      .attr('stroke', '#c37054')
      .attr('stroke-width', '2')
      .attr("x1", this.width / 25 * 19 - 5)
      .attr("y1", this.height - this.height * (this.mediumValue / this.yLimit))
      .attr("x2", this.width / 25 * 22.2)
      .attr("y2", this.height - this.height * (this.mediumValue / this.yLimit));
    this.svg2.append('text')                       // текст медиума четвертого процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.mediumValue / this.yLimit))
      .attr('x', this.width / 25 * 22.2)
      .text(this.mediumValue);
    this.svg2.append('rect')                         // столбец минимума четвертого процесса 
      .attr("width", this.width / 9)
      .attr("height", this.height * (this.minValue / this.yLimit))
      .attr("x", this.width / 25 * 19)
      .attr("y", this.height - this.height * (this.minValue / this.yLimit))
      .attr('stroke', '#c37054')
      .attr('fill', '#df8b68')
      .attr('stroke-width', '2');
    this.svg2.append("line")                   // линия минимума четвертого процесса
      .attr('stroke', '#c37054')
      .attr('stroke-width', '2')
      .attr("x1", this.width / 25 * 19)
      .attr("y1", this.height - this.height * (this.minValue / this.yLimit))
      .attr("x2", this.width / 25 * 22.4)
      .attr("y2", this.height - this.height * (this.minValue / this.yLimit));
    this.svg2.append('text')                         // текст минимума четвертого процесса
      .attr('style', 'font-size: 10px')
      .attr('text-anchor', 'start')
      .attr('y', this.height - this.height * (this.minValue / this.yLimit))
      .attr('x', this.width / 25 * 22.4)
      .text(this.minValue);
  }










  private buildSvg3() {
    this.svg3 = d3.select('.group3')
      .append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

  private addObject3() {
    this.svg3.append('text')
      .attr('y', 440)
      .attr('x', 1100)
      .attr('text-anchor', 'end')
      .text('Интеграционная диаграмма');

    this.svg3.append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', -30)
      .attr('x', -60)
      .attr('text-anchor', 'end')
      .text('Средняя статистика');
  }




  

  private addArc() {
    this.arc = this.arc()
      .outerRadius(100)
      .innerRadius(0);

    this.svg3.append('path')
      .attr('d', this.arc)
      .attr('y', 100)
      .attr('x', 100)
      .style("fill", '#eee');
  }

}


interface Value {
  id: number;
  dt: string;
  value1: number;
  value2: number;
  value3: number;
  value4: number;
}



interface ArrayParameters {
  dt: any;
  val: number;
  shortDesc: string;
  attributes: any;
  tagName: string;
  description: string;
  length: number;
}
