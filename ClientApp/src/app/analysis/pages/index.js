"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./predictive-analytics/predictive-analytics.component"));
__export(require("./analytics/analytics.component"));
//# sourceMappingURL=index.js.map