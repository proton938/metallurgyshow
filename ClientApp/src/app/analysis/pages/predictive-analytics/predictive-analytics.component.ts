import { Component, OnInit, Optional, Inject, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TreeParams } from "../../../core/models/TreeParams";

import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as d3Path from 'd3-path';

@Component({
  templateUrl: './predictive-analytics.component.html',
  styleUrls: ['./predictive-analytics.component.css']
})
export class PredictiveAnalyticsComponent implements OnInit {


  public xx: any =
    {
      "2020-01-31T05:54:00": [5.2899999618530273, 7.5100002288818359, 0.0, 0.0, 0.0, 0.0],
      "2020-01-31T05:55:40": [5.1999998092651367, 7.4600000381469727, 0.0, 0.0, 0.0, 0.0],
      "2020-01-31T05:57:20": [5.619999885559082, 7.5199999809265137, 0.0, 0.0, 0.0, 0.0]
    };

  public treeParams: TreeParams;

  private permissionSelectedParameters: number = 0; // разрешение еа селекцию параметров в дереве


  public start: number;
  public stop: number;

  public startDateTime: any;
  public stopDateTime: any;
  public currentDateTime: any;   // текущее время
  public currentMonth: any;
  public currentDate: any;
  public currentHour: any;
  public lastHour: any;
  public timeBufer: any;

  public startDate: any;
  public stopDate: any;
  public startHour: any;
  public stopHour: any;

  public timeScaleRight: number = 0;
  public timeScaleLeft: number = 0;


  public values: Value[];
  public arrayParameters: ArrayParameters[];
  public getHistoryValues: ArrayParameters[];
  public getPredictValues: any = [];
  public res: string;

  title = 'Прогнозирование';

  public dataLength: number;     // ширина диапазона

  data: any[] = [];
  data1: any[] = [];     // массивы для сбора данных 
  data2: any[] = [];
  data3: any[] = [];
  data4: any[] = [];

  public pathUrl: string;
  public treeParametersUrl: string;
  public getHistoryValuesUrl: string;
  public getPredictValuesUrl: string;

  public yLimit: number = 1300;

  public yMaxLimit: number = 0;
  public yMinLimit: number = 1000000;
  private leftOffsetForBarChart: number = 0;        // переменная смещения прямоугольников интеграционной диаграмы 

  public maxValue: number = 0;
  public mediumValue: number = 0;
  public minValue: number;
  public bufer: any;
  public getPageNumber: string;
  public counter: number;
  private arc: any;

  private margin = { top: 20, right: 0, bottom: 40, left: 80 };
  private width: number;
  private height: number;
  private x: any;
  private y: any;
  private svg1: any;
  private svg2: any;
  private svg3: any;
  private line: d3Shape.Line<[number, number]>; // this is line defination

  private idChart: any;

  private baseUrl: string;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, public tree: TreeParams) {

    this.baseUrl = baseUrl;

    this.treeParams = tree;



    this.pathUrl = baseUrl + 'api/PrmValues';
    this.treeParametersUrl = baseUrl + 'api/parameters/getParametersByArea';
    this.getHistoryValuesUrl = baseUrl + 'api/PrmValues/GetHistoryValues';
    this.getPredictValuesUrl = baseUrl + 'api/parameters/GetPredictValues';


    http.get<Value[]>(this.pathUrl).subscribe(result => {
      this.values = result;
    }, error => console.error(error));

    this.http.get<ArrayParameters[]>(this.getHistoryValuesUrl + '/' + this.selected_prms + '/' + this.startDateTime + '/' + this.stopDateTime).subscribe(result => {
      this.getHistoryValues = result;
    }, error => console.error(error));


    this.http.get<ArrayParameters[]>(this.getPredictValuesUrl + '/' + this.selected_prms + '/' + this.startDateTime + '/' + this.stopDateTime + '/30').subscribe(result => {
      this.getPredictValues = result;
    }, error => console.error(error));


    this.currentDateTime = new Date();        // Выводим текущую дату

    this.currentMonth = '' + Number(this.currentDateTime.getMonth() + 1);   // двузначное значение месяца
    if (this.currentMonth.length < 2) {
      this.currentMonth = '0' + this.currentMonth;
    }

    this.currentDate = '' + Number(this.currentDateTime.getDate());       // двузначное значение даты
    if (this.currentDate.length < 2) {
      this.currentDate = '0' + this.currentDate;
    }

    this.currentHour = this.currentDateTime.getHours() + ':00';
    if (this.currentHour.length < 5) {
      this.currentHour = '0' + this.currentHour;
    }

    this.lastHour = this.currentDateTime.getHours() - 1 + ':00';
    if (this.lastHour.length < 5) {
      this.lastHour = '0' + this.lastHour;
    }



    // configure margins and width/height of the graph
    this.width = 1200 - this.margin.left - this.margin.right;
    this.height = 460 - this.margin.top - this.margin.bottom;
  }







  ngOnInit() {

    if (this.treeParams.getTreeParametersUrl == '') {

      var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

      // при загрузке страницы выводим последний час

      var currentDateTime: any = new Date();        // Выводим будущее время на час вперед

      var futureDateTime: any = new Date(currentDateTime - eraUnix + 3600000);

      (<HTMLInputElement>document.getElementById("dateStop")).value = futureDateTime.getFullYear() + '-' + ('0' + (futureDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + futureDateTime.getDate()).slice(-2);
      (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + futureDateTime.getHours()).slice(-2) + ':00';
      (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + futureDateTime.getMinutes()).slice(-2);

      var lastDateTime: any = new Date(currentDateTime - eraUnix - 3600000);

      (<HTMLInputElement>document.getElementById("dateStart")).value = lastDateTime.getFullYear() + '-' + ('0' + (lastDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + lastDateTime.getDate()).slice(-2);
      (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + lastDateTime.getHours()).slice(-2) + ':00';
      (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + lastDateTime.getMinutes()).slice(-2);

    } else {

      var currentDateTime: any = new Date(this.treeParams.startDateTime);        // Выводим выгружаемую дату

      (<HTMLInputElement>document.getElementById("dateStart")).value = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);
      (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + currentDateTime.getHours()).slice(-2) + ':00';
      (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + currentDateTime.getMinutes()).slice(-2);

      var lastDateTime: any = new Date(this.treeParams.stopDateTime);

      (<HTMLInputElement>document.getElementById("dateStop")).value = lastDateTime.getFullYear() + '-' + ('0' + (lastDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + lastDateTime.getDate()).slice(-2);
      (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + lastDateTime.getHours()).slice(-2) + ':00';
      (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + lastDateTime.getMinutes()).slice(-2);
    }






    this.startDateTime = (<HTMLInputElement>document.getElementById("dateStart")).value + ' ' + (<HTMLInputElement>document.getElementById("timeStart")).value;
    this.stopDateTime = (<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + (<HTMLInputElement>document.getElementById("timeStop")).value;

    this.buildSvg1();


    // this.fullRequest();

    this.getClient();
  }






  updateModel() {
    this.treeParams.getTreeParametersUrl = '';      // обновляем url строку get-запроса в модели
  }



  unHideToolbarMenu() {       // скрытие меню инструментов
    var hiddenToolBar = (<HTMLInputElement>document.getElementById("unHiddenToolbar")).className;
    if (hiddenToolBar == 'unhidden_toolbar') {
      (<HTMLInputElement>document.getElementById("unHiddenToolbar")).className = 'hidden_toolbar';
      (<HTMLInputElement>document.getElementById('toolbarTitle')).style.display = 'none';
    } else {
      (<HTMLInputElement>document.getElementById("unHiddenToolbar")).className = 'unhidden_toolbar';
      (<HTMLInputElement>document.getElementById('toolbarTitle')).style.display = 'inline-block';
    }
  }



  testGraphic() {        // вывод координатной сетки при старте страницы

    this.startDateTime = (<HTMLInputElement>document.getElementById("dateStart")).value + ' ' + (<HTMLInputElement>document.getElementById("timeStart")).value;
    this.stopDateTime = (<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + (<HTMLInputElement>document.getElementById("timeStop")).value;

    this.data1 = [];
    this.data2 = [];
    this.data3 = [];
    this.data4 = [];

    this.fullRequest();
  }



  private timePoint1: any = '';
  private timePoint2: any = '';
  private xSelectRect: any = '';

  private zoomMemory: any = [];

  // функции масштабирования графика выделением участка
  sensorScale() {

    if (this.timePoint1 == '') {

      this.timePoint1 = new Date((<HTMLInputElement>document.getElementById("selectTime")).value);

      var lineX: any = (<HTMLInputElement>document.getElementById("lineX")).value;
      (<HTMLInputElement>document.getElementById("selectRect")).setAttribute("x", lineX);
      this.xSelectRect = lineX;
    } else {



      this.updateModel();



      this.timePoint2 = new Date((<HTMLInputElement>document.getElementById("selectTime")).value);

      var timeDifference = this.timePoint2 - this.timePoint1;

      if (Math.abs(timeDifference) > 300000) {

        // запоминаем начало и завершение текущего временного интервала для возврата

        var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
        startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут
        var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
        stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут
        this.zoomMemory.unshift({ dateTimeStart: (<HTMLInputElement>document.getElementById("dateStart")).value + ' ' + startTime, dateTimeStop: (<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime });
        (<HTMLInputElement>document.getElementById("returnButton")).style.display = 'block';





        if (Number(timeDifference) > 0) {
          (<HTMLInputElement>document.getElementById("dateStart")).value = this.timePoint1.getFullYear() + '-' + ('0' + (this.timePoint1.getMonth() + 1)).slice(-2) + '-' + ('0' + this.timePoint1.getDate()).slice(-2);
          (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + this.timePoint1.getHours()).slice(-2) + ':00';
          (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + this.timePoint1.getMinutes()).slice(-2);
          (<HTMLInputElement>document.getElementById("dateStop")).value = this.timePoint2.getFullYear() + '-' + ('0' + (this.timePoint2.getMonth() + 1)).slice(-2) + '-' + ('0' + this.timePoint2.getDate()).slice(-2);
          (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + this.timePoint2.getHours()).slice(-2) + ':00';
          (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + this.timePoint2.getMinutes()).slice(-2);
        } else {
          (<HTMLInputElement>document.getElementById("dateStart")).value = this.timePoint2.getFullYear() + '-' + ('0' + (this.timePoint2.getMonth() + 1)).slice(-2) + '-' + ('0' + this.timePoint2.getDate()).slice(-2);
          (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + this.timePoint2.getHours()).slice(-2) + ':00';
          (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + this.timePoint2.getMinutes()).slice(-2);
          (<HTMLInputElement>document.getElementById("dateStop")).value = this.timePoint1.getFullYear() + '-' + ('0' + (this.timePoint1.getMonth() + 1)).slice(-2) + '-' + ('0' + this.timePoint1.getDate()).slice(-2);
          (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + this.timePoint1.getHours()).slice(-2) + ':00';
          (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + this.timePoint1.getMinutes()).slice(-2);
        }

        (<HTMLInputElement>document.getElementById("showChartDescription")).style.left = 0 + 'px';
        (<HTMLInputElement>document.getElementById("showChartDescription")).style.top = 1200 + 'px';
        (<HTMLInputElement>document.getElementById("showChartDescription")).style.display = 'none';

        this.getClient();
      }

      this.timePoint1 = '';
      this.timePoint2 = '';

      (<HTMLInputElement>document.getElementById("selectRect")).setAttribute("width", '0');
      (<HTMLInputElement>document.getElementById("selectRect")).setAttribute("x", '0');
    }
  }





  returnZoom() {

    this.updateModel();

    let start = new Date(this.zoomMemory[0].dateTimeStart);
    let stop = new Date(this.zoomMemory[0].dateTimeStop);

    this.zoomMemory.shift();
    if (this.zoomMemory.length < 1) {
      (<HTMLInputElement>document.getElementById("returnButton")).style.display = 'none';
    }

    (<HTMLInputElement>document.getElementById("dateStart")).value = start.getFullYear() + '-' + ('0' + (start.getMonth() + 1)).slice(-2) + '-' + ('0' + start.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + start.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + start.getMinutes()).slice(-2);

    (<HTMLInputElement>document.getElementById("dateStop")).value = stop.getFullYear() + '-' + ('0' + (stop.getMonth() + 1)).slice(-2) + '-' + ('0' + stop.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + stop.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + stop.getMinutes()).slice(-2);

    this.getClient();
  }





  public identif: string = '';
  public styleDisplay: any;
  private permission: number = 0;

  treeParameters() {
    var currentUrl: any = document.location.href;
    currentUrl = currentUrl.split('/');
    if (currentUrl.length < 5) {
      currentUrl = 'Main';
    } else {
      currentUrl = currentUrl[4];
    }

    var testLoadMenu = (<HTMLInputElement>document.getElementById(currentUrl));

    if (testLoadMenu) {
      currentUrl = (<HTMLInputElement>document.getElementById(currentUrl)).value;
      this.treeParametersUrl = this.treeParametersUrl + '/' + currentUrl;

      this.styleDisplay = (<HTMLInputElement>document.getElementById('one_1')).style.display;
      if (this.styleDisplay == 'none') {
        (<HTMLInputElement>document.getElementById('one_1')).style.display = 'table';
        (<HTMLInputElement>document.getElementById('rolloverDown')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('rolloverUp')).style.display = 'block';
      } else {
        (<HTMLInputElement>document.getElementById('one_1')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('rolloverDown')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('rolloverUp')).style.display = 'none';
      }

      if (this.permission == 0) {
        this.http.get<ArrayParameters[]>(this.treeParametersUrl).subscribe(result => {
          this.arrayParameters = result;
          for (let i = 0; i < this.arrayParameters.length; i++) {      // если имя параметра пустое - заменяем его на системный тег 
            if (this.arrayParameters[i].shortDesc == '') {
              this.arrayParameters[i].shortDesc = this.arrayParameters[i].tagName;
            }
          }

          this.treeParams.getTreeParameters = this.arrayParameters;   // передаем массив дерева в модель

        }, error => console.error(error));

        this.permission = 1;                   // разрешение на повторную выгрузку массива закрываем
      }


      // возвращаем checked ранее выделенных параметров
      if (this.permissionSelectedParameters == 0) {
        if (this.treeParams.getSelectedParameters.length != 0) {
          for (let i = 0; i < this.treeParams.getSelectedParameters.length; i++) {
            (<HTMLInputElement>document.getElementById(this.treeParams.getSelectedParameters[i])).checked = true;
          }
        }
        this.permissionSelectedParameters = 1;
      }



    } else {
      alert('Меню навигации еще не догрузилось!');
    }
  }




  unHideTree() {
    this.styleDisplay = (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display;
    if (this.styleDisplay == 'none') {
      (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display = 'table';
      (<HTMLInputElement>document.getElementById('arrowOne_' + this.identif)).innerHTML = 'v';
    } else {
      (<HTMLInputElement>document.getElementById('two_' + this.identif)).style.display = 'none';
      (<HTMLInputElement>document.getElementById('arrowOne_' + this.identif)).innerHTML = '>';
    }
  }




  allAttributes(counterObject) {  // выделение всех параметров объекта
    if ((<HTMLInputElement>document.getElementById('callObject_' + (counterObject + 1))).checked) {
      for (let i = 0; i < this.arrayParameters[counterObject].attributes.length; i++) {
        (<HTMLInputElement>document.getElementById('callParameter_' + this.arrayParameters[counterObject].tagName + '_' + (i + 1))).checked = true;
      }
    } else {
      for (let i = 0; i < this.arrayParameters[counterObject].attributes.length; i++) {
        (<HTMLInputElement>document.getElementById('callParameter_' + this.arrayParameters[counterObject].tagName + '_' + (i + 1))).checked = false;
      }
    }
  }




  public parsePredictValues: any = [];

  public arrayPredictValues: any = [];

  public buferPredictValues: any = [];



  predictRequest() {

    this.arrayPredictValues = [];

    /*
    var x = 
      {
        "2020-01-31T05:54:00": [5.2899999618530273, 7.5100002288818359, 0.0, 0.0, 0.0, 0.0],
        "2020-01-31T05:55:40": [5.1999998092651367, 7.4600000381469727, 0.0, 0.0, 0.0, 0.0],
        "2020-01-31T05:57:20": [5.619999885559082, 7.5199999809265137, 0.0, 0.0, 0.0, 0.0]
    };

    
    var strName, strValue: string;

    for (strName in x) {
      strValue = x[strName];
      alert("name : " + strName + " : value : " + strValue);
    }
   */

  
    this.selected_prms = '';
    this.arraySelectedPrms = [];
    this.arrayForChart = [];
    var select_check: any;

    var documentWidth: number = document.body.clientWidth;
    // configure margins and width/height of the graph
    this.width = (documentWidth - documentWidth / 15 - 200) - this.margin.left - this.margin.right;
    this.height = 460 - this.margin.top - this.margin.bottom;

    this.svg1.selectAll("*").remove();


    // процедура выгрузки массива выбранных параметров
    for (let i = 0; i < this.arrayParameters.length; i++) {                    // выводим массив выбранных параметров 
      for (let j = 0; j < this.arrayParameters[i].attributes.length; j++) {
        select_check = (<HTMLInputElement>document.getElementById('callParameter_' + this.arrayParameters[i].tagName + '_' + (j + 1)));
        if (select_check.checked) {
          this.selected_prms = this.selected_prms + this.arrayParameters[i].tagName + '.' + this.arrayParameters[i].attributes[j].name + ',';  // формируем массив выбранных параметров
          this.arraySelectedPrms.push({ tagName_name: this.arrayParameters[i].tagName + '.' + this.arrayParameters[i].attributes[j].name, historyValues: [], name: this.arrayParameters[i].attributes[j].name, short_desc: this.arrayParameters[i].shortDesc, description: this.arrayParameters[i].attributes[j].description, engUnits: this.arrayParameters[i].attributes[j].engUnits });

          this.treeParams.getSelectedParameters.push('callParameter_' + this.arrayParameters[i].tagName + '_' + (j + 1));   // массив id отмеченных checkbox-ов в модели
        }
      }
    }

    this.selected_prms = this.selected_prms.slice(0, this.selected_prms.length - 1);   // убираем лишнюю зяпятую в конце массива выбранных параметров



    var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
    startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут

    var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
    stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут

    this.startDateTime = (<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + startTime;
    this.stopDateTime = (<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime;

    this.treeParams.startDateTime = this.startDateTime;    // отправляем точки времени в модель
    this.treeParams.stopDateTime = this.stopDateTime;







    this.http.get(this.getPredictValuesUrl + '/' + this.selected_prms + '/' + this.startDateTime + '/' + this.stopDateTime + '/30').subscribe(result => {



      this.getPredictValues = result;

      var strName: string;
      var strValue: any = [];


      for (let i = 0; i < this.arraySelectedPrms.length; i++) {
        for (strName in this.getPredictValues) {
          strValue = this.getPredictValues[strName];
          this.parsePredictValues.push({ dt: strName, val: strValue[i] });
        }

        var key = this.arraySelectedPrms[i].tagName_name;   // имя ключа
        var obj = {};                                     // ассоциативный массив
        obj[key] = this.parsePredictValues;
        this.arrayPredictValues.push(obj);

        // alert(obj[this.arraySelectedPrms[i].tagName_name][2].dt);

        this.parsePredictValues = [];
      }

      console.log(this.arrayPredictValues[1]);




      this.yMaxLimit = 0; // зануляем максимум
      this.yMinLimit = 1000000; // увеличиваем минимум
      this.leftOffsetForBarChart = 0; // зануляем смещение интеграционных диаграм

      for (let i = 0; i < this.arraySelectedPrms.length; i++) {

        this.arraySelectedPrms[i].historyValues.push({ val: 0 }); // определяем минимальное значение шкалы

        for (let j = 0; j < this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name].length; j++) {  // получен ассоциативный массив - по ключу обращаемся к дочернему нумерованному, для получения длины
          if (j == 0) {   // зануляем первое значение
            this.arrayForChart.push({ dt: new Date(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }

          if (this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].val != 'NaN') {
            this.arraySelectedPrms[i].historyValues.push({ dt: new Date(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].dt), val: Number(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].val) });
          } else {
            this.arraySelectedPrms[i].historyValues.push({ dt: new Date(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }

          if (this.yMaxLimit < Number(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].val)) { // Выводим максимум
            this.yMaxLimit = Number(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].val);
          }
          if (this.yMinLimit > Number(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].val)) { // Выводим минимум
            this.yMinLimit = Number(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].val);
          }


          if (j == this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name].length - 1) {   // зануляем последнее значение
            this.arrayForChart.push({ dt: new Date(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].dt), val: this.yMaxLimit });
          }
        }

      }

      this.activeAddXandYAxis1();
      this.selectRect();
      this.addObject1();

      for (let i = 0; i < this.arraySelectedPrms.length; i++) {

        this.maxValue = 0;        // приводим параметры интеграционных диаграм в исходное значение
        this.minValue = 1000000;

        this.arrayForChart = [];
        for (let j = 0; j < this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name].length; j++) {
          if (j == 0) {     // зануляем первое значение
            this.arrayForChart.push({ dt: new Date(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }

          if (this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].val != 'NaN') {
            this.arrayForChart.push({ dt: new Date(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].dt), val: Number(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].val) });

            if (this.maxValue < this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].val) { // максимум интеграционных диаграм
              this.maxValue = this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].val;
            }
            if (this.minValue > this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].val) { // минимум интеграционных диаграм
              this.minValue = this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].val;
            }
          } else {
            this.arrayForChart.push({ dt: new Date(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }

          if (j == this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name].length - 1) {  // зануляем последнее значение
            this.arrayForChart.push({ dt: new Date(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].dt), val: 0 }); // зануляем последнее значение для завершения графика
          }
        }
        this.idChart = this.arraySelectedPrms[i].short_desc + '_' + this.arraySelectedPrms[i].description + '_' + i + '_' + this.arraySelectedPrms[i].engUnits;

        this.activeLineAndPath1();   // вывод графиков
      }

      this.lineCursor(); // линии прицела курсора

      for (let i = 0; i < this.arraySelectedPrms.length; i++) {  // дополнительный цикл для невидимых линий интерактивной подсказки
        this.arrayForChart = [];
        for (let j = 0; j < this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name].length; j++) {

          if (this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].val != 'NaN') {
            this.arrayForChart.push({ dt: new Date(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].dt), val: Number(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].val) });
          } else {
            this.arrayForChart.push({ dt: new Date(this.arrayPredictValues[i][this.arraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }

        }
        this.idChart = this.arraySelectedPrms[i].short_desc + '_' + this.arraySelectedPrms[i].description + '_' + i + '_' + this.arraySelectedPrms[i].engUnits;
        this.descriptionLine();
      }










    }, error => console.error(error));
    
  }






  public selected_prms: string = '';
  public arraySelectedPrms: any[] = [];
  public historyValues: any[] = [];
  public arrayForChart: any[] = [];       // выводим массив значений времени для вертикальной шкалы

  getClient() {
    this.timePoint1 = '';
    this.timePoint2 = '';

    var documentWidth: number = document.body.clientWidth;
    // configure margins and width/height of the graph
    this.width = (documentWidth - documentWidth / 15 - 200) - this.margin.left - this.margin.right;
    this.height = 460 - this.margin.top - this.margin.bottom;

    this.svg1.selectAll("*").remove();


    if (this.treeParams.getTreeParametersUrl == '') {

      this.selected_prms = '';
      this.arraySelectedPrms = [];
      this.arrayForChart = [];
      var select_check: any;
      this.treeParams.getSelectedParameters = [];


      if (this.treeParams.getArraySelectedPrms.length == 0) {
        // процедура выгрузки массива выбранных параметров
        for (let i = 0; i < this.arrayParameters.length; i++) {                    // выводим массив выбранных параметров 
          for (let j = 0; j < this.arrayParameters[i].attributes.length; j++) {
            select_check = (<HTMLInputElement>document.getElementById('callParameter_' + this.arrayParameters[i].tagName + '_' + (j + 1)));
            if (select_check.checked) {
              this.selected_prms = this.selected_prms + this.arrayParameters[i].tagName + '.' + this.arrayParameters[i].attributes[j].name + ',';  // формируем массив выбранных параметров
              this.arraySelectedPrms.push({ tagName_name: this.arrayParameters[i].tagName + '.' + this.arrayParameters[i].attributes[j].name, historyValues: [], name: this.arrayParameters[i].attributes[j].name, short_desc: this.arrayParameters[i].shortDesc, description: this.arrayParameters[i].attributes[j].description, engUnits: this.arrayParameters[i].attributes[j].engUnits });

              this.treeParams.getSelectedParameters.push('callParameter_' + this.arrayParameters[i].tagName + '_' + (j + 1));   // массив id отмеченных checkbox-ов в модели
            }
          }
        }

        this.selected_prms = this.selected_prms.slice(0, this.selected_prms.length - 1);   // убираем лишнюю зяпятую в конце массива выбранных параметров
      } else {
        this.arraySelectedPrms = this.treeParams.getArraySelectedPrms;
        for (let i = 0; i < this.treeParams.getArraySelectedPrms.length; i++) {
          this.selected_prms = this.selected_prms + this.treeParams.getArraySelectedPrms[i].tagName_name + ',';
        }
        this.selected_prms = this.selected_prms.slice(0, this.selected_prms.length - 1);   // убираем лишнюю зяпятую в конце массива выбранных параметров

      }


      var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
      startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут

      var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
      stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут

      this.startDateTime = (<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + startTime;
      this.stopDateTime = (<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime;

      this.treeParams.startDateTime = this.startDateTime;    // отправляем точки времени в модель
      this.treeParams.stopDateTime = this.stopDateTime;


      /*
      // если время завершения - будущее - преврашаем его в текущее
      var currentDateTime: any = new Date();        // Выводим текущую дату
      var buferStopDateTume: any = new Date(this.stopDateTime);
      if (buferStopDateTume - currentDateTime > 0) {
        (<HTMLInputElement>document.getElementById("dateStop")).value = currentDateTime.getFullYear() + '-' + ('0' + (currentDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + currentDateTime.getDate()).slice(-2);
        (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + currentDateTime.getHours()).slice(-2) + ':00';
        (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + currentDateTime.getMinutes()).slice(-2);

        var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
        stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут
        this.stopDateTime = (<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime;
      }
      */

      this.getHistoryValuesUrl = this.baseUrl + 'api/PrmValues/GetHistoryValues' + '/' + this.selected_prms + '/' + this.startDateTime + '/' + this.stopDateTime;

      // передаем данные в модель
      this.treeParams.getTreeParametersUrl = this.getHistoryValuesUrl;
      this.treeParams.getArraySelectedPrms = this.arraySelectedPrms;

    } else {

      this.getHistoryValuesUrl = this.treeParams.getTreeParametersUrl;
      this.arraySelectedPrms = this.treeParams.getArraySelectedPrms;

    }



    this.http.get<ArrayParameters[]>(this.getHistoryValuesUrl).subscribe(result => {

      this.getHistoryValues = result;

      this.yMaxLimit = 0; // зануляем максимум
      this.yMinLimit = 1000000; // увеличиваем минимум
      this.leftOffsetForBarChart = 0; // зануляем смещение интеграционных диаграм

      for (let i = 0; i < this.arraySelectedPrms.length; i++) {

        this.arraySelectedPrms[i].historyValues.push({ val: 0 }); // определяем минимальное значение шкалы

        for (let j = 0; j < this.getHistoryValues[this.arraySelectedPrms[i].tagName_name].length; j++) {  // получен ассоциативный массив - по ключу обращаемся к дочернему нумерованному, для получения длины
          if (j == 0) {   // зануляем первое значение
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }

          if (this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val != 'NaN') {
            this.arraySelectedPrms[i].historyValues.push({ dt: new Date(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].dt), val: Number(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val) });
          } else {
            this.arraySelectedPrms[i].historyValues.push({ dt: new Date(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }

          if (this.yMaxLimit < Number(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val)) { // Выводим максимум
            this.yMaxLimit = Number(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val);
          }
          if (this.yMinLimit > Number(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val)) { // Выводим минимум
            this.yMinLimit = Number(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val);
          }


          if (j == this.getHistoryValues[this.arraySelectedPrms[i].tagName_name].length - 1) {   // зануляем последнее значение
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].dt), val: this.yMaxLimit });
          }
        }

      }

      this.activeAddXandYAxis1();
      this.selectRect();
      this.addObject1();

      for (let i = 0; i < this.arraySelectedPrms.length; i++) {

        this.maxValue = 0;        // приводим параметры интеграционных диаграм в исходное значение
        this.minValue = 1000000;

        this.arrayForChart = [];
        for (let j = 0; j < this.getHistoryValues[this.arraySelectedPrms[i].tagName_name].length; j++) {
          if (j == 0) {     // зануляем первое значение
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }

          if (this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val != 'NaN') {
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].dt), val: Number(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val) });

            if (this.maxValue < this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val) { // максимум интеграционных диаграм
              this.maxValue = this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val;
            }
            if (this.minValue > this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val) { // минимум интеграционных диаграм
              this.minValue = this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val;
            }
          } else {
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }

          if (j == this.getHistoryValues[this.arraySelectedPrms[i].tagName_name].length - 1) {  // зануляем последнее значение
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].dt), val: 0 }); // зануляем последнее значение для завершения графика
          }
        }
        this.idChart = this.arraySelectedPrms[i].short_desc + '_' + this.arraySelectedPrms[i].description + '_' + i + '_' + this.arraySelectedPrms[i].engUnits;

        this.activeLineAndPath1();   // вывод графиков
      }

      this.lineCursor(); // линии прицела курсора

      for (let i = 0; i < this.arraySelectedPrms.length; i++) {  // дополнительный цикл для невидимых линий интерактивной подсказки
        this.arrayForChart = [];
        for (let j = 0; j < this.getHistoryValues[this.arraySelectedPrms[i].tagName_name].length; j++) {

          if (this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val != 'NaN') {
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].dt), val: Number(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].val) });
          } else {
            this.arrayForChart.push({ dt: new Date(this.getHistoryValues[this.arraySelectedPrms[i].tagName_name][j].dt), val: 0 });
          }

        }
        this.idChart = this.arraySelectedPrms[i].short_desc + '_' + this.arraySelectedPrms[i].description + '_' + i + '_' + this.arraySelectedPrms[i].engUnits;
        this.descriptionLine();
      }


    }, error => console.error(error));

  }





  private activeAddXandYAxis1() {                        // координатные оси первого SVG-шаблона
    // range of data configuring
    this.x = d3Scale.scaleTime().range([0, this.width]);
    this.y = d3Scale.scaleLinear().range([this.height, 0]);
    this.x.domain(d3Array.extent(this.arrayForChart, (d) => d.dt));
    this.y.domain(d3Array.extent(this.arrayForChart, (d) => d.val));

    // Configure the X Axis
    this.svg1.append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .attr('style', 'stroke-opacity: 0.1')
      .call(d3Axis.axisBottom(this.x)
        .tickSize(-this.height)
        .ticks(10));

    // Configure the Y Axis
    this.svg1.append('g')
      .attr('style', 'stroke-opacity: 0.2')
      .call(d3Axis.axisLeft(this.y)
        .tickSize(-this.width)
        .ticks(10))
  }

  private selectRect() {       // селекторный прямоугольник
    this.svg1.append('rect')
      .attr("width", 0)
      .attr("height", this.height)
      .attr("x", 0)
      .attr("y", 0)
      .attr('stroke', '#feb41b')
      .attr('fill', 'rgba(0,0,0,0.05)')
      .attr('stroke-width', '0')
      .attr('id', 'selectRect');
  }







  private activeLineAndPath1() {        // вывод графиков
    this.line = d3Shape.line()
      .x((d: any) => this.x(d.dt))
      .y((d: any) => this.y(d.val));
    // Configuring line path
    this.svg1.append('path')
      .datum(this.arrayForChart)
      .attr('class', 'line over_line')
      .attr('stroke', '#feb41b')
      .attr('fill', 'rgba(255, 150, 0, 0.1)')
      .attr('stroke-width', '2')
      .attr('d', this.line)
      .attr('id', 'chart_' + this.idChart);
  }




  private lineCursor() {                // линии пересечения на курсоре
    this.svg1.append("line")
      .attr('stroke', 'rgba(0, 0, 0, 0)')
      .attr('stroke-width', '0.5')
      .attr("x1", 50)
      .attr("y1", 0)
      .attr("x2", 50)
      .attr("y2", this.height)
      .attr("id", 'xLineCursor');
    this.svg1.append("line")
      .attr('stroke', 'rgba(0, 0, 0, 0)')
      .attr('stroke-width', '0.5')
      .attr("x1", 0)
      .attr("y1", 100)
      .attr("x2", this.width)
      .attr("y2", 100)
      .attr("id", 'yLineCursor');
  }



  descriptionLine() {                           // невидимые линии для вывода подсказок при наведении
    this.line = d3Shape.line()
      .x((d: any) => this.x(d.dt))
      .y((d: any) => this.y(d.val));

    // Configuring line path
    this.svg1.append('path')
      .datum(this.arrayForChart)
      .attr('class', 'line over_line')
      .attr('stroke', 'rgba(255, 240, 0, 0)')
      .attr('fill', 'none')
      .attr('stroke-width', '16')
      .attr('d', this.line)
      .attr('id', this.idChart)
      .on('mouseover', function (d) {                     // интерактив графика

        this.style.stroke = 'rgba(255, 240, 0, 0.05)';

        var obj: any = (<HTMLInputElement>document.getElementById("group1"));
        var rect: any = obj.getBoundingClientRect();
        var splitId: any = this.id.split('_');           // переменная вывода наименования объекта, который имеет данный параметр и сам параметр

        (<HTMLInputElement>document.getElementById("chart_" + this.id)).style.stroke = 'rgb(255, 10, 0)';
        (<HTMLInputElement>document.getElementById("chart_" + this.id)).style.fill = 'rgba(255, 240, 0, 0.3)';
        (<HTMLInputElement>document.getElementById("xLineCursor")).style.stroke = 'rgba(0, 0, 0, 0.5)';
        (<HTMLInputElement>document.getElementById("yLineCursor")).style.stroke = 'rgba(0, 0, 0, 0.5)';

        let elem = document.getElementById('showChartDescription');
        document.addEventListener('mousemove', function (event) {

          (<HTMLInputElement>document.getElementById("showChartDescription")).style.left = event.pageX + 10 + 'px';
          (<HTMLInputElement>document.getElementById("showChartDescription")).style.top = event.pageY - window.pageYOffset + 20 + 'px';

          var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
          startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут

          var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
          stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут

          var startDateTime: any = new Date((<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + startTime);
          var stopDateTime: any = new Date((<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime);

          var height: any = (<HTMLInputElement>document.getElementById("yAxisHeight")).value;
          height = Number(height);
          var width: any = (<HTMLInputElement>document.getElementById("xAxisWidth")).value;
          width = Number(width);
          var marginTop: any = (<HTMLInputElement>document.getElementById("marginTop")).value;
          var marginLeft: any = (<HTMLInputElement>document.getElementById("marginLeft")).value;
          var yMaxLimit: any = (<HTMLInputElement>document.getElementById("yMaxLimit")).value;

          var fullInterval: any = stopDateTime - startDateTime;
          var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));
          eraUnix = startDateTime - eraUnix;

          var lineX: any = '' + (event.pageX - rect.left - marginLeft);
          var lineY: any = event.pageY - window.pageYOffset - rect.top - marginTop;

          (<HTMLInputElement>document.getElementById("xLineCursor")).setAttribute("x1", lineX);
          (<HTMLInputElement>document.getElementById("xLineCursor")).setAttribute("x2", lineX);

          (<HTMLInputElement>document.getElementById("yLineCursor")).setAttribute("y1", lineY);
          (<HTMLInputElement>document.getElementById("yLineCursor")).setAttribute("y2", lineY);

          var timePoint1 = (<HTMLInputElement>document.getElementById("timePoint1")).value;
          var timePoint2 = (<HTMLInputElement>document.getElementById("timePoint2")).value;
          var xSelectRect: any = (<HTMLInputElement>document.getElementById("xSelectRect")).value;

          if (timePoint1 != '') {
            if (timePoint2 == '') {
              xSelectRect = Number(xSelectRect);
              var widthSelectRect: any = Math.abs(lineX - xSelectRect);
              if (Number(lineX) < xSelectRect) {
                (<HTMLInputElement>document.getElementById("selectRect")).setAttribute("width", widthSelectRect);
                (<HTMLInputElement>document.getElementById("selectRect")).setAttribute("x", lineX);
              } else {
                (<HTMLInputElement>document.getElementById("selectRect")).setAttribute("width", widthSelectRect);
                (<HTMLInputElement>document.getElementById("selectRect")).setAttribute("x", xSelectRect);
              }
            }
          }

          var selectTime: any = new Date(eraUnix + (fullInterval * (event.pageX - rect.left - marginLeft) / width));  // выводим выделенную временную точку
          var timeBufer = selectTime;
          var selectDate = ('0' + selectTime.getDate()).slice(-2) + '.' + ('0' + (selectTime.getMonth() + 1)) + '.' + selectTime.getFullYear();
          selectTime = ('0' + selectTime.getHours()).slice(-2) + ':' + ('0' + selectTime.getMinutes()).slice(-2) + ':' + ('0' + selectTime.getSeconds()).slice(-2);

          var value: any = (height - (event.pageY - window.pageYOffset - rect.top - marginTop)) * yMaxLimit / height;  // выводим значение значение параметра в данной точке
          value = value.toFixed(3);

          (<HTMLInputElement>document.getElementById("showChartDescription")).innerHTML = '<div style="display: inline-block; background: #FFE4B5; padding: 1px 10px 1px 10px; border-radius: 3px; border: solid 1px #B22222;; ">' + splitId[0] + '</div><input type="hidden" id="lineX" value="' + lineX + '" /><input id="selectTime" type="hidden" value="' + timeBufer + '" /> ' + '<br><div style="display: inline-block; background: #FFE4B5; border: solid 1px #B22222; border-radius: 3px; margin-top: 5px; padding: 1px 10px 1px 10px;">' + splitId[1] + '</div><br><div style="display: inline-block; background: #FFFACD; border: solid 1px #B22222; margin-top: 5px; padding: 1px 10px 1px 10px; border-radius: 3px; color: #B22222; ">' + selectDate + '<br>' + selectTime + '</div><br><div style="display: inline-block; background: #fff; margin-top: 5px; padding: 3px 10px 3px 10px; border: solid 2px #B22222; border-radius: 3px;"><span style="color: #B22222;">' + value + '</span>  <span style="color: #555; margin-left: 5px;">' + splitId[3] + '</span></div>';
        });
        (<HTMLInputElement>document.getElementById("showChartDescription")).style.display = 'block';


      })
      .on('mouseout', function () {
        this.style.stroke = 'rgba(255, 240, 0, 0)';

        (<HTMLInputElement>document.getElementById("chart_" + this.id)).style.stroke = '#feb41b';
        (<HTMLInputElement>document.getElementById("chart_" + this.id)).style.fill = 'rgba(255, 150, 0, 0.1)';
        (<HTMLInputElement>document.getElementById("xLineCursor")).style.stroke = 'rgba(0, 0, 0, 0)';
        (<HTMLInputElement>document.getElementById("yLineCursor")).style.stroke = 'rgba(0, 0, 0, 0)';

        (<HTMLInputElement>document.getElementById("showChartDescription")).style.left = 0 + 'px';
        (<HTMLInputElement>document.getElementById("showChartDescription")).style.top = 1200 + 'px';
        (<HTMLInputElement>document.getElementById("showChartDescription")).style.display = 'none';
      });
  }








  rightScalePlus() {  // правое масштабирование на +

    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

    var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
    stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут

    var stopDateTime: any = new Date((<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime);
    stopDateTime = new Date(stopDateTime - eraUnix + 300000);

    (<HTMLInputElement>document.getElementById("dateStop")).value = stopDateTime.getFullYear() + '-' + ('0' + (stopDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + stopDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + stopDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + stopDateTime.getMinutes()).slice(-2);

    this.getClient();
  }


  rightScaleMinus() {    // правое масштабирование на -

    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

    var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
    stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут

    var stopDateTime: any = new Date((<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime);
    stopDateTime = new Date(stopDateTime - eraUnix - 300000);

    (<HTMLInputElement>document.getElementById("dateStop")).value = stopDateTime.getFullYear() + '-' + ('0' + (stopDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + stopDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + stopDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + stopDateTime.getMinutes()).slice(-2);

    this.getClient();
  }


  leftScaleMinus() {  // левое масштабирование на -

    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

    var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
    startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут

    var startDateTime: any = new Date((<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + startTime);
    startDateTime = new Date(startDateTime - eraUnix + 300000);

    (<HTMLInputElement>document.getElementById("dateStart")).value = startDateTime.getFullYear() + '-' + ('0' + (startDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + startDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + startDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + startDateTime.getMinutes()).slice(-2);

    this.getClient();
  }


  leftScalePlus() {    // левое масштабирование на +

    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

    var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
    startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут

    var startDateTime: any = new Date((<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + startTime);
    startDateTime = new Date(startDateTime - eraUnix - 300000);

    (<HTMLInputElement>document.getElementById("dateStart")).value = startDateTime.getFullYear() + '-' + ('0' + (startDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + startDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + startDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + startDateTime.getMinutes()).slice(-2);

    this.getClient();
  }


  moveRight() {    // сдвигаем временной интервал вправо 

    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

    var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
    stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут

    var stopDateTime: any = new Date((<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime);
    stopDateTime = new Date(stopDateTime - eraUnix + 300000);

    (<HTMLInputElement>document.getElementById("dateStop")).value = stopDateTime.getFullYear() + '-' + ('0' + (stopDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + stopDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + stopDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + stopDateTime.getMinutes()).slice(-2);


    var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
    startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут

    var startDateTime: any = new Date((<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + startTime);
    startDateTime = new Date(startDateTime - eraUnix + 300000);

    (<HTMLInputElement>document.getElementById("dateStart")).value = startDateTime.getFullYear() + '-' + ('0' + (startDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + startDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + startDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + startDateTime.getMinutes()).slice(-2);

    this.getClient();
  }


  moveLeft() {    // сдвигаем временной интервал влево

    var eraUnix: any = new Date(Date.UTC(1970, 0, 1, 0, 0));

    var stopTime = (<HTMLInputElement>document.getElementById("timeStop")).value;
    stopTime = stopTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStop")).value;  // время завершения с учетом минут

    var stopDateTime: any = new Date((<HTMLInputElement>document.getElementById("dateStop")).value + ' ' + stopTime);
    stopDateTime = new Date(stopDateTime - eraUnix - 300000);

    (<HTMLInputElement>document.getElementById("dateStop")).value = stopDateTime.getFullYear() + '-' + ('0' + (stopDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + stopDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStop")).value = ('0' + stopDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStop")).value = ('0' + stopDateTime.getMinutes()).slice(-2);

    var startTime = (<HTMLInputElement>document.getElementById("timeStart")).value;
    startTime = startTime.slice(0, 3) + (<HTMLInputElement>document.getElementById("minStart")).value;  // стартовое время с учетом минут

    var startDateTime: any = new Date((<HTMLInputElement>document.getElementById('dateStart')).value + ' ' + startTime);
    startDateTime = new Date(startDateTime - eraUnix - 300000);

    (<HTMLInputElement>document.getElementById("dateStart")).value = startDateTime.getFullYear() + '-' + ('0' + (startDateTime.getMonth() + 1)).slice(-2) + '-' + ('0' + startDateTime.getDate()).slice(-2);
    (<HTMLInputElement>document.getElementById("timeStart")).value = ('0' + startDateTime.getHours()).slice(-2) + ':00';
    (<HTMLInputElement>document.getElementById("minStart")).value = ('0' + startDateTime.getMinutes()).slice(-2);

    this.getClient();
  }





  fullRequest() {
    this.startDateTime = this.startDateTime.slice(0, this.startDateTime.length - 2) + (<HTMLInputElement>document.getElementById("minStart")).value;
    this.stopDateTime = this.stopDateTime.slice(0, this.stopDateTime.length - 2) + (<HTMLInputElement>document.getElementById("minStop")).value;

    this.http.get<Value[]>(this.pathUrl + '/' + this.startDateTime + '/' + this.stopDateTime).subscribe(result => {
      this.values = result;

      this.svg1.selectAll("*").remove();

      this.start = 0;
      this.stop = this.values.length - 1;

      this.getDateArray();
      this.addXandYAxis1();
      this.addObject1();


    }, error => console.error(error));
  }





  getDateArray() {            // Вывод массива тестовых данных
    this.data1.push({ dt: new Date(this.values[this.start].dt), value: 0 });
    this.data2.push({ dt: new Date(this.values[this.start].dt), value: 0 });
    this.data3.push({ dt: new Date(this.values[this.start].dt), value: 0 });
    this.data4.push({ dt: new Date(this.values[this.start].dt), value: 0 });
    for (let i = this.start; i <= this.stop; i++) {
      this.data1.push({ dt: new Date(this.values[i].dt), value: this.values[i].value1 });
      this.data2.push({ dt: new Date(this.values[i].dt), value: this.values[i].value2 });
      this.data3.push({ dt: new Date(this.values[i].dt), value: this.values[i].value3 });
      this.data4.push({ dt: new Date(this.values[i].dt), value: this.values[i].value4 });
    }
    this.data1.push({ dt: new Date(this.values[this.stop].dt), value: 0 });
    this.data2.push({ dt: new Date(this.values[this.stop].dt), value: 0 });
    this.data3.push({ dt: new Date(this.values[this.stop].dt), value: 0 });
    this.data4.push({ dt: new Date(this.values[this.stop].dt), value: 0 });

    this.data1.push({ dt: new Date(this.values[this.stop].dt), value: this.yLimit });
    this.data2.push({ dt: new Date(this.values[this.stop].dt), value: this.yLimit });
    this.data3.push({ dt: new Date(this.values[this.stop].dt), value: this.yLimit });
    this.data4.push({ dt: new Date(this.values[this.stop].dt), value: this.yLimit });
    this.dataLength = this.data1.length;
  }

  getValues() {
    return this.values;
  }

  private buildSvg1() {
    this.svg1 = d3.select('.group1')
      .append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

  private addXandYAxis1() {
    // range of data configuring
    this.x = d3Scale.scaleTime().range([0, this.width]);
    this.y = d3Scale.scaleLinear().range([this.height, 0]);
    this.x.domain(d3Array.extent(this.data1, (d) => d.dt));
    this.y.domain(d3Array.extent(this.data1, (d) => d.value));

    // Configure the X Axis
    this.svg1.append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .attr('style', 'stroke-opacity: 0.1')
      .call(d3Axis.axisBottom(this.x)
        .tickSize(-this.height)
        .ticks(10));

    // Configure the Y Axis
    this.svg1.append('g')
      .attr('style', 'stroke-opacity: 0.2')
      .call(d3Axis.axisLeft(this.y)
        .tickSize(-this.width)
        .ticks(5))
  }



  private addObject1() {
    this.svg1.append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', -50)
      .attr('x', 0)
      .attr('text-anchor', 'end')
      .text('Величина');

    this.svg1.append('text')
      .attr('y', 440)
      .attr('x', 0)
      .attr('text-anchor', 'start')
      .text('Хронологическая последовательность');
  }



}


interface Value {
  id: number;
  dt: string;
  value1: number;
  value2: number;
  value3: number;
  value4: number;
}


interface ArrayParameters {
  dt: any;
  shortDesc: string;
  attributes: any;
  tagName: string;
  description: string;
  length: number;
}

