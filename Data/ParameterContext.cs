﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProtoMES.Controllers.Models;

namespace ProtoMES.Models
{
    public class ParameterContext : DbContext
    {
        public ParameterContext()
        {
        }

        public ParameterContext (DbContextOptions<ParameterContext> options)
            : base(options)
        {
            /*Parameter prm0 = new Parameter(1, "Temperature", "Температура в печи. Зона 1", "double", 0, null);
            Parameter prm1 = new Parameter(2, "Temperature", "Температура в печи. Зона 1", "double", 1, null);
            Parameter prm2 = new Parameter(3, "Temperature", "Температура в печи. Зона 1", "double", 1, null);
            Parameter prm3 = new Parameter(4, "Temperature", "Температура в печи. Зона 1", "double", 1, null);
            Parameter prm4 = new Parameter(5, "Temperature", "Температура в печи. Зона 1", "double", 1, null);
            Parameter prm5 = new Parameter(6, "Temperature", "Температура в печи. Зона 1", "double", 1, null);
            Parameter prm6 = new Parameter(7, "Temperature", "Температура в печи. Зона 1", "double", 6, null);
            Parameter prm7 = new Parameter(8, "Temperature", "Температура в печи. Зона 1", "double", 6, null);
            Parameter prm8 = new Parameter(9, "Temperature", "Температура в печи. Зона 1", "double", 6, null);
            Parameter prm9 = new Parameter(10, "Temperature", "Температура в печи. Зона 1", "double", 6, null);
            prm0.children = new List<Parameter>();
            prm0.children.Add(prm1);
            prm0.children.Add(prm2);
            prm0.children.Add(prm3);
            prm0.children.Add(prm4);
            prm0.children.Add(prm5);
            //prm0.children.Add(prm6);
            prm6.children = new List<Parameter>();
            prm6.children.Add(prm7);
            prm6.children.Add(prm8);
            prm6.children.Add(prm9);
            Parameter.Add(prm0);
            Parameter.Add(prm6);*/

            var rng = new Random();
            foreach(int i in Enumerable.Range(1, 3000))
            {
                PrmValue pv = new PrmValue();
                pv.id = i;
                pv.dt = DateTime.Now.AddMinutes(-1000 + i);
                pv.value1 = rng.Next(1000, 1200);
                pv.value2 = rng.Next(800, 1200);
                pv.value3 = rng.Next(500, 700);
                pv.value4 = rng.Next(100, 150);
                PrmValue.Add(pv);
            }
            /*prm1.values =  Enumerable.Range(1, 100).Select(index => new KeyValuePair<DateTime, object>
            {
                Key = DateTime.Now.AddDays(index).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });*/
        }

        public DbSet<ProtoMES.Controllers.Models.Parameter> Parameter { get; set; }

        public DbSet<ProtoMES.Controllers.Models.PrmValue> PrmValue { get; set; }
    }
}
