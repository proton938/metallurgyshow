﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProtoMES.Controllers.Models;

namespace ProtoMES.Models
{
    public class DataContext : DbContext
    {
        public DataContext()
        {

        }

        public DataContext (DbContextOptions<DataContext> options)
            : base(options)
        {
            Equipment eq = new Equipment();
            eq.id = 1;
            eq.name = "Методическая печь 1";
            Equipment eq1 = new Equipment();
            eq1.id = 2;
            eq1.name = "Методическая печь 2";
            this.Equipment.Add(eq);
            this.Equipment.Add(eq1);
        }

        public DbSet<ProtoMES.Controllers.Models.Equipment> Equipment { get; set; }
    }
}
